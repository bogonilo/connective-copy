package com.lorenzo.connectivecp.me

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.*
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.init.landing.LandingActivityNew
import com.lorenzo.connectivecp.me.past.PastFragment
import com.lorenzo.connectivecp.me.upcoming.UpcomingFragment
import com.lorenzo.connectivecp.profile.view.ViewProfileActivity
import com.lorenzo.connectivecp.utils.Utils
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_me.*

class MeFragment : Fragment(), MeContract.View {

    private lateinit var name: TextView
    private lateinit var proPic: ImageView
    private lateinit var dialogView: View
    private lateinit var logoutBtn: Button
    private lateinit var cancelLogOutDialog: ImageButton
    private lateinit var dialogBuilder: AlertDialog.Builder
    private lateinit var dialog: AlertDialog
    private var upcomingFrag = UpcomingFragment()
    private var pastFrag = PastFragment()
    private lateinit var presenter: MePresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_me, container, false)
        name = v.findViewById(R.id.name_meFrag)
        proPic = v.findViewById(R.id.proPic_meFrag)
        dialogView = View.inflate(context, R.layout.logout_dialog, null)
        dialogBuilder = AlertDialog.Builder(context).setView(dialogView)
        logoutBtn = dialogView.findViewById(R.id.buttonLogOutDialog)
        cancelLogOutDialog = dialogView.findViewById(R.id.cancelLogOutDialog)
        logoutBtn.setOnClickListener {
            dialog.cancel()
            signOut()
        }
        cancelLogOutDialog.setOnClickListener { dialog.cancel() }
        presenter = MePresenter(this, context)
        presenter.getUserData()
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        childFragmentManager.beginTransaction().add(fragContainer_meFrag.id, pastFrag,"past").hide(pastFrag).commit()
        childFragmentManager.beginTransaction().add(fragContainer_meFrag.id, upcomingFrag, "upcoming").commit()
        tablayout_meFrag.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(p0: TabLayout.Tab?) { presenter.tabSelected(p0?.position) }
            override fun onTabReselected(p0: TabLayout.Tab?) {}
            override fun onTabUnselected(p0: TabLayout.Tab?) {}
        })
        viewProfileTxt_meFrag.setOnClickListener { presenter.viewProfileClicked() }
        settings_meFrag.setOnClickListener { createDialog(resources.getStringArray(R.array.settings_profile)) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun loadInfo() {
        upcomingFrag.user = presenter.user
        pastFrag.user = presenter.user
        name.setText("Hi, "+presenter.user.name.split(" ")[0])
        Glide.with(proPic.context).load(presenter.user.profileImgUrl).centerCrop().into(proPic)
        presenter.getCalendarListOfEvents()
    }

    override fun displayPast() {
        val ft = childFragmentManager.beginTransaction()
        ft.hide(upcomingFrag).show(pastFrag)
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun displayUpcoming() {
        val ft = childFragmentManager.beginTransaction()
        ft.hide(pastFrag).show(upcomingFrag)
        ft.addToBackStack(null)
        ft.commit()
    }

    override fun passDataToFragment() {
        upcomingFrag.populateList(presenter.sortedUpcomingEvents)
        pastFrag.populateList(presenter.sortedPastEvents)
    }

    override fun hideEmptyPast() {
        pastFrag.hideEmptyListTxt()
    }

    override fun showEmptyPast() {
        pastFrag.showEmptyListTxt()
    }

    override fun hideEmptyUpcoming() {
        upcomingFrag.hideEmptyListTxt()
    }

    override fun showEmptyUpcoming() {
        upcomingFrag.showEmptyListTxt()
    }

    override fun startViewProfileActivity() {
        val intent = Intent(context, ViewProfileActivity::class.java)
        intent.putExtra("local", true)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivityForResult(intent, Utils.PROFILE_VIEW_CODE)
    }

    override fun startLandingActivity() {
        startActivity(Intent(context, LandingActivityNew::class.java))
        activity?.finishAffinity()
    }

    override fun failedLogOut() {
        Toast.makeText(context, "Failed Log Out", Toast.LENGTH_LONG).show()
    }

    fun updateCalendar(){
        presenter.getCalendarListOfEvents()
    }

    override fun emptyAdaptersInFragments() {
        upcomingFrag.emptyAdapter()
        pastFrag.emptyAdapter()
    }

    override fun signOut() {
        presenter.signOutUser(context)
    }

    fun createDialog(listItems: Array<String>){
        val items = listItems
        val builder = AlertDialog.Builder(context, R.style.settings_profile_dialog)
        builder.setItems(items,
            DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel()
                presenter.settingsClicked(which)
            })
        val dialog = builder.create()
        with(dialog){
            listView.divider = ColorDrawable(Color.parseColor("#E8E8E8"))
            listView.dividerHeight = 2
            show()
        }
        val window = dialog.getWindow()
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
        window?.setBackgroundDrawableResource(R.drawable.shape_location_dialog)
        val wlp = window?.getAttributes()
        wlp?.gravity = Gravity.BOTTOM
        wlp?.dimAmount = 0.3f
        window?.setAttributes(wlp)
    }

    override fun showLogOutDialog() {
        dialog = dialogBuilder.create()
        with(dialog){
            show()
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setOnCancelListener { (dialogView.parent as ViewGroup).removeView(dialogView) }
        }
    }

    fun updateUser(){
        presenter.getUserData()
    }
}
