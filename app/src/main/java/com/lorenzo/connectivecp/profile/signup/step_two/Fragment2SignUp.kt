package com.lorenzo.connectivecp.profile.signup.step_two

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.view.*
import android.widget.*
import androidx.cardview.widget.CardView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.image_crop.ImageCropActivity
import com.lorenzo.connectivecp.utils.Utils
import com.lorenzo.connectivecp.profile.signup.SignUpFragmentsActivity
import com.lorenzo.connectivecp.profile.signup.step_three.Fragment3SignUp
import com.lorenzo.connectivecp.view.TextInputField
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import java.io.File
import java.util.ArrayList

class Fragment2SignUp : Fragment(), Fragment2Contract.View {

    private lateinit var nextBtn: Button
    private lateinit var name: TextInputField
    private lateinit var imageViewProfile: ImageView
    private lateinit var imageViewCard: CardView
    private lateinit var presenter: Fragment2Presenter
    private var fragment3SignUp = Fragment3SignUp()
    private lateinit var fm: FragmentManager
    private var imageUri: Uri = Uri.parse("android.resource://com.lorenzo.connectivecp/drawable/profile_photo")
    private var imagePath = "android.resource://com.lorenzo.connectivecp/drawable/accenture_logo"
    private lateinit var initialPicParam: FrameLayout.LayoutParams

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.signup_frag2, container, false)
        nextBtn = view.findViewById(R.id.nextFragBtn2)
        name = view.findViewById(R.id.name_signUp2)
        imageViewProfile = view.findViewById(R.id.proPic_viewProfile)
        imageViewCard = view.findViewById(R.id.proPic_card_frag2)
        presenter = Fragment2Presenter(this)
        imageViewProfile.setImageURI(imageUri)
        (activity as SignUpFragmentsActivity).setTitleText(1)
        initialPicParam = imageViewProfile.layoutParams as FrameLayout.LayoutParams
        fm = fragmentManager!!
        imageViewProfile.setOnClickListener{ createDialogForPic() }
        nextBtn.setOnClickListener{
            presenter.nextBtnClicked(arguments!!.getStringArrayList("user")!![0], name.text.toString(), imagePath)
        }
        name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                presenter.textChanged(s)
            }
        })
        return view
    }

    private fun createDialogForPic(){
        val listItems = presenter.getPicListItems(resources.getStringArray(R.array.pic_options).toCollection(ArrayList()))
        val builder = AlertDialog.Builder(context, R.style.full_width_dialog)
        builder.setItems(listItems) { dialog, which ->
            presenter.picOptClicked(which)
        }
        val dialog = builder.create()
        with(dialog){
            listView.divider = ColorDrawable(Color.parseColor("#F2F2F2"))
            listView.dividerHeight = 2
            show()
        }
        val window = dialog.getWindow()
        window?.let{
            it.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            it.setBackgroundDrawableResource(R.drawable.shape_location_dialog)
            val wlp = it.getAttributes()
            wlp.gravity = Gravity.BOTTOM
            wlp.dimAmount = 0.3f
            it.setAttributes(wlp)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.activityResult(requestCode, resultCode, data, context!!)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        presenter.requestPermissionResult(requestCode, permissions, grantResults)
    }

    override fun nextPage() {
        SignUpFragmentsActivity.nextFragment(fm, fragment3SignUp)
    }

    override fun setArgumentsBundle(bundle: Bundle) {
        fragment3SignUp.arguments = bundle
    }

    override fun nameFieldEmpty() {
        name.shakeView(context)
        Toast.makeText(context, "Enter your full name", Toast.LENGTH_LONG).show()
    }

    override fun setUri(uri: Uri) {
        imageUri = uri
        imageViewProfile.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT)
        Glide.with(imageViewProfile.context).load(uri).centerCrop().into(imageViewProfile)
        imagePath = Utils.getPath(context, imageUri)
    }

    override fun setImgUri(uri: Uri) {
        imageUri = uri
        imagePath = "android.resource://com.lorenzo.connectivecp/drawable/accenture_logo"
        imageViewProfile.setImageURI(Uri.parse("android.resource://com.lorenzo.connectivecp/drawable/profile_photo"))
    }

    override fun setUriPathEverything(uri: Uri, path: String) {
        imageUri = uri
        imagePath = path
        imageViewProfile.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT)
        Glide.with(imageViewProfile.context).load(uri).diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true).centerCrop().into(imageViewProfile)
    }

    override fun setImageFromCamera(file: File) {
        imageUri = Uri.fromFile(file)
        imagePath = file.absolutePath
    }

    override fun setImageView() {
        startCrop(imageUri)
//        imageViewProfile.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//            ViewGroup.LayoutParams.MATCH_PARENT)
//        Glide.with(imageViewProfile.context).load(imageUri).centerCrop().into(imageViewProfile)
    }

    override fun clickAddPic() {
        imageViewProfile.performClick()
    }

    override fun galleryPic() {
        Utils.pickFromGallery(this, this.context!!)
    }

    override fun cameraPic(): File? {
        return Utils.pickFromCameraForProPic(this, this.context!!)
    }

    override fun startCrop(uri: Uri) {
//        val intent = CropImage.activity(uri)
//            .setCropShape(CropImageView.CropShape.OVAL)
//            .setFixAspectRatio(true)
//            .setActivityTitle("")
//            .setAllowRotation(false)
//            .setAllowFlipping(false)
//            .getIntent(context!!)
//        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        ImageCropActivity.startActivity(this, uri)
    }

    override fun switchOffNextButton() {
        nextBtn.setBackgroundResource(R.drawable.shape_signup_unselected)
    }

    override fun switchOnNextButton() {
        nextBtn.setBackgroundResource(R.drawable.shape_signup)
    }

    override fun setRadiusOnCard() {
        imageViewCard.radius = Utils.dpToPx(context, 70f)!!.toFloat()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageViewCard.elevation = 30f
        }
    }

    override fun unsetRadiusOnCard() {
        imageViewCard.radius = 0f
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageViewCard.elevation = 0f
        }
    }

    override fun removePic() {
        unsetRadiusOnCard()
        imageUri = Uri.parse("android.resource://com.lorenzo.connectivecp/drawable/profile_photo")
        imagePath = "android.resource://com.lorenzo.connectivecp/drawable/accenture_logo"
        imageViewProfile.layoutParams = initialPicParam
        imageViewProfile.setImageURI(imageUri)
    }
}