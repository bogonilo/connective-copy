package com.lorenzo.connectivecp.utils

enum class EventsFilterEnum {
    TIME, RELEVANCE
}