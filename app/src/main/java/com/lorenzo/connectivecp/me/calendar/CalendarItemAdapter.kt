package com.lorenzo.connectivecp.me.calendar

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.ideas.browsing.ideaInfo.IdeaInfoActivity
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.utils.Utils

class CalendarItemAdapter(private var dataset: ArrayList<Idea>, private var userUid: String,
                          private var mContext: Context?) : RecyclerView.Adapter<CalendarItemViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CalendarItemViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.calendar_item, p0, false)
        return CalendarItemViewHolder(v)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(p0: CalendarItemViewHolder, p1: Int) {
        p0.bindView(dataset[p1], userUid, this)
    }

    fun startEventInfo(idea: Idea){
        val intentIdeaInfo = Intent(mContext, IdeaInfoActivity::class.java)
        intentIdeaInfo.putExtra("ideaID", idea.id)
        (mContext as Activity).startActivityForResult(intentIdeaInfo, Utils.VIEW_EVENT_INFO)
    }
}