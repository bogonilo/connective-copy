package com.lorenzo.connectivecp.profile.signup.skillsFragment.suggestion

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.signup.skillsFragment.SkillsActivity
import com.lorenzo.connectivecp.profile.signup.skillsFragment.SkillsFragment
import com.lorenzo.connectivecp.skills.SkillsSearchFragment

class SuggestionDialogAdapter(private var suggestions: ArrayList<String>) :
    RecyclerView.Adapter<SuggestionDialogViewHolder>() {

    private var actProfile = SkillsActivity()
    private var fragProfile = SkillsFragment()
    private var fragSkillsSeach = SkillsSearchFragment()

    var flag = 0

    constructor(suggestions: ArrayList<String>, act: SkillsActivity) : this(suggestions) {
        this.actProfile = act
        flag = 0
    }

    constructor(suggestions: ArrayList<String>, frag: SkillsFragment) : this(suggestions) {
        this.fragProfile = frag
        flag = 1
    }

    constructor(suggestions: ArrayList<String>, frag: SkillsSearchFragment) : this(suggestions) {
        this.fragSkillsSeach = frag
        flag = 2
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SuggestionDialogViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.skills_suggestion_list, p0, false)
        return SuggestionDialogViewHolder(v)
    }

    override fun getItemCount(): Int {
        return suggestions.size
    }

    override fun onBindViewHolder(p0: SuggestionDialogViewHolder, p1: Int) {
        p0.bindView(suggestions[p1], this)
    }

    fun passSkillToFrag(skill: String){
        fragProfile.receiveData(skill)
    }

    fun passSkillToAct(skill: String){
        actProfile.receiveData(skill)
    }

    fun passSkillToSkillsSearchFrag(skill: String){
        fragSkillsSeach.receiveData(skill)
    }
}