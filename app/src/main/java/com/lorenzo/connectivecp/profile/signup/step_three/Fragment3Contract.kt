package com.lorenzo.connectivecp.profile.signup.step_three

import com.lorenzo.connectivecp.model.User

interface Fragment3Contract {

    interface View{
        fun nonValidfields()
        fun startMainActivity(user: User)
        fun hideProgBar()
        fun showProgBar()
        fun setLocationText(value: String)
        fun setRoleText(value: String)
        fun setChapterText(value: String)
        fun createDialog(listItems: Array<String>, title: String, tag: String)
        fun shakeRole()
        fun shakeLocation()
        fun shakeChapter()
        fun activateButton()
        fun deactButton()
        fun skillsFrag(user: User)
        fun getLocation():String
        fun getPosition():String
        fun getChapter():String
    }

    interface Presenter {
        fun nextClicked(email: String, name: String,
            imagePath: String, position: String, location: String, chapter: String)
        fun dialogClicked(tag: String, value: String, index: Int)
        fun loadRolesArray()
        fun loadChapters()
        fun checkAllFields()
    }
}