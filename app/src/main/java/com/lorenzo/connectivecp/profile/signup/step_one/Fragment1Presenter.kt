package com.lorenzo.connectivecp.profile.signup.step_one

import android.content.Context
import android.os.Bundle
import com.lorenzo.connectivecp.utils.SharedPreferenceManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import java.util.regex.Matcher
import java.util.regex.Pattern

class Fragment1Presenter(view: Fragment1Contract.View, context: Context?) : Fragment1Contract.Presenter {

    private var mView = view
    private var auth = FirebaseAuth.getInstance()
    private var sharedPreferences = SharedPreferenceManager(context)

    override fun passwordValidation(password: String): Boolean {
        val pattern: Pattern
        val matcher: Matcher

        /**
         * ^                 # start-of-string
         * (?=.*[0-9])       # a digit must occur at least once
         * (?=.*[a-z])       # a lower case letter must occur at least once
         * (?=.*[A-Z])       # an upper case letter must occur at least once
         * (?=.*[@#$%^&+=])  # a special character must occur at least once you can replace with your special characters
         * (?=\\S+$)          # no whitespace allowed in the entire string
         * .{8,}             # anything, at least eight places though
         * $                 # end-of-string
         */
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$"

        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }

    override fun nextBtnClicked(email: String, email_domain: String, pwd: String) {
        mView.disableButtonClick()
        if (passwordValidation(pwd)) {
            if(emailValidation(email)){
                mView.showProgressBar()
                auth.createUserWithEmailAndPassword(email + '@' + email_domain, pwd)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            auth.currentUser?.let { sharedPreferences.updateIDinPreferences(it.uid) }
                            var user: ArrayList<String> = ArrayList(7)
                            user.add(email + '@' + email_domain)
                            var bundle = Bundle()
                            bundle.putStringArrayList("user", user)
//                        bundle.putString("pwd", pwd)
                            mView.setArgumentsBundle(bundle)
                            mView.nextPage()
                        }
                        else {
                            mView.hideProgressBar()
                            mView.activeButtonClick()
                            if (task.exception is FirebaseAuthUserCollisionException)
                                mView.authErrorUserCollision()
                            else
                                mView.authError(task.exception)
                        }
                    }
            }
            else{
                mView.invalidEmail()
                mView.activeButtonClick()
            }
        } else {
            mView.passwordNotValid()
            mView.activeButtonClick()
        }
    }

    override fun onTextChangedPwd(s: CharSequence?, email: String) {
        if (emailValidation(email)) {
            s?.let {
                if (passwordValidation(it.toString()))
                    mView.switchOnSignUpBtn()
                else
                    mView.switchOffSignUpBtn()
            }
        } else {
            mView.switchOffSignUpBtn()
        }
    }

    override fun onTextChangedEmail(s: CharSequence?, pwd: String) {
        if (passwordValidation(pwd)) {
            s?.let {
                if (emailValidation(it.toString()))
                    mView.switchOnSignUpBtn()
                else
                    mView.switchOffSignUpBtn()
            }
        } else {
            mView.switchOffSignUpBtn()
        }
    }

    //^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]$
    override fun emailValidation(email: String): Boolean {
        if (email.isNotEmpty()){
            val pattern: Pattern
            val matcher: Matcher
            val USERNAME_PATTERN = "^[A-Za-z0-9+_.-]+$"
            pattern = Pattern.compile(USERNAME_PATTERN)
            matcher = pattern.matcher(email)
            return matcher.matches()
        }
        return false
    }

    override fun setButtonStatus(email: String, pwd: String) {
        if(emailValidation(email) && passwordValidation(pwd))
            mView.switchOnSignUpBtn()
        else
            mView.switchOffSignUpBtn()
    }
}