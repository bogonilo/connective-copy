package com.lorenzo.connectivecp.profile.edit

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.storage.StorageReference
import java.io.File
import java.lang.Exception

interface EditProfileContract {

    interface View{
        fun notifyAdapterSkills()
        fun notifyAdapterInterests()
        fun showProgBar()
        fun hideProgBar()
        fun finishActivityWithResult()
        fun restartFromSplash()
        fun showErrorDelUser(exception: Exception)
        fun setImage(uri: Uri)
        fun getContext(): Context
        fun setOfficeText(office: String)
        fun setLocationText(value: String)
        fun setRoleText(value: String)
        fun setChapterText(value: String)
        fun showDialog(dialog: Dialog)
//        fun setUri(uri: Uri)
//        fun setImgUri(uri: Uri)
        fun clickAddPic()
        fun galleryPic()
        fun cameraPic(): File?
        fun setImageFromCamera(file: File)
        fun setImageView()
        fun startCrop(uri: Uri)
        fun setUriPathEverything(uri: Uri, path: String)
        fun createDialog(listItems: Array<String>, title: String, tag: String)
        fun removePic()
        fun startSkillsActivity()
        fun startInterestsActivity()
        fun invalidName()
        fun reSetOriginalPic()
    }

    interface Presenter{
        fun setImgUrl()
        fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, context: Context)
        fun getImageReference(): StorageReference
        fun updateUserInfo(name: String, position: String, office: String, chapter: String, imagePath: String)
        fun deleteUserClicked():Dialog
        fun delCurrentUser()
        fun loadRolesArray()
        fun loadChapters()
        fun getPicListItems(array: ArrayList<String>): Array<String>
        fun reauthUserAndDelete(user: FirebaseUser, password: String)
        fun dialogClicked(tag: String, value: String, index: Int)
        fun picOptClicked(opt: Int)
        fun removePicFromDB()
    }

}