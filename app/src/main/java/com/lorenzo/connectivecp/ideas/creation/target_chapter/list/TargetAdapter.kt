package com.lorenzo.connectivecp.ideas.creation.target_chapter.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R

class TargetAdapter (private var dataset: ArrayList<String>): RecyclerView.Adapter<TargetViewHolder>() {

    var chapters = ArrayList<String>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TargetViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.chapter_element, p0, false)
        return TargetViewHolder(v)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(p0: TargetViewHolder, p1: Int) {
        p0.bindView(dataset[p1], this, false)
    }

    fun retrieveChaptersSelected():ArrayList<String>{
        return chapters
    }
}