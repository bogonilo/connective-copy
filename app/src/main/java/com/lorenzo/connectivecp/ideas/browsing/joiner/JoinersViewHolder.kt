package com.lorenzo.connectivecp.ideas.browsing.joiner

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.bumptech.glide.Glide
import com.lorenzo.connectivecp.model.Joiner
import com.lorenzo.connectivecp.profile.view.ViewProfileActivity
import kotlinx.android.synthetic.main.joiner.view.*

class JoinersViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    private var firestoreManager = FirestoreManager()

    fun bindView(joiner: Joiner){
        itemView.layout_joiner.setOnClickListener(View.OnClickListener {
            val intent = Intent(it.context, ViewProfileActivity::class.java)
            intent.putExtra("uid", joiner.uid)
            it.context.startActivity(intent)
        })
    }

    fun setNameAndImg(name: String, url: String){
        itemView.name_joiner.setText(name)
        Glide.with(itemView.proPic_joiner.context).load(url).placeholder(R.drawable.accenture_logo).centerCrop().into(itemView.proPic_joiner)
    }
}
