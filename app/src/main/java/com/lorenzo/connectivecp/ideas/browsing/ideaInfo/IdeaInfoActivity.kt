package com.lorenzo.connectivecp.ideas.browsing.ideaInfo

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.ideas.browsing.card.joiners.JoinerCardAdapter
import com.lorenzo.connectivecp.ideas.browsing.ideaInfo.updates.UpdateAdapter
import com.lorenzo.connectivecp.ideas.browsing.joiner.JoinersAdapter
import com.lorenzo.connectivecp.model.Joiner
import java.text.SimpleDateFormat
import com.lorenzo.connectivecp.profile.view.ViewProfileActivity
import com.lorenzo.connectivecp.utils.Utils
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_idea_info.*
import java.util.*
import kotlin.collections.ArrayList

class IdeaInfoActivity : AppCompatActivity(), IdeaInfoContract.View {

    private lateinit var presenter: IdeaInfoPresenter
    private lateinit var partecipantsN: TextView
    private lateinit var location: TextView
    private lateinit var joiners: TextView
    private lateinit var joinBtn: Button
    private lateinit var organizerName: TextView
    private lateinit var organizerPic: ImageView
    private lateinit var viewProfile: TextView
    private lateinit var description: TextView
    private lateinit var ideaImage: ImageView
    private lateinit var updatesLayout: LinearLayout
    private lateinit var insertUpdateLayout: ConstraintLayout
    private lateinit var mainLayout: ConstraintLayout
    private lateinit var commentText: EditText
    private lateinit var insertComment: ImageButton
    private lateinit var recyclerViewComm: RecyclerView
    private lateinit var interestBtn: FloatingActionButton
    private lateinit var viewAdapterUpdates: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var recyclerViewJoiners: RecyclerView
    private lateinit var viewAdapterJoiners: RecyclerView.Adapter<*>
    private lateinit var joinersText: TextView
    var df = SimpleDateFormat("EEE, MMM d, yyyy HH:mm", Locale.ENGLISH)
    private var creator = false
    private lateinit var circularProgressDrawable: CircularProgressDrawable
    private lateinit var editButton: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_idea_info)
        partecipantsN = findViewById(R.id.partecipants_n_ideaInfo)
        location = findViewById(R.id.location_ideaInfo)
        joiners = findViewById(R.id.joiners_ideaInfo)
        joinBtn = findViewById(R.id.join_btn_ideaInfo)
        organizerName = findViewById(R.id.name_organizer_ideaInfo)
        organizerPic = findViewById(R.id.image_creator_ideaInfo)
        viewProfile = findViewById(R.id.view_profile_ideaInfo)
        ideaImage = findViewById(R.id.imageView_ideaInfo)
        description = findViewById(R.id.description_ideaInfo)
        updatesLayout = findViewById(R.id.updatesLayout_ideaInfo)
        commentText = findViewById(R.id.text_comment_fragment)
        insertComment = findViewById(R.id.insert_comment_fragment)
        recyclerViewComm = findViewById(R.id.updates_recyclerView)
        insertUpdateLayout = findViewById(R.id.insert_comm_layout)
        mainLayout = findViewById(R.id.main_layout_ideaInfo)
        interestBtn = findViewById(R.id.interest_btn_ideaInfo)
        recyclerViewJoiners = findViewById(R.id.recyclerView_joiners_ideaInfo)
        joinersText = findViewById(R.id.joiners_text_ideaInfo)
        editButton = findViewById(R.id.edit_ideaInfo)
        presenter = IdeaInfoPresenter(this, applicationContext)
        presenter.activityID = intent.getStringExtra("ideaID")
        presenter.loadUser()
        presenter.loadEventInfo()
        circularProgressDrawable = CircularProgressDrawable(this)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()
        viewProfile.setOnClickListener { presenter.viewProfileClicked() }
        joinBtn.setOnClickListener { presenter.joinBtnClicked() }
        insertComment.setOnClickListener{ presenter.addComment(commentText.text.toString())}
        editButton.setOnClickListener { createPopUpMenu() }
    }

    override fun createPopUpMenu() {
        val popup = PopupMenu(this, editButton)
        popup.menuInflater.inflate(R.menu.idea_info_menu, popup.menu)
        popup.setOnMenuItemClickListener { presenter.menuItemClicked(it.itemId) }
        popup.show()
    }

    override fun loadInfo(joinersTextRes: Int){
        title_ideaInfo.text = presenter.idea.title
        date_ideaInfo.text = df.format(presenter.idea.startDate.toDate())
        exp_date_ideaInfo.text = df.format(presenter.idea.RSVPDate.toDate())
        partecipantsN.text = presenter.idea.minPartecipants.toString() + " - " + presenter.idea.maxPartecipants.toString()
        location.text = presenter.idea.location
        joiners.text = presenter.idea.joiners.size.toString()
        joinersText.setText(joinersTextRes)
        joiners.setOnClickListener { showJoiners(presenter.idea.joiners) }
        joinersText.setOnClickListener { showJoiners(presenter.idea.joiners) }
        organizerName.text = presenter.idea.creatorName
        description.text = presenter.idea.description
        presenter.loadOrganizerInfo()
        presenter.setupImage()
        viewAdapterJoiners = JoinerCardAdapter(presenter.idea.joiners)
        recyclerViewJoiners.adapter = viewAdapterJoiners
        recyclerViewJoiners.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    override fun setIdeamImg(url: String){
        mainLayout.setBackgroundColor(Color.WHITE)
        Glide.with(ideaImage.context).load(url).centerCrop().into(ideaImage)
    }

    override fun setOrgImg(url: String) {
        Glide.with(organizerPic.context).load(url).placeholder(R.drawable.accenture_logo).centerCrop().into(organizerPic)
    }

    override fun hideIdeaImageView() {
        ideaImage.visibility = View.GONE
    }

    override fun startViewProfileActivity(uid: String) {
        val intent = Intent(this, ViewProfileActivity::class.java)
        intent.putExtra("uid", uid)
        startActivity(intent)
    }

    override fun setJoinButton() {
        joinBtn.setText(R.string.join)
        joinBtn.setBackgroundResource(R.drawable.join_button)
    }

    override fun setUnjoinButton() {
        joinBtn.setText(R.string.joined)
        joinBtn.setBackgroundResource(R.drawable.unjoin_button)
    }

    override fun setJoinersNumberAndNotifyAdapter() {
        joiners.text = presenter.idea.joiners.size.toString()
        viewAdapterJoiners.notifyDataSetChanged()
    }

    override fun showInsertComm() {
        insertUpdateLayout.visibility = View.VISIBLE
        viewManager = LinearLayoutManager(this, RecyclerView.VERTICAL, true)
        viewAdapterUpdates = UpdateAdapter(presenter.idea.updates, presenter.activityID, creator)
        recyclerViewComm.layoutManager = viewManager
        recyclerViewComm.adapter = viewAdapterUpdates
    }

    override fun setCreator(value: Boolean){
        creator = value
    }

    override fun notifyNewComment() {
        commentText.setText("")
        viewAdapterUpdates.notifyDataSetChanged()
        presenter.checkForUpdates()
    }

    override fun showUpdatesLayout() {
        updatesLayout.visibility = View.VISIBLE
    }

    override fun hideUpdatesLayout() {
        updatesLayout.visibility = View.GONE
    }

    override fun moveInterestBtn(){
        val params = interestBtn.layoutParams as ConstraintLayout.LayoutParams
        params.topMargin = Utils.dpToPx(this, 48f) ?: 36
        interestBtn.layoutParams = params
    }

    override fun hideJoinBtn() {
        joinBtn.visibility = View.INVISIBLE
    }

    override fun showJoiners(joiners: ArrayList<Joiner>){
        val builder = AlertDialog.Builder(this)
        val inflater = LayoutInflater.from(this)
        val dialogView = inflater.inflate(R.layout.joiners_dialog, null)
        val detailsRecyclerView = dialogView.findViewById(R.id.recyclerView_joinersDialog) as RecyclerView
        detailsRecyclerView.layoutManager = LinearLayoutManager(dialogView.context)
        detailsRecyclerView.adapter = JoinersAdapter(joiners)
        builder.setView(dialogView)
        val dialog = builder.create()
        dialog.show()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun hideEditBtn() {
        editButton.visibility = View.INVISIBLE
    }

    override fun showEditBtn() {
        editButton.visibility = View.VISIBLE
    }

    override fun closeActivity() {
        finish()
    }
}
