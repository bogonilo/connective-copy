package com.lorenzo.connectivecp.ideas.creation.target_chapter.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R

class SubChapterAdapter(private var dataset: ArrayList<String>): RecyclerView.Adapter<SubChapterViewHolder>() {

    var chapters = ArrayList<String>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SubChapterViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.subchapter_element, p0, false)
        return SubChapterViewHolder(v)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(p0: SubChapterViewHolder, p1: Int) {
        p0.bindView(dataset[p1], this)
    }

    fun retrieveChaptersSelected():ArrayList<String>{
        return chapters
    }

}