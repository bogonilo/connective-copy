package com.lorenzo.connectivecp.firebase

import com.lorenzo.connectivecp.model.Idea

interface FireCallback {

   interface User{
      fun onCallbackUser(value: com.lorenzo.connectivecp.model.User)
      fun onCallbackUserAuthButNoCompleteSignUp()
   }

   interface UserAdded{
      fun onCallbackUser(value: com.lorenzo.connectivecp.model.User)
   }

   interface NameProfilePic{
      fun onCallbackNameProPicUrl(name: String, url: String)
   }

   interface ProfilePic{
      fun onCallbackProPicUrl(value: String)
   }

   interface CardInfo{
      fun onCallbackProPicPosition(proPic: String, position: String)
   }

   interface SingleActivity{
      fun onCallbackActivity(value: Idea)
//      fun onCallbackActivity(value: Idea, id: String)
   }

   interface IdeasList{
      fun onCallbackIdeasList(value: HashMap<String, Idea>)
   }

   interface IdeasListAlternative{
//      fun onCallbackIdeasList(events: ArrayList<Idea>, IDs: ArrayList<String>)
      fun onCallbackIdeasList(events: ArrayList<Idea>)
   }

   interface IdeasListCreatedByUser{
      fun onCallbackIdeasListCreatedByUser(list: ArrayList<Idea>)
   }

   interface JoinedEventsList{
      fun onCallbackJoinedEventsList(list: ArrayList<String>)
   }

   interface Error{
      fun onCallbackError(error: String)
   }

   interface UserListBySkill{
      fun onCallbackUserList(usersUid: ArrayList<com.lorenzo.connectivecp.model.User>)
      fun onCallbackNoResults()
   }

   interface SkillsList{
      fun onCallbackSkillsList(skillsList: ArrayList<String>)
   }

   interface InterestsList{
      fun onCallbackInterestsList(interestsList: ArrayList<String>)
   }

   interface CareersLevel{
      fun onCallbackCareersLevelList(list: Array<String>)
   }

   interface MainChapters{
      fun onCallbackMainChaptersList(listAll: Array<String>, listParents: Array<String>)
   }

   interface SubChapters{
      fun onCallbackSubChaptersList(list: Array<String>)
   }

}