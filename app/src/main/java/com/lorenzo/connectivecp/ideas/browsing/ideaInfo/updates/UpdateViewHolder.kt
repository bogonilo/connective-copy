package com.lorenzo.connectivecp.ideas.browsing.ideaInfo.updates

import android.app.AlertDialog
import android.content.DialogInterface
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.Toast
import com.lorenzo.connectivecp.model.Update
import kotlinx.android.synthetic.main.ideas_comment_layout.view.*

class UpdateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindView(update: Update, adapter: UpdateAdapter){
        itemView.text_comment.setText(update.text)
        itemView.date_comment.setText(update.date)
        itemView.comment_layout.setOnLongClickListener { v ->
            val builder = AlertDialog.Builder(itemView.context)
            builder.setTitle("Delete this update?")
            builder.setPositiveButton("Yes",
                DialogInterface.OnClickListener { dialog, id ->
                    adapter.removeComment(update)
                    showToastCommentRemoved()
                })
                .setNegativeButton("Cancel",
                    DialogInterface.OnClickListener { dialog, id ->
                        // User cancelled the dialog
                    })
            val dialog = builder.create()
            dialog.show()
            true
        }

    }

    fun setCommentNotClickable(){
        itemView.comment_layout.isClickable = false
        itemView.comment_layout.isLongClickable = false
    }

    fun showToastCommentRemoved(){
        Toast.makeText(itemView.context, "Update deleted", Toast.LENGTH_LONG).show()
    }
}