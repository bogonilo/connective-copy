package com.lorenzo.connectivecp.profile.edit

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.lorenzo.connectivecp.R

class SkillsAdapter(private var dataset: ArrayList<String>, private var editable: Boolean): RecyclerView.Adapter<SkillsViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SkillsViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.skill_element_viewprofile, p0, false)
        return SkillsViewHolder(v)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(p0: SkillsViewHolder, p1: Int) {
        p0.bindView(dataset[p1], this, editable, p1)
    }

    fun remove(position: Int){
        dataset.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, dataset.size)
    }
}