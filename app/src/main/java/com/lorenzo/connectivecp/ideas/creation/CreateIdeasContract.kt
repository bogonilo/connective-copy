package com.lorenzo.connectivecp.ideas.creation

import android.content.Context
import android.content.Intent
import android.net.Uri
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

interface CreateIdeasContract {

    interface View {
        fun showProgBar()
        fun hideProgBar()
        fun finishActivity()
        fun requiredFieldsEmpty()
        fun galleryPic()
        fun cameraPic(): File?
        fun setImageFromCamera(file: File)
        fun clickAddPic()
        fun setImageView()
        fun setImgUri(uri: Uri)
        fun setUri(uri: Uri)
        fun setImageSet(bol: Boolean)
        fun getImageSet(): Boolean
        fun removePic()
        fun startDateTimeActForStartDate()
        fun startDateTimeActForRSVP()
        fun showRSVPField()
        fun hideRSVPField()
        fun showPartecipantsField()
        fun hidePartecipantsField()
        fun showTargetField()
        fun hideTargetField()
        fun setStartDate(date: Date)
        fun setEndDate(date: Date)
        fun setRSVPDate(date: Date)
        fun hideEndDate()
        fun notifyCategories()
        fun activateCreateBtn()
        fun deactCreateBtn()
        fun getEventTitle():String
        fun getDescription():String
        fun getLocation():String
        fun getSelectedCategories():ArrayList<String>
        fun disableCreateButtonDialog()
        fun enableCreateButtonDialog()
        fun disableCreateButton()
        fun enableCreateButton()
        fun getStringResValue(id: Int): String
        fun checkOfficeRadio(n: Int)
        fun showConfirmDialog()
        fun closeConfirmDialog()
    }

    interface Presenter {
        fun postIdeaClicked(
            title: String, description: String, category: ArrayList<String>, location: String,
            minP: Int, maxP: Int, pictureUri: Uri
        )
        fun picOptClicked(which: Int)
        fun requestPermissionResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray)
        fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, context: Context)
        fun getPicListItems(array: ArrayList<String>): Array<String>
        fun startDateClicked()
        fun switchTarget(isChecked: Boolean)
        fun switchRSVP(isChecked: Boolean)
        fun switchPartecipants(isChecked: Boolean)
        fun loadCategories()
        fun checkMandatoryFields()
        fun officeRadioButtonClicked(id: Int, checked: Boolean)
        fun checkOfficeTarget()
        fun createClicked(title: String, description: String, category: ArrayList<String>, location: String)
    }
}