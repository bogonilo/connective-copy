package com.lorenzo.connectivecp.profile.signup.skillsFragment.suggestion

import android.view.View
import kotlinx.android.synthetic.main.skills_suggestion_list.view.*

class SuggestionDialogViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    fun bindView(entry: String, adapter: SuggestionDialogAdapter) {
        itemView.skills_item.setText(entry)
        itemView.setOnClickListener {
            when(adapter.flag){
                0 -> adapter.passSkillToAct(entry)
                1 -> adapter.passSkillToFrag(entry)
                else -> adapter.passSkillToSkillsSearchFrag(entry)
            }
        }
    }
}