package com.lorenzo.connectivecp.profile.login

import com.lorenzo.connectivecp.model.User
import java.lang.Exception

interface LoginContract {

    interface View{
        fun progBarVisible()
        fun progBarInvisible()
        fun startMainActivity(user: User)
        fun displayAuthError(exception: Exception)
        fun setUpDomainSpinner()
    }

    interface Presenter{
        fun loginClicked(user:String, domain: String, pwd: String)
    }

}