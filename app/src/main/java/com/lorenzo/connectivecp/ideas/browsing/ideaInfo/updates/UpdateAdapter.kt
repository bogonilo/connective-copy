package com.lorenzo.connectivecp.ideas.browsing.ideaInfo.updates

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.model.Update
import com.lorenzo.connectivecp.firebase.FirestoreManager
import kotlin.collections.ArrayList

class UpdateAdapter(update: ArrayList<Update>, activityID: String, creator: Boolean) :
    RecyclerView.Adapter<UpdateViewHolder>() {

    private var updates = update
    private var creator = creator
    var activityID = activityID

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): UpdateViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.ideas_comment_layout, p0, false)
        return UpdateViewHolder(v)
    }

    override fun getItemCount(): Int {
        return updates.size
    }

    override fun onBindViewHolder(parent: UpdateViewHolder, position: Int) {
        parent.bindView(updates[position], this)
        if(!creator){
            parent.setCommentNotClickable()
        }
    }

    fun removeComment(update: Update) {
        var firestoreManager = FirestoreManager()
        firestoreManager.removeUpdateFromActivity(activityID, update)
        updates.remove(update)
        notifyDataSetChanged()
    }
}