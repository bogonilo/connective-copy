package com.lorenzo.connectivecp.profile.view.skills

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R

class SkillsProfileAdapter (private var dataset: ArrayList<String>): RecyclerView.Adapter<SkillsProfileViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): SkillsProfileViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.skill_element_viewprofile, p0, false)
        return SkillsProfileViewHolder (v)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(p0: SkillsProfileViewHolder , p1: Int) {
        p0.bindView(dataset[p1], this)
    }
}