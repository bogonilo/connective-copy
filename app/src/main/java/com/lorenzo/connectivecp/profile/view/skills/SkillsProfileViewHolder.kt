package com.lorenzo.connectivecp.profile.view.skills

import android.view.View
import kotlinx.android.synthetic.main.skill_element_viewprofile.view.*

class SkillsProfileViewHolder (itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    fun bindView(entry: String, adapter: SkillsProfileAdapter) {
        itemView.skill_txt_viewProfile.setText(entry)
    }
}