package com.lorenzo.connectivecp.model
//data class Recommendation(@SerializedName("data") internal val data: ArrayList<String>)

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Recommendation {

    @SerializedName("data")
    @Expose
    var data: List<String>? = null

    /**
     * No args constructor for use in serialization
     *
     */
    constructor() {}

    /**
     *
     * @param data
     */
    constructor(data: List<String>) : super() {
        this.data = data
    }

}