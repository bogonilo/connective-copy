package com.lorenzo.connectivecp.ideas.browsing.card

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.text.Html
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.Glide
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.ideas.browsing.card.joiners.JoinerCardAdapter
import com.lorenzo.connectivecp.ideas.browsing.joiner.JoinersAdapter
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.model.Joiner
import kotlinx.android.synthetic.main.ideas_card_new.view.*
import kotlinx.android.synthetic.main.ideas_card_new.view.cardView_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.cardView_picture_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.date_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.description_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.join_btn_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.joiners_number_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.joiners_text_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.location_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.name_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.picture_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.proPic_idea
import kotlinx.android.synthetic.main.ideas_card_new.view.title_idea
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class IdeaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var df = SimpleDateFormat("EEE d MMM, HH:mm", Locale.ENGLISH)
    var picUrl = ""
    private var joined = false

    fun bindView(model: Idea, activityID: String, adapter: IdeaCardAdapter, userUID: String, past: Boolean){
        joined = Joiner(userUID) in model.joiners
        if(joined)
            setUnjoinBtn()
        else
            setJoinBtn()
        if(userUID == model.creatorUid || past)
            hideJoinButton()
        else
            showJoinButton()
        itemView.name_idea.text = model.creatorName
        itemView.title_idea.text = model.title
        itemView.description_idea.text = model.description
        itemView.date_idea.text = df.format(model.startDate.toDate())
        itemView.location_idea.text = model.location
        itemView.joiners_number_idea.text = model.joiners.size.toString()
        if(model.joiners.size > 1)
            itemView.joiners_text_idea.setText(R.string.people_joined)
        else
            itemView.joiners_text_idea.setText(R.string.person_joined)
        itemView.title_idea.setOnClickListener { adapter.startEventInfo(model) }
        itemView.cardView_picture_idea.setOnClickListener { adapter.startEventInfo(model) }
        itemView.description_idea.setOnClickListener { adapter.startEventInfo(model)  }
        itemView.description_idea.post(Runnable {
            if(itemView.description_idea.lineCount > 2){
                val start = itemView.description_idea.layout.getLineStart(0)
                val end = itemView.description_idea.layout.getLineEnd(2)
                val toDisplay = itemView.description_idea.text.toString().substring(start, end-11)
                val seeMore = "<font color='#BEBEBE'>see more</font>"
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    itemView.description_idea.text = Html.fromHtml(toDisplay+"... "+seeMore, 0)
                }
                else
                    itemView.description_idea.text = Html.fromHtml(toDisplay+seeMore)
            }
        })
        itemView.name_idea.setOnClickListener{ adapter.startViewProfile(model.creatorUid) }
        itemView.proPic_idea.setOnClickListener{ adapter.startViewProfile(model.creatorUid)  }
        itemView.joiners_text_idea.setOnClickListener{ showJoiners(model.joiners) }
        itemView.joiners_number_idea.setOnClickListener { showJoiners(model.joiners) }
        val joinersListCard = itemView.recyclerViewJoinersCard
        joinersListCard.layoutManager = LinearLayoutManager(joinersListCard.context, LinearLayoutManager.HORIZONTAL, false)
        joinersListCard.adapter = JoinerCardAdapter(model.joiners)
        joinersListCard.setItemViewCacheSize(4)

        itemView.join_btn_idea.setOnClickListener {
            val joiner = Joiner(userUID)
            if (!joined) {
                joined = true
                model.joiners.add(joiner)
                setJoinersNumber(model.joiners.size.toString())
                adapter.addJoinedDB(activityID, joiner)
                setUnjoinBtn()
            } else {
                joined = false
                model.joiners.remove(joiner)
                setJoinersNumber(model.joiners.size.toString())
                adapter.removeJoinedDB(activityID, joiner)
                setJoinBtn()
            }
            (joinersListCard.adapter as JoinerCardAdapter).notifyDataSetChanged()
        }
    }

    fun setImg(value: String){
        picUrl = value
        Glide.with(itemView.cardView_idea.context).load(value).placeholder(R.drawable.accenture_logo).centerCrop().into(itemView.proPic_idea)
    }

    fun showJoiners(joiners: ArrayList<Joiner>){
        val builder = AlertDialog.Builder(itemView.context)
        val inflater = LayoutInflater.from(itemView.context)
        val dialogView = inflater.inflate(R.layout.joiners_dialog, null)
        val detailsRecyclerView = dialogView.findViewById(R.id.recyclerView_joinersDialog) as androidx.recyclerview.widget.RecyclerView
        detailsRecyclerView.layoutManager = LinearLayoutManager(dialogView.context)
        detailsRecyclerView.adapter = JoinersAdapter(joiners)
        builder.setView(dialogView)
        val dialog = builder.create()
        dialog.show()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    fun setUnjoinBtn(){
        itemView.join_btn_idea.setText(R.string.joined)
        itemView.join_btn_idea.setBackgroundResource(R.drawable.unjoin_button)
    }

    fun setJoinBtn(){
        itemView.join_btn_idea.setText(R.string.join)
        itemView.join_btn_idea.setBackgroundResource(R.drawable.join_button)
    }

    fun hidePicImageView(){
        itemView.cardView_picture_idea.visibility = View.GONE
    }

    fun loadIdeaPic(url: String){
        itemView.cardView_picture_idea.visibility = View.VISIBLE
        Glide.with(itemView.picture_idea.context).load(url).centerCrop().into(itemView.picture_idea)
    }

    fun setJoinersNumber(number: String){
        itemView.joiners_number_idea.text = number
    }

    fun hideJoinButton(){
        itemView.join_btn_idea.visibility = View.INVISIBLE
    }

    fun showJoinButton(){
        itemView.join_btn_idea.visibility = View.VISIBLE
    }
}
