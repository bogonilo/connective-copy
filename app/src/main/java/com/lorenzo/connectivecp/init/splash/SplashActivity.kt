package com.lorenzo.connectivecp.init.splash

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.init.landing.LandingActivityNew
import com.lorenzo.connectivecp.main.MainActivity
import com.lorenzo.connectivecp.profile.signup.SignUpFragmentsActivity

class SplashActivity : AppCompatActivity(), SplashContract.View {

    private lateinit var presenter: SplashContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter = SplashPresenter(this, this as Context)
        presenter.loadUserInfo()
    }

    override fun startLandingActivity(){
        startActivity(Intent(this, LandingActivityNew::class.java))
        finish()
    }

    override fun startMainActivity(user: User) {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("user", user)
        startActivity(intent)
        finish()
    }

    override fun startSkillsInterests(user: User) {
        val intent = Intent(this, SignUpFragmentsActivity::class.java)
        intent.putExtra("skillsFrag", true)
        intent.putExtra("user", user)
        startActivity(intent)
        finish()
    }

    override fun startSignUpFromSecondFrag() {
        val intent = Intent(this, SignUpFragmentsActivity::class.java)
        intent.putExtra("secondFrag", true)
        startActivity(intent)
        finish()
    }
}
