package com.lorenzo.connectivecp.recommendation

interface APIManagerInterface {

    fun recommendedList(eventIds: ArrayList<String>)

}