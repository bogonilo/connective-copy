package com.lorenzo.connectivecp.me

import android.content.Context
import android.os.Bundle
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.utils.SharedPreferenceManager
import com.firebase.ui.auth.AuthUI
import com.google.firebase.Timestamp
import java.util.*
import kotlin.collections.ArrayList

class MePresenter(view: MeContract.View, context: Context?) : MeContract.Presenter {

    private var mView = view
    var user = User("", "", "")
    private var firestoreManager = FirestoreManager()
    var upcomingEvents = ArrayList<Idea>()
    var sortedUpcomingEvents = ArrayList<Idea>()
    var pastEvents = ArrayList<Idea>()
    var sortedPastEvents = ArrayList<Idea>()
    var currTime = Timestamp(Calendar.getInstance().time)
    private var firebaseAuth = AuthUI.getInstance()
    private var preferenceManager = SharedPreferenceManager(context)
    var uid = preferenceManager.getIDFromPref()

    override fun getBundleData(bundle: Bundle) {
        if (bundle.getSerializable("user") != null) {
            user = bundle.getSerializable("user") as User
            mView.loadInfo()
        }
    }

    override fun getUserData() {
        firestoreManager.getUserFromFirestore(uid, object: FireCallback.User{
            override fun onCallbackUser(value: User) {
                user = value
                mView.loadInfo()
            }
            override fun onCallbackUserAuthButNoCompleteSignUp() {}
        })
    }

    override fun getCalendarListOfEvents() {
        firestoreManager.getJoinedList(uid, object : FireCallback.JoinedEventsList{
            override fun onCallbackJoinedEventsList(list: ArrayList<String>) {
                createAndOrderSubLists(list)
            }
        })
    }

    fun createAndOrderSubLists(eventsList: ArrayList<String>){
        upcomingEvents.clear()
        sortedUpcomingEvents.clear()
        pastEvents.clear()
        sortedPastEvents.clear()
        mView.passDataToFragment()
        var calendarItems = 0
        for (eventId in eventsList){
            firestoreManager.getIdea(eventId, object : FireCallback.SingleActivity {
                override fun onCallbackActivity(value: Idea) {
                    calendarItems++
                    if(value.startDate.seconds > currTime.seconds){
                        if(!upcomingEvents.contains(value)){
                            upcomingEvents.add(value)
                        }
                    }
                    else{
                        if(!pastEvents.contains(value)){
                            pastEvents.add(value)
                        }
                    }
                    if(upcomingEvents.size > 0)
                        mView.hideEmptyUpcoming()
                    else
                        mView.showEmptyUpcoming()
                    if(pastEvents.size > 0)
                        mView.hideEmptyPast()
                    else
                        mView.showEmptyPast()
                    if(calendarItems == eventsList.size){
                        sortedUpcomingEvents = ArrayList(upcomingEvents.sortedWith(compareBy({it.startDate.seconds})))
                        sortedPastEvents = ArrayList(pastEvents.sortedWith(compareBy({it.startDate.seconds})))
                        mView.passDataToFragment()
                    }
                }
            })
        }
    }

    override fun tabSelected(position: Int?) {
        if(position == 0)
            mView.displayUpcoming()
        else
            mView.displayPast()
    }

    override fun viewProfileClicked() {
        mView.startViewProfileActivity()
    }

    override fun signOutUser(context: Context?) {
        context?.let {
            firebaseAuth.signOut(it)
                .addOnSuccessListener {
                    mView.startLandingActivity()
                }
                .addOnFailureListener {
                    mView.failedLogOut()
                }
        }
    }

    override fun settingsClicked(index: Int) {
        when(index){
            0->{
                mView.showLogOutDialog()
            }
            else->{}
        }
    }
}