package com.lorenzo.connectivecp.ideas.browsing

import android.content.Context
import android.util.Log
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.utils.EventsFilterEnum
import com.lorenzo.connectivecp.utils.SharedPreferenceManager
import com.lorenzo.connectivecp.utils.Utils
import com.google.firebase.Timestamp
import com.lorenzo.connectivecp.recommendation.APIManager
import com.lorenzo.connectivecp.recommendation.APIManagerInterface
import java.util.*
import kotlin.collections.ArrayList

class IdeasPresenter(view: IdeasContract.View, context: Context?) :
    IdeasContract.Presenter {

    private var mView = view
    var user = User("", "", "")
    private var firestoreManager = FirestoreManager()
    private var eventList = ArrayList<Idea>()
    private var orderedEventsList = ArrayList<Idea>()
    private var recommendedEventsList = ArrayList<String>()
    var currTime = Timestamp(Calendar.getInstance().time)
    private var preferenceManager = SharedPreferenceManager(context)
    private var eventsFilter = EventsFilterEnum.TIME
    private var apiManager = APIManager()

    override fun initialSet() {
        if (Utils.isStoragePermissionGranted(mView.getFragment(), mView.getFragment().context!!)) {
            try {

            } catch (e: Exception) {

            }
        }
    }

    override fun getIdeasList() {
        if(eventsFilter == EventsFilterEnum.RELEVANCE){
            createAndOrderList(buildRecommendedList(), eventsFilter)
        }
        else{
            firestoreManager.getIdeasListFromFirestore(object : FireCallback.IdeasListAlternative {
                override fun onCallbackIdeasList(events: ArrayList<Idea>) {
                    if(events.isNotEmpty()){
                        eventList.clear()
                        eventList.addAll(events)
                        createAndOrderList(events, eventsFilter)
                    }
                    else
                        mView.stopRefreshing()
//                createAndOrderList(events, EventsFilterEnum.RELEVANCE)
                }
            })
        }
    }

    fun createAndOrderList(eventsList: ArrayList<Idea>, eventsFilter: EventsFilterEnum) {
        Log.e("IdeasPresenter", "eventsFilter:"+eventsFilter.name)
        orderedEventsList.clear()
        var calendarItems = 0
        var upcomingEvents = ArrayList<Idea>()
        var pastEvents = ArrayList<Idea>()
        for (event in eventsList) {
            calendarItems++
            if (event.startDate.seconds > currTime.seconds) {
                if (!upcomingEvents.contains(event)) {
                    if(eventsFilter == EventsFilterEnum.TIME)
                        upcomingEvents.add(event)
                    else{
//                        if(event.categories.intersect(user.interests).isNotEmpty() && (event.target.contains(user.office) || event.target.isEmpty() ))
                            upcomingEvents.add(event)
                    }
                }
            } else {
                if (!pastEvents.contains(event)) {
                    if(eventsFilter == EventsFilterEnum.TIME)
                        pastEvents.add(event)
                    else{
//                        if(event.categories.intersect(user.interests).isNotEmpty() && (event.target.contains(user.office) || event.target.isEmpty() ))
                            pastEvents.add(event)
                    }
                }
            }
            if (calendarItems == eventsList.size) {
                if(eventsFilter == EventsFilterEnum.RELEVANCE){
                    orderedEventsList.addAll(ArrayList(upcomingEvents))
                    orderedEventsList.addAll(ArrayList(pastEvents))
                }
                else{
                    orderedEventsList.addAll(ArrayList(upcomingEvents.sortedWith(compareBy({ it.startDate.seconds }))))
                    orderedEventsList.addAll(ArrayList(pastEvents.sortedWith(compareByDescending({ it.startDate.seconds }))))
                }
                mView.setAdapter(orderedEventsList)
            }
        }
    }

    override fun getEventsOrdering() {
        eventsFilter = preferenceManager.getEventsFilterFromPref()
    }

    override fun setEventsFilter(type: EventsFilterEnum) {
        eventsFilter = type
    }

    override fun getUserData() {
        firestoreManager.getUserFromFirestore(user.uid, object: FireCallback.User{
            override fun onCallbackUser(value: User) {
                user = value
            }
            override fun onCallbackUserAuthButNoCompleteSignUp() {}
        })
    }

    override fun getRecommendations() {
        apiManager.getRecommendations(preferenceManager.getIDFromPref(), object : APIManagerInterface{
            override fun recommendedList(eventIds: ArrayList<String>) {
                recommendedEventsList.addAll(eventIds)
            }
        })
    }

    override fun buildRecommendedList(): ArrayList<Idea>{
        val list = ArrayList<Idea>()
        val tempList = ArrayList(eventList)
        for(id in recommendedEventsList){
            tempList.forEach{
                if(it.id == id) {
                    list.add(it)
//                    tempList.remove(it)
                }
            }
        }
        return list
    }
}