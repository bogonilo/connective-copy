package com.lorenzo.connectivecp.me.calendar

import android.view.View
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.model.Idea
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.calendar_item.view.*
import java.text.SimpleDateFormat

class CalendarItemViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    var df = SimpleDateFormat("EEE, MMM d, yyyy HH:mm")

    fun bindView(idea: Idea, userUid: String, adapter: CalendarItemAdapter) {
        itemView.title_calendarItem.setText(idea.title)
        itemView.date_calendarItem.setText(df.format(idea.startDate.toDate()))
        itemView.location_calendarItem.setText(idea.location)
        var creator = (idea.creatorUid == userUid)
        if(creator){
            itemView.status_calendarItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_hoster, 0,0,0)
            itemView.status_calendarItem.setText(R.string.status_hosting)
        }
        else{
            itemView.status_calendarItem.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_going, 0,0,0)
            itemView.status_calendarItem.setText(R.string.status_joines)
        }
        if (idea.pictureUrl.isEmpty()){
            itemView.cardImg_calendarItem.visibility = View.GONE
        }
        else{
            itemView.cardImg_calendarItem.visibility = View.VISIBLE
            Glide.with(itemView.img_calendarItem.context).load(idea.pictureUrl).centerCrop().into(itemView.img_calendarItem)
        }
        itemView.layout_calendarItem.setOnClickListener {
            adapter.startEventInfo(idea)
        }
    }
}
