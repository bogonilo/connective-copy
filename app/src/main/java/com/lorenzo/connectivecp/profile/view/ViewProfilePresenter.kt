package com.lorenzo.connectivecp.profile.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.utils.SharedPreferenceManager
import com.lorenzo.connectivecp.utils.Utils
import com.firebase.ui.auth.AuthUI

class ViewProfilePresenter(view: ViewProfileContract.View, context: Context):
    ViewProfileContract.Presenter {

    private var mView = view
    private var firestoreManager = FirestoreManager()
    var user = User("", "", "")
    private var firebaseAuth = AuthUI.getInstance()
    private var sharedPreferenceManager = SharedPreferenceManager(context)

    override fun getUserFromFirestore(uid: String) {
        firestoreManager.getUserFromFirestore(uid, object: FireCallback.User{
            override fun onCallbackUser(value: User) {
                user = value
                mView.loadInfo()
                mView.notifyAdapter()
            }
            override fun onCallbackUserAuthButNoCompleteSignUp() {}
        })
    }

    override fun getUserData() {
        getUserFromFirestore(sharedPreferenceManager.getIDFromPref())
    }

    override fun activtiyResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(requestCode == Utils.PROFILE_EDIT_CODE)
        {
            if(resultCode == Activity.RESULT_OK){
                user = data!!.getSerializableExtra("user") as User
                mView.loadInfo()
            }
        }
    }

    override fun getIntentData(intent: Intent) {
        if(intent.getBooleanExtra("local", false)){
            getUserData()
        }
        else{
            if(intent.getStringExtra("uid") != null){
                val uid  = intent.getStringExtra("uid")
                mView.editProfileBtnInvisible()
                getUserFromFirestore(uid)
            }
        }
    }

    override fun signOutUser(context: Context) {
        firebaseAuth.signOut(context)
            .addOnSuccessListener {
                mView.startLandingActivity()
            }
            .addOnFailureListener {
                mView.failedLogOut()
            }
    }

}