package com.lorenzo.connectivecp.profile.signup.step_one

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.signup.SignUpFragmentsActivity
import com.lorenzo.connectivecp.profile.signup.step_two.Fragment2SignUp
import com.lorenzo.connectivecp.view.TextInputField
import java.lang.Exception

class Fragment1SignUp : Fragment(), Fragment1Contract.View {

    private lateinit var nextBtn:Button
    private lateinit var email: TextInputField
    private lateinit var pwd: TextInputField
    private lateinit var spinnerDomains: Spinner
    private lateinit var progBar: ProgressBar
    private var email_domain = "accenture.com"
    private lateinit var presenter: Fragment1Contract.Presenter
    private var fragment2SignUp = Fragment2SignUp()
    private lateinit var fm: FragmentManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.signup_frag1, container, false)
        nextBtn = view.findViewById(R.id.nextFragBtn1)
        email = view.findViewById(R.id.email_signUp2)
        pwd = view.findViewById(R.id.password_signUp2)
        progBar = view.findViewById(R.id.progressBar_frag1)
        spinnerDomains = view.findViewById(R.id.spinnerDomains_signUp)
        return view
    }

    override fun onStart() {
        super.onStart()
        fm = fragmentManager!!
        hideProgressBar()
        (activity as SignUpFragmentsActivity).setTitleText(0)
        presenter = Fragment1Presenter(this, context)
        setUpDomainSpinner()
        presenter.setButtonStatus(email.text.toString(), pwd.text.toString())
        nextBtn.setOnClickListener { presenter.nextBtnClicked(email.text.toString(), email_domain, pwd.text.toString()) }
        pwd.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { presenter.onTextChangedPwd(s, email.text.toString()) }
        })
        email.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) { }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { presenter.onTextChangedEmail(s, pwd.text.toString()) }
        })
    }

    override fun setUpDomainSpinner() {
        ArrayAdapter.createFromResource(context!!, R.array.domains_array, R.layout.custom_spinner)
            .also { adapter ->
                adapter.setDropDownViewResource(R.layout.custom_spinner)
                spinnerDomains.adapter = adapter
            }
        spinnerDomains.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View,
                position: Int, id: Long
            ) {
                email_domain = parent.getItemAtPosition(position).toString()
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    override fun passwordNotValid() {
        Toast.makeText(context, "Password must have at least 8 character including Upper case, lower case and a number", Toast.LENGTH_LONG).show()
    }

    override fun emailNotValid(){
        email.shakeView(context)
    }

    override fun passwordsDifferent() {
        Toast.makeText(context, "Password and Confirm Password are different", Toast.LENGTH_LONG).show()
    }

    override fun setArgumentsBundle(bundle: Bundle){
        fragment2SignUp.arguments = bundle
    }

    override fun nextPage(){
        SignUpFragmentsActivity.nextFragment(fm, fragment2SignUp)
    }

    override fun switchOnSignUpBtn() {
        nextBtn.setBackgroundResource(R.drawable.shape_signup)
    }

    override fun switchOffSignUpBtn() {
        nextBtn.setBackgroundResource(R.drawable.shape_signup_unselected)
    }

    override fun hideProgressBar() {
        progBar.visibility = View.INVISIBLE
    }

    override fun showProgressBar() {
        progBar.visibility = View.VISIBLE
    }

    override fun authError(exception: Exception?) {
        Toast.makeText(context, "Authentication failed: " + exception!!.message, Toast.LENGTH_LONG).show()
    }

    override fun authErrorUserCollision() {
        Toast.makeText(context, "Authentication failed user already existent", Toast.LENGTH_LONG).show()
    }

    override fun invalidEmail() {
        email.shakeView(context)
        Toast.makeText(context, R.string.invalid_email, Toast.LENGTH_LONG).show()
    }

    override fun activeButtonClick() {
        nextBtn.isClickable = true
    }

    override fun disableButtonClick() {
        nextBtn.isClickable = false
    }
}
