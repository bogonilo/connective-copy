package com.lorenzo.connectivecp.profile.login

import android.content.Context
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.utils.SharedPreferenceManager
import com.google.firebase.auth.FirebaseAuth

class LoginPresenter(view: LoginContract.View, activity: LoginActivity, context: Context): LoginContract.Presenter {

    private var mView = view
    private var auth = FirebaseAuth.getInstance()
    private var firestoreManager = FirestoreManager()
    private var activity = activity
    private var preferenceManager = SharedPreferenceManager(context)

    override fun loginClicked(user: String, domain: String, pwd: String) {
        mView.progBarVisible()
        if(user.isNotEmpty() && pwd.isNotEmpty()){
            auth.signInWithEmailAndPassword(user+'@'+domain, pwd)
                .addOnCompleteListener(activity) { task ->
                    if (task.isSuccessful) {
                        auth.currentUser?.let {
                            preferenceManager.updateIDinPreferences(it.uid)
                            firestoreManager.getUserFromFirestore(it.uid, object: FireCallback.User {
                                override fun onCallbackUser(value: User) {
                                    mView.progBarInvisible()
                                    mView.startMainActivity(value)
                                }
                                override fun onCallbackUserAuthButNoCompleteSignUp() {}
                            })
                        }
                    } else {
                        mView.progBarInvisible()
                        mView.displayAuthError(task.exception!!)
                    }
                }
        }
        else{
            mView.progBarInvisible()
        }
    }
}