package com.lorenzo.connectivecp.profile.signup

import android.content.Intent
import com.lorenzo.connectivecp.model.User
import com.google.firebase.auth.FirebaseAuth

class SignUpFragmentsPresenter(view: SignUpFragmentsContract.View) :
    SignUpFragmentsContract.Presenter {

    private var auth = FirebaseAuth.getInstance()
    private var mView = view
    var user = User("", "", "")

    override fun getEmailUser(): String {
        auth.currentUser?.let {
            return it.email.toString()
        }
        return ""
    }

    override fun checkReceivingIntent(intent: Intent) {
        if (intent.getBooleanExtra("secondFrag", false)) {
            mView.startWithSecondFrag()
        }
        if(intent.getBooleanExtra("skillsFrag", false)) {
            user = intent.getSerializableExtra("user") as User
            mView.startFromSkills()
        }
    }
}