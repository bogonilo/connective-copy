package com.lorenzo.connectivecp.profile.signup.skillsFragment

import android.content.Intent

interface SkillsContract {

    interface View{
        fun notifyAdapterSuggestions()
        fun notifyAdapterSkills()
        fun clearText()
        fun activateNextBtn()
        fun deactivateNextBtn()
        fun startInterestFrag()
        fun addSkillsToBundle(skills: ArrayList<String>)
        fun cancelDialog()
        fun shakeText()
        fun finishActivityAndPassResult()
        fun receiveData(skill: String)
    }

    interface Presenter{
        fun nextBtnClicked()
        fun loadSkillsList()
        fun receiveIntentWithSkills(intent: Intent)
        fun dialogTextChange(s: CharSequence?, count: Int)
        fun dialogBackPressed(keyCode: Int): Boolean
        fun checkSkillsNumber()
        fun dialogTextEnter(actionId: Int, text: String): Boolean
        fun doneBtnClicked()
        fun addNewSkill(skill: String)
    }
}