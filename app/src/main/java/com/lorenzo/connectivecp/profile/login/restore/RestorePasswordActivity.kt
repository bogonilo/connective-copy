package com.lorenzo.connectivecp.profile.login.restore

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.lorenzo.connectivecp.R
import java.util.regex.Pattern


class RestorePasswordActivity : AppCompatActivity(),
    RestorePasswordContract.View {

    private lateinit var email: EditText
    private lateinit var textRestore: TextView
    private lateinit var restorePwdBtn: Button
    private lateinit var presenter: RestorePasswordPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restore_password)
        email = findViewById(R.id.email_restore)
        restorePwdBtn = findViewById(R.id.createNewPwd_btn)
        textRestore = findViewById(R.id.text_RestorePwd)
        presenter = RestorePasswordPresenter(this)
        val pattern = Pattern.compile("^(.+)@(.+)$")
        restorePwdBtn.isEnabled = email.text.toString().isNotEmpty() && pattern.matcher(email.text.toString()).matches()

        email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                restorePwdBtn.isEnabled = p0.toString().isNotEmpty() && pattern.matcher(p0.toString()).matches()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    override fun onStart() {
        super.onStart()
        restorePwdBtn.setOnClickListener(View.OnClickListener {
            presenter.restoreBtnClick(email.text.toString())
        })
    }

    override fun onPasswordResetSuccessful() {
        textRestore.setText(R.string.create_new_pwd_2)
        restorePwdBtn.visibility = View.INVISIBLE
    }

    override fun onPasswordResetError() {
        Toast.makeText(
            this,
            "Problem resetting password, check your email address.",
            Toast.LENGTH_LONG
        ).show()
    }
}
