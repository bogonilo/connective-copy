package com.lorenzo.connectivecp.skills.results

import android.content.Intent
import android.view.View
import com.lorenzo.connectivecp.R
import com.bumptech.glide.Glide
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.profile.view.ViewProfileActivity
import kotlinx.android.synthetic.main.skills_search_result.view.*

class ResultViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    fun bindView(user: User) {
        itemView.name_skillsSearchResult.text = user.name
        itemView.position_skillsSearchResult.text = user.position
        itemView.layoutSkillsSearchResult.setOnClickListener {
            val intent = Intent(itemView.context, ViewProfileActivity::class.java)
            intent.putExtra("uid", user.uid)
            itemView.context.startActivity(intent)
        }
        Glide.with(itemView.context).load(user.profileImgUrl).placeholder(R.drawable.accenture_logo).centerCrop()
            .into(itemView.imageView_skillsSearchResult)
    }
}