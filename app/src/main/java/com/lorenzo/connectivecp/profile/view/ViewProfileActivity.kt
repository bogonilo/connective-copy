package com.lorenzo.connectivecp.profile.view

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.init.landing.LandingActivityNew
import com.lorenzo.connectivecp.profile.edit.EditProfileActivity
import com.lorenzo.connectivecp.profile.view.skills.SkillsProfileAdapter
import com.lorenzo.connectivecp.utils.Utils
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager

class ViewProfileActivity : AppCompatActivity(), ViewProfileContract.View {

    override fun startLandingActivity() {
        startActivity(Intent(this, LandingActivityNew::class.java))
        finishAffinity()
    }

    override fun failedLogOut() {
        Toast.makeText(this, R.string.toast_failed_logout, Toast.LENGTH_LONG).show()
    }

    private lateinit var name: TextView
    private lateinit var office: TextView
    private lateinit var position: TextView
    private lateinit var chapter: TextView
    private lateinit var skillsRecyclerView: RecyclerView
    private lateinit var interestsRecyclerView: RecyclerView
    private lateinit var skillsAdapter: RecyclerView.Adapter<*>
    private lateinit var interestsAdapter: RecyclerView.Adapter<*>
    private lateinit var email: TextView
    private lateinit var proPic: ImageView
    private lateinit var editProfile: TextView
    private lateinit var presenter: ViewProfilePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_profile)
        name = findViewById(R.id.name_viewProfile)
        office = findViewById(R.id.officeLocation_viewProfile)
        position = findViewById(R.id.position_viewProfile)
        proPic = findViewById(R.id.proPic_viewProfile)
        editProfile = findViewById(R.id.edit_viewProfile)
        chapter = findViewById(R.id.chapter_viewProfile)
        skillsRecyclerView = findViewById(R.id.recyclerView_skills_viewProfile)
        interestsRecyclerView = findViewById(R.id.recyclerView_interests_viewProfile)
        email = findViewById(R.id.emailAddr_viewProfile)
        presenter = ViewProfilePresenter(this, applicationContext)
        presenter.getIntentData(intent)
        editProfile.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, EditProfileActivity::class.java)
            intent.putExtra("user", presenter.user)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivityForResult(intent, Utils.PROFILE_EDIT_CODE)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.activtiyResult(requestCode,resultCode,data)
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK)
        finish()
        super.onBackPressed()
    }

    override fun loadInfo(){
        name.setText(presenter.user.name)
        email.setText(presenter.user.email)
        office.setText(presenter.user.office)
        position.setText(presenter.user.position)
        Glide.with(proPic.context).load(presenter.user.profileImgUrl).placeholder(R.drawable.accenture_logo).centerCrop().into(proPic)
        chapter.setText(presenter.user.chapter)
        initRecyclerViews()
    }

    fun initRecyclerViews(){
        val layoutManagerSkills = FlexboxLayoutManager(this)
        layoutManagerSkills.flexDirection = FlexDirection.ROW
        val layoutManagerInterests = FlexboxLayoutManager(this)
        layoutManagerInterests.flexDirection = FlexDirection.ROW
        interestsAdapter = SkillsProfileAdapter(presenter.user.interests)
        interestsRecyclerView.adapter = interestsAdapter
        interestsRecyclerView.layoutManager = layoutManagerInterests
        skillsAdapter = SkillsProfileAdapter(presenter.user.skills)
        skillsRecyclerView.adapter = skillsAdapter
        skillsRecyclerView.layoutManager = layoutManagerSkills
        notifyAdapter()
    }

    override fun editProfileBtnInvisible(){
        editProfile.visibility = View.INVISIBLE
    }

    override fun notifyAdapter(){
        skillsAdapter.notifyDataSetChanged()
        interestsAdapter.notifyDataSetChanged()
    }
}
