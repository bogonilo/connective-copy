package com.lorenzo.connectivecp.ideas.browsing.card.joiners

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.joiner_card.view.*

class JoinerCardViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun setImg(value: String){
        Glide.with(itemView.context.applicationContext).load(value).placeholder(R.drawable.accenture_logo).centerCrop().into(itemView.imageJoinerCard)
    }
}