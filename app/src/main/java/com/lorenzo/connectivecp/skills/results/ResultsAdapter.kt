package com.lorenzo.connectivecp.skills.results

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.model.User

class ResultsAdapter (private var users: ArrayList<User>): RecyclerView.Adapter<ResultViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ResultViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.skills_search_result, p0, false)
        return ResultViewHolder(v)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(p0: ResultViewHolder, p1: Int) {
        p0.bindView(users[p1])
    }

}