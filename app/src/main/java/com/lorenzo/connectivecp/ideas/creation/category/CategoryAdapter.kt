package com.lorenzo.connectivecp.ideas.creation.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.ideas.creation.CreateIdeasActivity

class CategoryAdapter (private var dataset: ArrayList<String>, private var activty: CreateIdeasActivity):
    RecyclerView.Adapter<CategoryViewHolder>() {

    var categories = ArrayList<String>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CategoryViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.category_element, p0, false)
        return CategoryViewHolder(v)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(p0: CategoryViewHolder, position: Int) {
        p0.bindView(dataset[position],this, position)
    }

    fun sendFieldsCheckToActivity(){
        activty.checkMandatoryFields()
    }

    fun retrieveCategoriesSelected():ArrayList<String>{
        return categories
    }
}