package com.lorenzo.connectivecp.profile.signup.step_two

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import java.io.File

interface Fragment2Contract {

    interface View{
        fun nextPage()
        fun setArgumentsBundle(bundle: Bundle)
        fun nameFieldEmpty()
        fun setUri(uri: Uri)
        fun setImgUri(uri: Uri)
        fun clickAddPic()
        fun galleryPic()
        fun cameraPic(): File?
        fun setImageFromCamera(file: File)
        fun setImageView()
        fun startCrop(uri: Uri)
        fun setUriPathEverything(uri: Uri, path: String)
        fun switchOnNextButton()
        fun switchOffNextButton()
        fun setRadiusOnCard()
        fun unsetRadiusOnCard()
        fun removePic()
    }

    interface Presenter{
        fun nextBtnClicked(email: String, name: String, pathImg: String)
        fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, context: Context)
        fun requestPermissionResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray)
        fun picOptClicked(opt: Int)
        fun textChanged(s: CharSequence?)
        fun getPicListItems(array: ArrayList<String>): Array<String>
    }
}