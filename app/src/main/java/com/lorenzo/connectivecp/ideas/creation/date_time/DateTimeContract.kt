package com.lorenzo.connectivecp.ideas.creation.date_time

import android.content.Intent

interface DateTimeContract {

    interface View {
        fun setStartText()
        fun setEndText()
        fun setStartDate(year: Int, month: Int, dayOfMonth: Int)
        fun setEndDate(year: Int, month: Int, dayOfMonth: Int)
        fun setStartTime(hour: Int, min: Int)
        fun setEndTime(hour: Int, min: Int)
        fun setEndTextActive()
        fun setEndDateInactive()
        fun setStartDateInactive()
        fun disableEndDate()
        fun setResultAndFinishStart(endDateBool: Boolean)
        fun setResultAndFinishRSVP()
        fun setStartContext()
        fun setRSVPContext()
        fun setStartDateActive()
        fun timeNotSettedProperly()
        fun startDateInThePast()
        fun getStartDateMillis():Long
        fun getEndDateMillis():Long
        fun getCurrDate():Long
    }

    interface Presenter {
        fun checkReceivingIntent(intent: Intent?)
        fun dateChanged(year: Int, month: Int, dayOfMonth: Int)
        fun timeChanged(hour: Int, min: Int)
        fun startDateClicked()
        fun endDateClicked()
        fun doneClicked()
        fun disableEndDate()
    }
}