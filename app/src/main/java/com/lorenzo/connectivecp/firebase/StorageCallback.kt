package com.lorenzo.connectivecp.firebase

interface StorageCallback {
    fun onCallbackImgUrl(value: String)
    fun onUploadError()
}