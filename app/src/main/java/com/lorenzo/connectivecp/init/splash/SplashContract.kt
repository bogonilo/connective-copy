package com.lorenzo.connectivecp.init.splash

import com.lorenzo.connectivecp.model.User

interface SplashContract {

    interface View{
        fun startLandingActivity()
        fun startMainActivity(user: User)
        fun startSkillsInterests(user: User)
        fun startSignUpFromSecondFrag()
//        fun updateIDinPreferences(id: String)
    }

    interface Presenter{
        fun loadUserInfo()
    }
}