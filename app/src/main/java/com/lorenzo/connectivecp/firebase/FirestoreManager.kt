package com.lorenzo.connectivecp.firebase

import android.net.Uri
import android.util.Log
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.model.Update
import com.lorenzo.connectivecp.model.Joiner
import com.lorenzo.connectivecp.model.User
import com.google.firebase.Timestamp
import com.google.firebase.firestore.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FirestoreManager {

    private val db = FirebaseFirestore.getInstance()

    fun addUserToCollection(userIn: User): HashMap<String, Any> {
        val user = createMapFromUser(userIn)
        // Add a new document with a generated ID
        db.collection("users")
            .add(userIn)
            .addOnSuccessListener { documentReference ->
                Log.v("FirestoreManager", "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.e("FirestoreManager", "Error adding document", e)
            }
        return user
    }

    fun addUserToCollection(userIn: User, callback: FireCallback.UserAdded): HashMap<String, Any> {
        val user = createMapFromUser(userIn)
        // Add a new document with a generated ID
        db.collection("users")
            .add(userIn)
            .addOnSuccessListener { documentReference ->
                callback.onCallbackUser(userIn)
                Log.v("FirestoreManager", "DocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.e("FirestoreManager", "Error adding document", e)
            }
        return user
    }

    fun addActivityToCollection(ideaIn: Idea, callback: FireCallback.SingleActivity) {
//        var activity = createMapFromActivity(ideaIn)
        db.collection("ideas")
            .add(ideaIn)
            .addOnSuccessListener { documentReference ->
                documentReference.update("id", documentReference.id)
                var idea = ideaIn
                idea.id = documentReference.id
                callback.onCallbackActivity(idea)
//                callback.onCallbackActivity(ideaIn, documentReference.id)
            }
            .addOnFailureListener { e ->
                Log.e("FirestoreManager", "Error adding document", e)
//                callback.onCallbackError(e.message!!)
            }
    }

    fun addUpdateToActivity(activityID: String, update: Update) {
        db.collection("ideas").document(activityID).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    document.reference.update("updates", FieldValue.arrayUnion(update))
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error updating document", e)
            }
    }

    fun addJoinedIdeaToUser(activityID: String, uid: String) {
        val docRef = db.collection("users").whereEqualTo("uid", uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document.size() != 0) {
                    document.documents[0].reference.update("joinedIdeas", FieldValue.arrayUnion(activityID))
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.e("FirestoreManager", "get failed with ", exception)
            }
    }

    fun addBioToUser(uid: String, bio: String){
        val docRef = db.collection("users").whereEqualTo("uid", uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document.size() != 0) {
                    document.documents[0].reference.update("bio", bio)
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.e("FirestoreManager", "get failed with ", exception)
            }
    }

    fun addJoinerToIdea(activityID: String, joiner: Joiner) {
        db.collection("ideas").document(activityID).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    document.reference.update("joiners", FieldValue.arrayUnion(joiner))
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error updating document", e)
            }
    }

    fun removeJoinerFromIdea(activityID: String, joiner: Joiner) {
        db.collection("ideas").document(activityID).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    document.reference.update("joiners", FieldValue.arrayRemove(joiner))
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error removing update ", e)
            }
    }

    fun removeJoinedIdeaToUser(activityID: String, uid: String) {
        val docRef = db.collection("users").whereEqualTo("uid", uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document.size() != 0) {
                    document.documents[0].reference.update("joinedIdeas", FieldValue.arrayRemove(activityID))
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.e("FirestoreManager", "get failed with ", exception)
            }
    }

    fun removeUpdateFromActivity(activityID: String, update: Update) {
        db.collection("ideas").document(activityID).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    document.reference.update("updates", FieldValue.arrayRemove(update))
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error removing update ", e)
            }
    }

    fun getIdeasListFromFirestore(callback: FireCallback.IdeasListAlternative) {
        val docRef = db.collection("ideas")
        var actList = ArrayList<Idea>()
//        var idList = ArrayList<String>()
//        var actList = HashMap<String, Idea>()
        docRef
            .orderBy("startDate", Query.Direction.ASCENDING)
            .get()
            .addOnSuccessListener { documents ->
                if (documents.size() != 0) {
                    for (document in documents) {
                        actList.add(createActivityFromDocument(document))
//                        idList.add(document.id)
//                        Log.e("getIdeasListFromFireste", document["title"].toString())
                    }
                    callback.onCallbackIdeasList(actList)
                } else {
                    Log.e("FirestoreManager", "No such document ideasList")
                    callback.onCallbackIdeasList(actList)
                }
            }
            .addOnFailureListener { exception ->
                Log.e("FirestoreManager", "get failed with ", exception)
            }
    }

    fun getActivityListCreatedByUser(uid: String, callback: FireCallback.IdeasListAlternative) {
        var actList = ArrayList<Idea>()
        var idsList = ArrayList<String>()
//        var actList = HashMap<String, Idea>()
        db.collection("ideas").whereEqualTo("creatorUid", uid)
            .orderBy("startDate")
            .get()
            .addOnSuccessListener { documents ->
                if (documents.size() != 0) {
                    for (document in documents){
                        actList.add(createActivityFromDocument(document))
                        idsList.add(document.id)
                    }
                    callback.onCallbackIdeasList(actList)
//                    callback.onCallbackIdeasList(actList, idsList)
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error getting documents", e)
            }
    }

    fun getJoinedList(uid: String, callback: FireCallback.JoinedEventsList){
        var list = ArrayList<String>()
        val docRef = db.collection("users").whereEqualTo("uid", uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document.size() != 0) {
                    callback.onCallbackJoinedEventsList(document.documents[0]["joinedIdeas"] as ArrayList<String>)
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.e("FirestoreManager", "get failed with ", exception)
            }
    }

    fun getProfilePicture(uid: String, callback: FireCallback.ProfilePic) {
        val docRef = db.collection("users").whereEqualTo("uid", uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document.size() != 0) {
                    callback.onCallbackProPicUrl(document.documents[0]["profileImgUrl"].toString())
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.e("FirestoreManager", "get failed with ", exception)
            }
    }

    fun getNameProPic(uid: String, callback: FireCallback.NameProfilePic){
        val docRef = db.collection("users").whereEqualTo("uid", uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document.size() != 0) {
                    callback.onCallbackNameProPicUrl(document.documents[0]["name"].toString(),
                        document.documents[0]["profileImgUrl"].toString())
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.e("FirestoreManager", "get failed with ", exception)
            }
    }

    fun getPicPositionCard(uid: String, callback: FireCallback.CardInfo) {
        val docRef = db.collection("users").whereEqualTo("uid", uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document.size() != 0) {
                    callback.onCallbackProPicPosition(
                        document.documents[0]["profileImgUrl"].toString(),
                        document.documents[0]["position"].toString()
                    )
                } else {
                    Log.e("FirestoreManager", "No such document of user")
                }
            }
            .addOnFailureListener { exception ->
                Log.e("FirestoreManager", "get failed with ", exception)
            }
    }

    fun getIdea(id: String, callback: FireCallback.SingleActivity){
        db.collection("ideas").document(id).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    callback.onCallbackActivity(createActivityFromDocument(document))
//                    callback.onCallbackActivity(createActivityFromDocument(document), document.reference.id)
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error deleting document", e)
            }
    }

    fun getUserFromFirestore(uid: String, callback: FireCallback.User) {
        var user = User("ERR", "", "")
        val docRef = db.collection("users").whereEqualTo("uid", uid)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document.size() != 0) {
                    user = createUserFromDocument(document.documents[0])
                    callback.onCallbackUser(user)
                } else {
                    Log.e("FirestoreManager", "No such document")
                    callback.onCallbackUserAuthButNoCompleteSignUp()
                }
            }
            .addOnFailureListener { exception ->
                Log.e("FirestoreManager", "get failed with ", exception)
            }
    }

    fun getUserListBySkill(skill: String, callback: FireCallback.UserListBySkill) {
        var userList = ArrayList<User>()
        db.collection("users")
            .whereArrayContains("skills", skill)
            .get()
            .addOnSuccessListener { documents ->
                if (documents.size() != 0) {
                    for (document in documents)
                        userList.add(createUserFromDocument(document))
                    callback.onCallbackUserList(userList)
                } else {
                    Log.e("FirestoreManager", "No such document")
                    callback.onCallbackNoResults()
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error getting documents", e)
            }
    }

    fun getSkillsList(callback: FireCallback.SkillsList) {
        var skills = ArrayList<String>()
        db.collection("skillsList").document("skills").get()
            .addOnSuccessListener { document ->
                skills = document["values"] as ArrayList<String>
                callback.onCallbackSkillsList(skills)
            }
            .addOnFailureListener {
                Log.e("FirestoreManager", "Error retrieving skills list")
            }
    }

    fun getInterestsList(callback: FireCallback.InterestsList){
        var interests = ArrayList<String>()
        db.collection("interestsList").document("interests").get()
            .addOnSuccessListener { document ->
                interests = document["values"] as ArrayList<String>
                callback.onCallbackInterestsList(interests)
            }
            .addOnFailureListener {
                Log.e("FirestoreManager", "Error retrieving interests list")
            }
    }

    fun loadCareersLevel(callback: FireCallback.CareersLevel){
        var careersLevel = ArrayList<String>()
        db.collection("fl_content")
            .whereEqualTo(FieldPath.of("_fl_meta_", "schema"), "careerLevel")
            .orderBy("careerLevel", Query.Direction.DESCENDING)
            .get()
            .addOnSuccessListener { document ->
                for(value in document)
                    careersLevel.add(value["careerLevelName"].toString())
                callback.onCallbackCareersLevelList(careersLevel.toSet().toTypedArray())
            }
            .addOnFailureListener {
                Log.e("FirestoreManager", "Error retrieving role list " + it.message)
            }
    }

    fun loadMainChapters(callback: FireCallback.MainChapters){
        var chapters = ArrayList<String>()
        var parentChapters = ArrayList<String>()
        db.collection("fl_content")
            .whereEqualTo(FieldPath.of("_fl_meta_", "schema"), "chapters")
            .get()
            .addOnSuccessListener { document ->
                for(value in document){
                    chapters.add(value["chapterName"].toString())
                    if(value["parentChapter"] as Boolean)
                        parentChapters.add(value["chapterName"].toString())
                }
                callback.onCallbackMainChaptersList(chapters.toSet().toTypedArray(), parentChapters.toTypedArray())
            }
            .addOnFailureListener {
                Log.e("FirestoreManager", "Error retrieving main chapters list " + it.message)
            }
    }

    fun loadSubChapters(parentChapter: String, callback: FireCallback.SubChapters){
        var subChapters = ArrayList<String>()
        db.collection("fl_content")
            .whereEqualTo("parentChapter", parentChapter)
            .get()
            .addOnSuccessListener { document ->
                for(value in document)
                    subChapters.add(value["subchapterName"].toString())
                callback.onCallbackSubChaptersList(subChapters.toSet().toTypedArray())
            }
            .addOnFailureListener {
                Log.e("FirestoreManager", "Error retrieving sub chapters list " + it.message)
            }
    }

    fun updateSkillsCollection(skills: ArrayList<String>) {
        db.collection("skillsList").document("skills").get()
            .addOnSuccessListener { document ->
                for (value in skills)
                    document.reference.update("values", FieldValue.arrayUnion(value))
            }
            .addOnFailureListener {
                Log.e("FirestoreManager", "Error updating skills")
            }
    }

    fun updateSkillsInterests(uid: String, skills: ArrayList<String>, interests: ArrayList<String>) {
        db.collection("users").whereEqualTo("uid", uid).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    document.documents.get(0).reference.update("skills", skills, "interests", interests)
                        .addOnCompleteListener {
                            Log.e("FirestoreManager", "skills interests info updated successfully ")
                        }
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error updating document", e)
            }
    }

    fun updateUserInfo(user: User) {
        db.collection("users").whereEqualTo("uid", user.uid).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    document.documents.get(0).reference.set(user).addOnCompleteListener {
                        Log.e("FirestoreManager", "User info updated successfully")
                    }
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error updating document", e)
            }
    }

    fun updatePicUrl(uid: String, uri: Uri?) {
        db.collection("users").whereEqualTo("uid", uid).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    document.documents.get(0).reference.update("profileImgUrl", uri.toString()).addOnCompleteListener {
                        Log.e("FirestoreManager", "Propicurl info updated successfully " + uri.toString())
                    }
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error updating document", e)
            }
    }

    fun deleteUserFromFirestore(user: User) {
        db.collection("users").whereEqualTo("uid", user.uid).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    document.documents[0].reference.delete()
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error deleting document", e)
            }
    }

    fun deleteActivityFromFirestore(id: String) {
        db.collection("ideas").document(id).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    for(joiner in (document["joiners"] as ArrayList<*>)){
                        removeEventIDFromUserJoinedIdeas(id, (joiner as HashMap<String,String>)["uid"])
                    }
                    document.reference.delete()
                    Log.e("FirestoreManager", "Idea correctly deleted")
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error deleting document", e)
            }
    }

    fun removeEventIDFromUserJoinedIdeas(eventID: String, uid: String?){
        db.collection("users").whereEqualTo("uid", uid).get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    document.documents[0].reference.update("joinedIdeas", FieldValue.arrayRemove(eventID))
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error deleting document", e)
            }
    }

    fun deleteActivityListCreatedByUser(uid: String) {
        var actList = HashMap<String, Idea>()
        db.collection("ideas").whereEqualTo("creatorUid", uid).get()
            .addOnSuccessListener { documents ->
                if (documents.size() != 0) {
                    for (document in documents)
                        document.reference.delete()
                } else {
                    Log.e("FirestoreManager", "No such document")
                }
            }
            .addOnFailureListener { e ->
                Log.w("FirestoreManager", "Error deleting document", e)
            }
    }

    fun createMapFromUser(user: User): HashMap<String, Any> {
        val userHash = HashMap<String, Any>()
        userHash["uid"] = user.uid
        userHash["email"] = user.email
        userHash["name"] = user.name
        userHash["phoneN"] = user.phoneN
        userHash["position"] = user.position
        userHash["office"] = user.office
        userHash["profileImgUrl"] = user.profileImgUrl
        userHash["chapter"] = user.chapter
        userHash["bio"] = user.bio
        userHash["skills"] = user.skills
        userHash["interests"] = user.interests
        userHash["joinedIdeas"] = user.joinedIdeas
        return userHash
    }

//    fun createMapFromActivity(idea: Idea): HashMap<String, Any> {
//        val activityHash = HashMap<String, Any>()
//        activityHash["title"] = idea.title
//        activityHash["description"] = idea.description
//        activityHash["categories"] = idea.categories
//        activityHash["startDate"] = idea.startDate
//        activityHash["expDate"] = idea.expDate
//        activityHash["location"] = idea.location
//        activityHash["creatorName"] = idea.creatorName
//        activityHash["creatorUid"] = idea.creatorUid
//        activityHash["minPartecipants"] = idea.minPartecipants
//        activityHash["maxPartecipants"] = idea.maxPartecipants
//        activityHash["target"] = idea.target
//        activityHash["updates"] = idea.updates
//        activityHash["joiners"] = idea.joiners
//        activityHash["pictureUrl"] = idea.pictureUrl
//        return activityHash
//    }

    fun createUserFromDocument(userDoc: DocumentSnapshot): User {
        val user = User(
            userDoc["uid"].toString(),
            userDoc["email"].toString(),
            userDoc["name"].toString(),
            userDoc["phoneN"].toString(),
            userDoc["position"].toString(),
            userDoc["office"].toString(),
            userDoc["profileImgUrl"].toString(),
            userDoc["chapter"].toString(),
            userDoc["bio"].toString(),
            userDoc["skills"] as ArrayList<String>,
            userDoc["interests"] as ArrayList<String>,
            userDoc["joinedIdeas"] as ArrayList<String>
        )
        return user
    }

    fun getArrayListFrom(doc: DocumentSnapshot): ArrayList<String> {
        return doc["values"] as ArrayList<String>
    }

    fun createActivityFromDocument(activityDoc: DocumentSnapshot): Idea {
        var updates = ArrayList<Update>()
        for (value in activityDoc.get("updates") as ArrayList<*>) {
            value as HashMap<String, *>
            val text = value["text"].toString()
            val date = value["date"] as Timestamp
            updates.add(Update(date.toString(), text))
        }
        var joiners = ArrayList<Joiner>()
        for (value in activityDoc.get("joiners") as ArrayList<*>) {
            value as HashMap<String, String>
            joiners.add(Joiner(value["uid"]!!))
        }
        var endTimestamp = Timestamp(0,0)
        if(activityDoc["endDateSet"] as Boolean){
            endTimestamp = activityDoc["endDate"] as Timestamp
        }
        var rsvpBool = false
        if(activityDoc["rsvpdateSet"] != null)
            rsvpBool == activityDoc["rsvpdateSet"] as Boolean
        var rsvpTimestamp = Timestamp(0,0)
        if(activityDoc["rsvpdate"] != null){
            rsvpTimestamp = activityDoc["endDate"] as Timestamp
        }
        var minpart = 1
        var maxpart = 100
        val activity = Idea(
            activityDoc["title"].toString(),
            activityDoc["description"].toString(),
            activityDoc["categories"] as ArrayList<String>,
            activityDoc["startDate"] as Timestamp,
            activityDoc["endDateSet"] as Boolean,
            endTimestamp,
            rsvpBool,
            rsvpTimestamp,
            activityDoc["location"].toString(),
            activityDoc["creatorName"].toString(),
            activityDoc["creatorUid"].toString(),
            minpart,
            maxpart,
            activityDoc["target"] as ArrayList<String>,
            updates,
            joiners,
            activityDoc["pictureUrl"].toString(),
            activityDoc["id"].toString()
        )
        return activity
    }
}