package com.lorenzo.connectivecp.ideas.creation.target_chapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.lorenzo.connectivecp.R

class TargetChapterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_target_chapter)
    }
}
