package com.lorenzo.connectivecp.profile.signup.step_one

import android.os.Bundle
import java.lang.Exception

interface Fragment1Contract {

    interface View{
        fun setUpDomainSpinner()
        fun passwordNotValid()
        fun passwordsDifferent()
        fun setArgumentsBundle(bundle: Bundle)
        fun nextPage()
        fun switchOnSignUpBtn()
        fun switchOffSignUpBtn()
        fun emailNotValid()
        fun showProgressBar()
        fun hideProgressBar()
        fun authErrorUserCollision()
        fun authError(exception: Exception?)
        fun invalidEmail()
        fun disableButtonClick()
        fun activeButtonClick()
    }

    interface Presenter{
        fun passwordValidation(password: String): Boolean
        fun emailValidation(email: String): Boolean
        fun nextBtnClicked(email: String, email_domain: String, pwd: String)
        fun onTextChangedPwd(s: CharSequence?, email: String)
        fun onTextChangedEmail(s: CharSequence?, pwd: String)
        fun setButtonStatus(email: String, pwd: String)
    }
}