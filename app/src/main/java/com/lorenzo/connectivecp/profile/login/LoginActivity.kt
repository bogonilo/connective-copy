package com.lorenzo.connectivecp.profile.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.main.MainActivity
import com.lorenzo.connectivecp.profile.login.restore.RestorePasswordActivity
import com.lorenzo.connectivecp.utils.SharedPreferenceManager
import java.lang.Exception

class LoginActivity : AppCompatActivity(), LoginContract.View {

    private lateinit var loginButton : Button
    private lateinit var email: EditText
    private lateinit var pwd: EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var forgotPwd: TextView
    private lateinit var spinnerDomains: Spinner
    private lateinit var presenter: LoginPresenter
    private var email_domain = "accenture.com"
    private lateinit var sharedPref: SharedPreferenceManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginButton = findViewById(R.id.login_button)
        email = findViewById(R.id.email_signUp2)
        pwd = findViewById(R.id.password_input)
        progressBar = findViewById(R.id.progressBar_login)
        forgotPwd = findViewById(R.id.forgotPassword_text)
        spinnerDomains = findViewById(R.id.spinnerDomains_logIn)
        sharedPref = SharedPreferenceManager(this)
        presenter = LoginPresenter(this, this, applicationContext)
    }

    override fun onStart() {
        super.onStart()
        setUpDomainSpinner()

        loginButton.setOnClickListener(View.OnClickListener {
            presenter.loginClicked(email.text.toString(), email_domain, pwd.text.toString())
        })

        forgotPwd.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, RestorePasswordActivity::class.java))
        })
    }

    override fun progBarVisible() {
        progressBar.visibility = View.VISIBLE
    }

    override fun progBarInvisible() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun startMainActivity(user: User) {
        var intent = Intent(this, MainActivity::class.java)
        intent.putExtra("user", user)
        this.startActivity(intent)
        finishAffinity()
    }

    override fun displayAuthError(exception: Exception) {
        Log.w("LoginActivity", "signInWithEmail:failure", exception)
        Toast.makeText(this, "Authentication failed.",
            Toast.LENGTH_SHORT).show()
    }

    override fun setUpDomainSpinner() {
        ArrayAdapter.createFromResource(this, R.array.domains_array, R.layout.custom_spinner)
            .also { adapter ->
            adapter.setDropDownViewResource(R.layout.custom_spinner)
            spinnerDomains.adapter = adapter
        }

        spinnerDomains.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View,
                position: Int, id: Long
            ) {
                email_domain = parent.getItemAtPosition(position).toString()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }
    }

}
