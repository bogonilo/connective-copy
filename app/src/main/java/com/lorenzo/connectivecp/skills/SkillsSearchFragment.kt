package com.lorenzo.connectivecp.skills

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.signup.skillsFragment.suggestion.SuggestionDialogAdapter
import com.lorenzo.connectivecp.skills.results.ResultsAdapter
import android.widget.*

class SkillsSearchFragment : Fragment(), SkillsSearchContract.View{

    private lateinit var searchText: EditText
    private lateinit var resultsRecycler: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var title: TextView
    private lateinit var presenter: SkillsSearchPresenter
    private lateinit var adapterResults: RecyclerView.Adapter<*>
    private lateinit var dialogView: View
    private lateinit var dialogText: EditText
    private lateinit var cancelDialog: TextView
    private lateinit var searchDialog: ImageButton
    private lateinit var expertsText: TextView
    private lateinit var noExpertsText: TextView
    private lateinit var imageSkillsSearch: ImageView
    private lateinit var dialog: AlertDialog
    private lateinit var dialogBuilder: AlertDialog.Builder
    private lateinit var recyclerViewSuggestionSkills: RecyclerView
    private lateinit var viewAdapterSuggestionSkills: RecyclerView.Adapter<*>
    private lateinit var titleLayoutParams: ConstraintLayout.LayoutParams
    var searching = false
    private lateinit var alternateTextLayoutParams: ConstraintLayout.LayoutParams

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_skills, container, false)
        presenter = SkillsSearchPresenter(this, context)
        searchText = view.findViewById(R.id.searchText_skills_frag)
        resultsRecycler = view.findViewById(R.id.resultsRecyclerView_skills_frag)
        progressBar = view.findViewById(R.id.progressBar_skills_frag)
        title = view.findViewById(R.id.title_skills_frag)
        imageSkillsSearch = view.findViewById(R.id.image_skills_frag)
        titleLayoutParams = title.layoutParams as ConstraintLayout.LayoutParams
        alternateTextLayoutParams = ConstraintLayout.LayoutParams(titleLayoutParams)
        alternateTextLayoutParams.width = ConstraintLayout.LayoutParams.WRAP_CONTENT
        expertsText = view.findViewById(R.id.expertsTextSkillsFrag)
        noExpertsText = view.findViewById(R.id.no_experts_skillsFrag)
        resultsRecycler.setHasFixedSize(true)
        resultsRecycler.setItemViewCacheSize(20)
        dialogView = View.inflate(context, R.layout.skills_search_dialog_signup, null)
        dialogText = dialogView.findViewById(R.id.searchText_skills_signUp)
        cancelDialog = dialogView.findViewById(R.id.cancel_dialog_skills)
        searchDialog = dialogView.findViewById(R.id.search_button_dialog_skills)
        recyclerViewSuggestionSkills = dialogView.findViewById(R.id.recyclerview_dialog_skills)
        recyclerViewSuggestionSkills.layoutManager = LinearLayoutManager(context)
        viewAdapterSuggestionSkills = SuggestionDialogAdapter(presenter.suggestedSkills, this)
        recyclerViewSuggestionSkills.adapter = viewAdapterSuggestionSkills
        resultsRecycler.layoutManager = LinearLayoutManager(context)
        adapterResults = ResultsAdapter(presenter.results)
        resultsRecycler.adapter = adapterResults
        dialogBuilder = AlertDialog.Builder(context, R.style.full_width_dialog).setView(dialogView)
        presenter.loadSkills()
        searchText.setOnClickListener {showSearchSkillsDialog()}
        dialogText.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                return presenter.dialogTextEnter(actionId, v?.text.toString())
            }})
        dialogText.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { presenter.dialogTextChange(s, count) }
        })
        cancelDialog.setOnClickListener {cancelDialog()}
        searchDialog.setOnClickListener { onSuggestionSelected(dialogText.text.toString()) }
        return view
    }

    fun showSearchSkillsDialog() {
        dialog = dialogBuilder.create()
        dialog.show()
        val window = dialog.getWindow()
        window?.let {
            it.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
            val wlp = it.getAttributes()
            wlp.gravity = Gravity.TOP
            wlp.dimAmount = 0.3f
            it.setAttributes(wlp)
        }
        dialog.setOnKeyListener { dialog, keyCode, event ->  presenter.backPressedInDialog(keyCode)}
        dialogText.requestFocus()
        val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        emptyFragment()
    }

    override fun cancelDialog() {
        val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        dialogText.setText("")
        presenter.suggestedSkills.clear()
        viewAdapterSuggestionSkills.notifyDataSetChanged()
        dialog.cancel()
        (dialogView.parent as ViewGroup).removeView(dialogView)
    }

    override fun onSuggestionSelected(skill: String) {
        cancelDialog()
        showProgressBar()
        searching = true
//        searchText.setText("")
        searchText.setText(skill + getString(R.string.experts_skills_search))
//        searchText.visibility = View.GONE
        imageSkillsSearch.visibility = View.INVISIBLE
        presenter.suggestedSkills.clear()
//        title.text = skill
//        title.layoutParams = alternateTextLayoutParams
        expertsText.text = skill + getString(R.string.experts_skills_search)
//        expertsText.visibility = View.VISIBLE
        presenter.textSelected(skill.toLowerCase())
    }

    fun receiveData(skill: String){
        onSuggestionSelected(skill)
    }

    override fun onSuggestionDeselected() {
        searching = false
        hideProgressBar()
        presenter.results.clear()
        adapterResults.notifyDataSetChanged()
    }

    override fun notifyNewDataResults() {
        adapterResults.notifyDataSetChanged()
        hideProgressBar()
    }

    override fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun setSearchingFalse() {
        searching = false
    }

    override fun emptyFragment() {
        searching = false
        searchText.setText("")
        presenter.suggestedSkills.clear()
        presenter.results.clear()
        notifyNewDataResults()
        expertsText.visibility = View.INVISIBLE
//        title.layoutParams = titleLayoutParams
//        title.setText(R.string.skills_frag_title)
//        searchText.visibility = View.VISIBLE
        imageSkillsSearch.visibility = View.VISIBLE
        hideNoExperts()
    }

    override fun notifyAdapterSuggestions() {
        viewAdapterSuggestionSkills.notifyDataSetChanged()
    }

    override fun hideNoExperts() {
        noExpertsText.visibility = View.INVISIBLE
    }

    override fun showNoExperts() {
        noExpertsText.visibility = View.VISIBLE
    }
}
