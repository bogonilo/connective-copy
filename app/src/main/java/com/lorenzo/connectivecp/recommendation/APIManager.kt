package com.lorenzo.connectivecp.recommendation

import android.util.Log
import com.lorenzo.connectivecp.model.Recommendation
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class APIManager {

    fun getRecommendations(uid: String, callback: APIManagerInterface) {
        val retrofit = APIClient.A.getClient()
        val service = retrofit.create(RecommendationAPI::class.java)
        val call1 = service.getRecommendation(uid)
        call1.enqueue(object : Callback<Recommendation> {
            override fun onResponse(call: Call<Recommendation>, response: Response<Recommendation>) {
                val recomms = response.body() as Recommendation
//                Log.e("IdeasPresenter", "response: " + recomms.data)
                val recommendedEventsList = ArrayList<String>()
                recomms.data?.forEach {
                    recommendedEventsList.add(it)
                }
                callback.recommendedList(recommendedEventsList)
            }

            override  fun onFailure(call: Call<Recommendation>, t: Throwable) {
                Log.e("IdeasPresenter", "failure " + t.message)
                call.cancel()
            }
        })
    }

    fun setParticipation(uid: String, eventId: String, participation: Int){
        val retrofit = APIClient.A.getClient()
        val service = retrofit.create(RecommendationAPI::class.java)
        val call1 = service.setParticipation(uid, eventId, participation)
        call1.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
//                val resp = response.body()
                Log.e("IdeasPresenter", "response: " + response.code())
            }

            override  fun onFailure(call: Call<Void>, t: Throwable) {
                Log.e("IdeasPresenter", "failure " + t.message)
                call.cancel()
            }
        })
    }

}