package com.lorenzo.connectivecp.ideas.browsing

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.ideas.browsing.card.IdeaCardAdapter
import com.lorenzo.connectivecp.ideas.creation.CreateIdeasActivity
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.utils.EventsFilterEnum
import com.lorenzo.connectivecp.utils.Utils

class IdeasFragment : Fragment(), IdeasContract.View {

    private lateinit var newEvent: TextView
    private lateinit var interestingForYou: TextView
    private lateinit var allEvents: TextView
    private lateinit var activityRecyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var refreshLayout: SwipeRefreshLayout
    private var activityDataset = ArrayList<Idea>()
    private lateinit var presenter: IdeasPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.activity_ideas, container, false)
        newEvent = v.findViewById(R.id.newActivity_ideas)
        activityRecyclerView = v.findViewById(R.id.recyclerView_ideas)
        refreshLayout = v.findViewById(R.id.refreshLayoutEvents)
        allEvents = v.findViewById(R.id.all_events_ideas)
        interestingForYou = v.findViewById(R.id.recommended_ideas)
        presenter = IdeasPresenter(this, context)
        presenter.user = arguments!!.getSerializable("user") as User
        presenter.initialSet()
        presenter.getEventsOrdering()
        presenter.getRecommendations()
        viewManager = LinearLayoutManager(context)
        viewAdapter = IdeaCardAdapter(activityDataset, presenter.user.uid, presenter.user.name, context)
        activityRecyclerView.layoutManager = viewManager
        activityRecyclerView.adapter = viewAdapter
        newEvent.setOnClickListener { startCreateEvent() }
        refreshLayout.setOnRefreshListener {
            refreshLayout.isEnabled = false
            loadEvents()
        }
        allEvents.setOnClickListener {
            switchOnAllEvents()
            presenter.setEventsFilter(EventsFilterEnum.TIME)
            updateEvents()
        }
        interestingForYou.setOnClickListener {
            switchOnRecommended()
            presenter.setEventsFilter(EventsFilterEnum.RELEVANCE)
            updateEvents()
        }
        return v
    }

    private fun startCreateEvent(){
        val intent = Intent(context, CreateIdeasActivity::class.java)
        intent.putExtra("user", presenter.user)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivityForResult(intent, Utils.CREATE_ACTIVITY_CODE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadEvents()
    }

    override fun updateEvents() {
        refreshLayout.isRefreshing = true
        loadEvents()
    }

    override fun loadEvents() {
        presenter.getIdeasList()
    }

    override fun stopRefreshing() {
        refreshLayout.isRefreshing = false
        refreshLayout.isEnabled = true
    }

    override fun getFragment(): Fragment {
        return this
    }

    override fun setAdapter(ideas: ArrayList<Idea>) {
        activityDataset.clear()
        activityDataset.addAll(ideas)
        viewAdapter.notifyDataSetChanged()
        stopRefreshing()
    }

    override fun switchOnAllEvents() {
        allEvents.setBackgroundResource(R.drawable.filter_events_selected)
        interestingForYou.setBackgroundResource(R.drawable.filter_events_unselected)
    }

    override fun switchOnRecommended() {
        allEvents.setBackgroundResource(R.drawable.filter_events_unselected)
        interestingForYou.setBackgroundResource(R.drawable.filter_events_selected)
    }

    fun updateUserInPresenter(){
        presenter.getUserData()
    }
}
