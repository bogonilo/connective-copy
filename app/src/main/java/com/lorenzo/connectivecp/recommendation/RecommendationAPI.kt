package com.lorenzo.connectivecp.recommendation

import com.lorenzo.connectivecp.model.Recommendation
import retrofit2.Call;
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface RecommendationAPI {

    @FormUrlEncoded
    @POST("recommendations")
    fun getRecommendation(@Field("uid") uid: String):Call<Recommendation>

    @FormUrlEncoded
    @POST("participation")
    fun setParticipation(@Field("uid") uid: String, @Field("eventId") eventId: String,
                         @Field("participation") participation: Int): Call<Void>

}