package com.lorenzo.connectivecp.ideas.creation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.util.Log
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.firebase.StorageCallback
import com.lorenzo.connectivecp.firebase.StorageManager
import com.lorenzo.connectivecp.model.Joiner
import com.lorenzo.connectivecp.utils.Utils
import com.google.firebase.Timestamp
import java.util.*
import kotlin.collections.ArrayList

class CreateIdeasPresenter(view: CreateIdeasContract.View) :
    CreateIdeasContract.Presenter {

    private var mView = view
    var user = User("", "", "")
    var target = ""
    private var firestoreManager = FirestoreManager()
    private var storageManager = StorageManager()
    var categories = ArrayList<String>()
    var dateS = Calendar.getInstance()
    var dateE = Calendar.getInstance()
    var endDateSetted = false
    var dateRSVP = Calendar.getInstance()
    var RSVPDateSetted = false
    var joiners = ArrayList<Joiner>()
    var targetOffice = ArrayList<String>()

    override fun loadCategories() {
        firestoreManager.getInterestsList(object : FireCallback.InterestsList {
            override fun onCallbackInterestsList(interestsList: ArrayList<String>) {
                categories.addAll(interestsList)
                mView.notifyCategories()
            }
        })
    }

    override fun createClicked(title: String, description: String, category: ArrayList<String>, location: String) {
        mView.disableCreateButton()
        if (title.isEmpty() || description.isEmpty() || location.isEmpty() || category.size == 0) {
            mView.requiredFieldsEmpty()
            mView.enableCreateButtonDialog()
            mView.enableCreateButton()
        } else {
            mView.showConfirmDialog()
        }
    }

    override fun postIdeaClicked(title: String, description: String, category: ArrayList<String>, location: String,
        minP: Int, maxP: Int, pictureUri: Uri) {
        mView.disableCreateButtonDialog()
        mView.showProgBar()
        mView.closeConfirmDialog()
        if (pictureUri.toString().isNotEmpty()) {
            storageManager.addIdeaImgToFirebase(user.uid, pictureUri, object :
                StorageCallback {
                override fun onCallbackImgUrl(value: String) {
                    val activity = Idea(
                        title, description, category, Timestamp(dateS.time), endDateSetted,
                        Timestamp(dateE.time), RSVPDateSetted, Timestamp(dateRSVP.time), location, user.name,
                        user.uid, minP, maxP, targetOffice, ArrayList(), joiners, value, ""
                    )
                    firestoreManager.addActivityToCollection(activity, object : FireCallback.SingleActivity {
                        override fun onCallbackActivity(value: Idea) {
                            firestoreManager.addJoinedIdeaToUser(value.id, user.uid)
                            user.joinedIdeas.add(value.id)
                            mView.finishActivity()
                        }
                    })
                }

                override fun onUploadError() {
                    val activity = Idea(
                        title, description, category, Timestamp(dateS.time), endDateSetted,
                        Timestamp(dateE.time), RSVPDateSetted, Timestamp(dateRSVP.time), location, user.name,
                        user.uid, minP, maxP, targetOffice, ArrayList(), joiners, "", ""
                    )
                    firestoreManager.addActivityToCollection(activity, object : FireCallback.SingleActivity {
                        override fun onCallbackActivity(value: Idea) {
                            firestoreManager.addJoinedIdeaToUser(value.id, user.uid)
                            user.joinedIdeas.add(value.id)
                            mView.finishActivity()
                        }
                    })
                }
            })
        } else {
            val activity = Idea(
                title, description, category, Timestamp(dateS.time), endDateSetted,
                Timestamp(dateE.time), RSVPDateSetted, Timestamp(dateRSVP.time), location, user.name,
                user.uid, minP, maxP, targetOffice, ArrayList(), joiners, "", ""
            )
            firestoreManager.addActivityToCollection(activity, object : FireCallback.SingleActivity {
                override fun onCallbackActivity(value: Idea) {
                    firestoreManager.addJoinedIdeaToUser(value.id, user.uid)
                    user.joinedIdeas.add(value.id)
                    mView.finishActivity()
                }
            })
        }
    }

    override fun picOptClicked(which: Int) {
        when (which) {
            0 -> mView.cameraPic()?.let { mView.setImageFromCamera(it) }
            1 -> mView.galleryPic()
            2 -> mView.removePic()
            else -> Log.e("CreateIdeasPresenter", which.toString())
        }
    }

    override fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, context: Context) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Utils.GALLERY_REQUEST_CODE -> {
                    mView.setImageSet(true)
                    data?.data?.let { mView.setUri(it) }
                }
                Utils.CAMERA_REQUEST_CODE -> {
                    mView.setImageSet(true)
                    mView.setImageView()
                }
                Utils.SET_START_DATE_CODE -> {
                    manageReceivedStartEndDate(
                        data!!.getLongExtra("startDate", 0),
                        data.getLongExtra("endDate", 0),
                        data.getBooleanExtra("endBool", false)
                    )
                }
                Utils.SET_RSVP_CODE -> {
                    manageReceivedRSVPDate(data!!.getLongExtra("RSVPDate", 0))
                }
            }
        }
    }

    fun manageReceivedStartEndDate(start: Long, end: Long, endBool: Boolean) {
        dateS.timeInMillis = start
        dateE.timeInMillis = end
        endDateSetted = endBool
        if (endBool) {
            mView.setStartDate(dateS.time)
            mView.setEndDate(dateE.time)
        } else {
            mView.setStartDate(dateS.time)
            mView.hideEndDate()
        }
    }

    fun manageReceivedRSVPDate(start: Long) {
        RSVPDateSetted = true
        dateRSVP.timeInMillis = start
        mView.setRSVPDate(dateRSVP.time)
    }

    override fun requestPermissionResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mView.clickAddPic()
        }
    }

    override fun getPicListItems(array: ArrayList<String>): Array<String> {
        if (mView.getImageSet()) {
            array.add("Delete")
            return array.toArray(arrayOfNulls(array.size))
        } else
            return array.toArray(arrayOfNulls(array.size))
    }

    override fun startDateClicked() {
        mView.startDateTimeActForStartDate()
    }

    override fun switchTarget(isChecked: Boolean) {
        if (isChecked) {
            mView.showTargetField()
            checkOfficeTarget()
            targetOffice.clear()
            targetOffice.add(user.office)
        } else {
            mView.hideTargetField()
            targetOffice.clear()
        }
    }

    override fun switchPartecipants(isChecked: Boolean) {
        if (isChecked)
            mView.showPartecipantsField()
        else
            mView.hidePartecipantsField()
    }

    override fun switchRSVP(isChecked: Boolean) {
        if (isChecked)
            mView.showRSVPField()
        else
            mView.hideRSVPField()
    }

    override fun checkMandatoryFields() {
        if (mView.getEventTitle().isNotEmpty() && mView.getDescription().isNotEmpty() && mView.getLocation().isNotEmpty()
            && mView.getSelectedCategories().isNotEmpty()
        )
            mView.activateCreateBtn()
        else
            mView.deactCreateBtn()
    }

    override fun officeRadioButtonClicked(id: Int, checked: Boolean) {
        when (id) {
            R.id.officeTarget_gronigen -> {
                targetOffice.clear()
                targetOffice.add(mView.getStringResValue(R.id.officeTarget_gronigen))
            }
            R.id.officeTarget_coruna -> {
                targetOffice.clear()
                targetOffice.add(mView.getStringResValue(R.id.officeTarget_coruna))
            }
            R.id.officeTarget_malaga -> {
                targetOffice.clear()
                targetOffice.add(mView.getStringResValue(R.id.officeTarget_malaga))
            }
            else -> {
                targetOffice.clear()
                targetOffice.add(mView.getStringResValue(R.id.officeTarget_amsterdam))
            }
        }
    }

    override fun checkOfficeTarget() {
        when (user.office) {
            mView.getStringResValue(R.string.office_gro) -> {
                mView.checkOfficeRadio(1)
            }
            mView.getStringResValue(R.string.office_cor) -> {
                mView.checkOfficeRadio(2)
            }
            mView.getStringResValue(R.string.office_mal) -> {
                mView.checkOfficeRadio(3)
            }
            else -> {
                mView.checkOfficeRadio(0)
            }
        }
    }
}