package com.lorenzo.connectivecp.profile.login.restore

import com.google.firebase.auth.FirebaseAuth

class RestorePasswordPresenter(view: RestorePasswordContract.View):
    RestorePasswordContract.Presenter {

    private var mView = view
    private var auth = FirebaseAuth.getInstance()

    override fun restoreBtnClick(email: String){
        if(email.isNotEmpty())
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    mView.onPasswordResetSuccessful()
                }
                else
                    mView.onPasswordResetError()
            }
    }

}