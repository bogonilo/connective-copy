package com.lorenzo.connectivecp.skills

interface SkillsSearchContract {

    interface View{
        fun notifyNewDataResults()
        fun showProgressBar()
        fun hideProgressBar()
        fun setSearchingFalse()
        fun emptyFragment()
        fun cancelDialog()
        fun notifyAdapterSuggestions()
        fun onSuggestionSelected(skill: String)
        fun onSuggestionDeselected()
        fun showNoExperts()
        fun hideNoExperts()
    }

    interface Presenter{
        fun loadSkills()
        fun textSelected(skill: String)
        fun dialogTextChange(s: CharSequence?, count: Int)
        fun dialogBackPressed(keyCode: Int): Boolean
        fun dialogTextEnter(actionId: Int, text: String): Boolean
        fun backPressedInDialog(keyCode: Int):Boolean
    }
}