package com.lorenzo.connectivecp.recommendation

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal object APIClient {

    object A{
        fun getClient(): Retrofit {

            val client = OkHttpClient.Builder().build()

            val retrofit = Retrofit.Builder()
                    .baseUrl("http://192.168.1.218:8000/rest/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()

            return retrofit
        }
    }

}