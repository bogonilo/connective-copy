package com.lorenzo.connectivecp.profile.signup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.widget.TextView
import android.widget.Toast
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.signup.skillsFragment.SkillsFragment
import com.lorenzo.connectivecp.profile.signup.step_one.Fragment1SignUp
import com.lorenzo.connectivecp.profile.signup.step_two.Fragment2SignUp


class SignUpFragmentsActivity : AppCompatActivity(),
    SignUpFragmentsContract.View {

    private lateinit var fragmentManager: FragmentManager
    private lateinit var title: TextView
    private lateinit var step: TextView
    private var titles = emptyArray<String>()
    private var steps = emptyArray<String>()
    private var presenter = SignUpFragmentsPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_fragments)
        titles = resources.getStringArray(R.array.signup_titles)
        title = findViewById(R.id.title_SignUp2)
        step = findViewById(R.id.step_signup)
        steps = resources.getStringArray(R.array.signup_steps)
        fragmentManager = supportFragmentManager
        var ft = fragmentManager.beginTransaction()
        ft.add(R.id.fragContainer, Fragment1SignUp()).commit();
        presenter.checkReceivingIntent(intent)
    }

    fun setTitleText(page: Int){
        title.setText(titles[page])
        step.setText(steps[page])
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun startWithSecondFrag(){
        fragmentManager.popBackStack()
        Toast.makeText(this, "Complete your registration", Toast.LENGTH_LONG).show()
        var frag2 = Fragment2SignUp()
        var user: ArrayList<String> = ArrayList(7)
        user.add(presenter.getEmailUser())
        var bundle = Bundle()
        bundle.putStringArrayList("user", user)
        frag2.arguments = bundle
        var ft = fragmentManager.beginTransaction()
        ft.replace(R.id.fragContainer, frag2)
        ft.addToBackStack(null);
        ft.commit()
    }

    override fun startFromSkills(){
        fragmentManager.popBackStack()
        Toast.makeText(this, "Insert your skills and interests", Toast.LENGTH_LONG).show()
        var skillsFragment2 = SkillsFragment()
        var user: ArrayList<String> = ArrayList(7)
        user.add(presenter.getEmailUser())
        var bundle = Bundle()
        bundle.putSerializable("user", presenter.user)
        skillsFragment2.arguments = bundle
        var ft = fragmentManager.beginTransaction()
        ft.replace(R.id.fragContainer, skillsFragment2)
        ft.addToBackStack(null);
        ft.commit()
    }

    companion object{
        fun nextFragment(fm: FragmentManager, nextFrag: Fragment){
            var ft = fm!!.beginTransaction()
            ft.replace(R.id.fragContainer, nextFrag)
            ft.addToBackStack(null);
            ft.commit()
        }
    }
}
