package com.lorenzo.connectivecp.init.landing

import androidx.viewpager.widget.PagerAdapter
import android.view.View
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.lorenzo.connectivecp.R
import com.bumptech.glide.Glide

class LandingPagerAdapter(inflater: LayoutInflater): PagerAdapter() {

    private var layouts = arrayOf(R.layout.landing_frag2, R.layout.landing_frag3, R.layout.landing_frag4)
    private var inflater = inflater

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val page = inflater.inflate(layouts[position], null)
        (container as ViewPager).addView(page, 0)
        when(position){
            0->{
                val imageView = page.findViewById<ImageView>(R.id.imageFrag2Landing)
                Glide.with(imageView.context).load(R.drawable.illustration_01).into(imageView)
            }
            1->{
                val imageView = page.findViewById<ImageView>(R.id.imageFrag3Landing)
                Glide.with(imageView.context).load(R.drawable.skill).into(imageView)
            }
            2->{
                val imageView = page.findViewById<ImageView>(R.id.imageFrag4Landing)
                Glide.with(imageView.context).load(R.drawable.family).into(imageView)
            }
        }
        return page
    }

    override fun isViewFromObject(p0: View, p1: Any): Boolean {
        return p0 == p1
    }

    override fun getCount(): Int {
        return layouts.size
    }

    override fun destroyItem(container: ViewGroup, position: Int, any: Any) {
        container.removeView(any as View)
    }
}