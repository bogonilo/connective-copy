package com.lorenzo.connectivecp.view

import android.content.Context
import android.util.AttributeSet
import android.view.animation.AnimationUtils
import com.lorenzo.connectivecp.R
import com.google.android.material.textfield.TextInputEditText

class TextInputField: TextInputEditText {
    constructor(context: Context): super(context)
    constructor(context: Context, attrs: AttributeSet): super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defsAttr: Int): super(context, attrs, defsAttr)

    fun shakeView(context: Context?){
        val shake = AnimationUtils.loadAnimation(context, R.anim.shake)
        this.startAnimation(shake)
    }
}