package com.lorenzo.connectivecp.init.splash

import android.content.Context
import android.os.Handler
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.utils.SharedPreferenceManager
import com.google.firebase.auth.FirebaseAuth

class SplashPresenter(view: SplashContract.View, context: Context):
    SplashContract.Presenter {

    private var firestoreManager = FirestoreManager()
    private var auth: FirebaseAuth = FirebaseAuth.getInstance()
    private var mView = view
    private var preferenceManager = SharedPreferenceManager(context)

    override fun loadUserInfo(){
        val currUser = auth.currentUser
        if(currUser == null){
            //delay
            val handler = Handler()
            handler.postDelayed({ mView.startLandingActivity() }, 2500)
        }
        else{
//            mView.updateIDinPreferences(currUser.uid)
            preferenceManager.updateIDinPreferences(currUser.uid)
            firestoreManager.getUserFromFirestore(currUser.uid, object: FireCallback.User {
                override fun onCallbackUser(value: User) {
                    if(value.interests.size < 3 || value.skills.size < 3)
                        mView.startSkillsInterests(value)
                    else
                        mView.startMainActivity(value)
                }
                override fun onCallbackUserAuthButNoCompleteSignUp() {
                    mView.startSignUpFromSecondFrag()
                }
            })
        }
    }

}