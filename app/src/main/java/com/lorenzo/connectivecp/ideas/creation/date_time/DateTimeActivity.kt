package com.lorenzo.connectivecp.ideas.creation.date_time

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.lorenzo.connectivecp.R
import java.text.SimpleDateFormat
import java.util.*

class DateTimeActivity : AppCompatActivity(), DateTimeContract.View {

    private lateinit var startDateText: TextView
    private lateinit var endDateText: TextView
    private lateinit var calendarView: CalendarView
    private lateinit var timePicker: TimePicker
    private lateinit var startDate: Calendar
    private lateinit var endDate: Calendar
    private lateinit var currDate: Calendar
    private lateinit var done: Button
    private lateinit var cancelEndDate: ImageButton
    private lateinit var endDateLayout: LinearLayout
    private lateinit var title: TextView
    private lateinit var presenter: DateTimePresenter
    private var df = SimpleDateFormat("EEE, MMM d, yyyy")
    private var tf = SimpleDateFormat("HH:mm")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_date_time)
        startDateText = findViewById(R.id.startDate_text_dateTime)
        endDateText = findViewById(R.id.endDate_text_dateTime)
        calendarView = findViewById(R.id.calendarView_dateTime)
        timePicker = findViewById(R.id.timePicker_dateTime)
        startDate = Calendar.getInstance()
        currDate = Calendar.getInstance()
        endDate = startDate.clone() as Calendar
        done = findViewById(R.id.done_dateTime)
        title = findViewById(R.id.title_dateTime)
        cancelEndDate = findViewById(R.id.cancel_end_date_dateTime)
        endDateLayout = findViewById(R.id.end_date_layout)
        presenter = DateTimePresenter(this)
        presenter.checkReceivingIntent(intent)
    }

    override fun onStart() {
        super.onStart()
        timePicker.setIs24HourView(true)
        setStartText()
        calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->  presenter.dateChanged(year, month, dayOfMonth)}
        timePicker.setOnTimeChangedListener { view, hourOfDay, minute -> presenter.timeChanged(hourOfDay, minute)}
        endDateLayout.setOnClickListener { presenter.endDateClicked() }
        done.setOnClickListener { presenter.doneClicked() }
        cancelEndDate.setOnClickListener { presenter.disableEndDate() }
        startDateText.setOnClickListener { presenter.startDateClicked() }
    }

    override fun setStartText() {
        startDateText.setText("Start "+df.format(startDate.time)+" at " + tf.format(startDate.time))
    }

    override fun setEndText() {
        endDateText.setText("End "+df.format(endDate.time)+" at " + tf.format(endDate.time))
    }

    override fun setStartDate(year: Int, month: Int, dayOfMonth: Int) {
        startDate.set(Calendar.YEAR, year)
        startDate.set(Calendar.MONTH, month)
        startDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)
    }

    override fun setStartTime(hour: Int, min: Int) {
        startDate.set(Calendar.HOUR_OF_DAY, hour)
        startDate.set(Calendar.MINUTE, min)
    }

    override fun setEndDate(year: Int, month: Int, dayOfMonth: Int) {
        endDate.set(Calendar.YEAR, year)
        endDate.set(Calendar.MONTH, month)
        endDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)
    }

    override fun setEndTime(hour: Int, min: Int) {
        endDate.set(Calendar.HOUR_OF_DAY, hour)
        endDate.set(Calendar.MINUTE, min)
    }

    override fun setEndTextActive() {
        endDateLayout.setBackgroundResource(R.drawable.shape_date)
        cancelEndDate.setImageResource(R.drawable.ic_close_black_24dp)
        setEndText()
    }

    override fun setEndDateInactive() {
        endDateLayout.setBackgroundResource(R.drawable.shape_date_inactive)
    }

    override fun disableEndDate() {
        endDateLayout.setBackgroundResource(R.drawable.shape_date_inactive)
        cancelEndDate.setImageResource(R.drawable.ic_add_grey_24dp)
        endDateText.setText(R.string.add_end_date)
    }

    override fun setResultAndFinishStart(endDateBool: Boolean) {
        val intent = Intent()
        intent.putExtra("startDate", startDate.timeInMillis)
        intent.putExtra("endDate", endDate.timeInMillis)
        intent.putExtra("endBool", endDateBool)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun setResultAndFinishRSVP() {
        val intent = Intent()
        intent.putExtra("RSVPDate", startDate.timeInMillis)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun setRSVPContext() {
        title.setText(R.string.rsvp_title)
        endDateText.visibility = View.INVISIBLE
    }

    override fun setStartContext() {
        title.setText(R.string.sel_date_time)
    }

    override fun setStartDateInactive() {
        startDateText.setBackgroundResource(R.drawable.shape_date_inactive)
    }

    override fun setStartDateActive() {
        startDateText.setBackgroundResource(R.drawable.shape_date)
    }

    override fun timeNotSettedProperly() {
        Toast.makeText(this, "You've set an end date before the start date", Toast.LENGTH_LONG).show()
    }

    override fun startDateInThePast() {
        Toast.makeText(this, "You can't create events in the past", Toast.LENGTH_LONG).show()
    }

    override fun getEndDateMillis(): Long {
        return endDate.timeInMillis
    }

    override fun getStartDateMillis(): Long {
        return startDate.timeInMillis
    }

    override fun getCurrDate(): Long {
        return currDate.timeInMillis
    }
}
