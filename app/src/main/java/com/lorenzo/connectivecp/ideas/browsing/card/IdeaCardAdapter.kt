package com.lorenzo.connectivecp.ideas.browsing.card

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.lorenzo.connectivecp.model.Idea
import androidx.recyclerview.widget.RecyclerView
import android.view.*
import kotlin.collections.ArrayList
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.model.Joiner
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.ideas.browsing.ideaInfo.IdeaInfoActivity
import com.lorenzo.connectivecp.profile.view.ViewProfileActivity
import com.lorenzo.connectivecp.utils.Utils
import com.google.firebase.Timestamp
import com.lorenzo.connectivecp.recommendation.APIManager
import java.util.*

class IdeaCardAdapter(
    private val myDataset: ArrayList<Idea>, private var userUID: String, private var userName: String,
    private var mContext: Context?) : RecyclerView.Adapter<IdeaViewHolder>() {

    var firestoreManager = FirestoreManager()
    var apiManager = APIManager()
    private var currTime = Timestamp(Calendar.getInstance().time)

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): IdeaViewHolder {
        val v = LayoutInflater.from(p0.context)
            .inflate(R.layout.ideas_card_new, p0, false)
        return IdeaViewHolder(v)
    }

    override fun getItemCount(): Int {
        return myDataset.size
    }

    override fun onBindViewHolder(p0: IdeaViewHolder, p1: Int) {
        if(p1>=myDataset.size)
            return
        val model = myDataset[p1]
        val activityID = model.id
        p0.bindView(model, activityID, this, userUID, model.startDate.seconds < currTime.seconds)
        if (model.pictureUrl == "")
            p0.hidePicImageView()
        else {
            p0.loadIdeaPic(model.pictureUrl)
        }

        firestoreManager.getPicPositionCard(model.creatorUid, object : FireCallback.CardInfo {
            override fun onCallbackProPicPosition(proPic: String, position: String) {
                p0.setImg(proPic)
            }
        })
    }

    fun addJoinedDB(activityID: String, joiner: Joiner) {
        firestoreManager.addJoinerToIdea(activityID, joiner)
        firestoreManager.addJoinedIdeaToUser(activityID, userUID)
        apiManager.setParticipation(userUID, activityID, 1)
    }

    fun removeJoinedDB(activityID: String, joiner: Joiner) {
        firestoreManager.removeJoinerFromIdea(activityID, joiner)
        firestoreManager.removeJoinedIdeaToUser(activityID, userUID)
        apiManager.setParticipation(userUID, activityID, 0)
    }

    fun startViewProfile(uid: String) {
        val intentUserProfile = Intent(mContext, ViewProfileActivity::class.java)
        intentUserProfile.putExtra("uid", uid)
        intentUserProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        (mContext as Activity).startActivity(intentUserProfile)
    }

    fun startEventInfo(idea: Idea) {
        val intentIdeaInfo = Intent(mContext, IdeaInfoActivity::class.java)
        intentIdeaInfo.putExtra("ideaID", idea.id)
        intentIdeaInfo.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        (mContext as Activity).startActivityForResult(intentIdeaInfo, Utils.VIEW_EVENT_INFO)
    }
}