package com.lorenzo.connectivecp.ideas.creation.date_time

import android.content.Intent

class DateTimePresenter(view: DateTimeContract.View) :
    DateTimeContract.Presenter {

    private var mView = view
    var start = true
    var endActive = false
    var endSet = false
    var startContext = true

    override fun dateChanged(year: Int, month: Int, dayOfMonth: Int) {
        if (start) {
            mView.setStartDate(year, month, dayOfMonth)
            mView.setStartText()
        } else {
            mView.setEndDate(year, month, dayOfMonth)
            mView.setEndText()
        }
    }

    override fun timeChanged(hour: Int, min: Int) {
        if (start) {
            mView.setStartTime(hour, min)
            mView.setStartText()
        } else {
            mView.setEndTime(hour, min)
            mView.setEndText()
        }
    }

    override fun endDateClicked() {
//        if(!endActive){
        start = false
        mView.setEndTextActive()
        mView.setStartDateInactive()
        endActive = true
        endSet = true
//        }
//        else{
//            endActive = false
//            start = true
//            mView.disableEndDate()
//        }
    }

    override fun disableEndDate() {
        endActive = false
        endSet = false
        start = true
        mView.disableEndDate()
        mView.setStartDateActive()
    }

    override fun startDateClicked() {
        if (endActive) {
            endActive = false
            start = true
            mView.setEndDateInactive()
            mView.setStartDateActive()
        }
    }

    override fun doneClicked() {
        if(mView.getStartDateMillis() < mView.getCurrDate()){
            mView.startDateInThePast()
        }
        else if(mView.getEndDateMillis() < mView.getStartDateMillis() && endSet){
            mView.timeNotSettedProperly()
        }
        else{
            if (startContext)
                mView.setResultAndFinishStart(endSet)
            else
                mView.setResultAndFinishRSVP()
        }
    }

    override fun checkReceivingIntent(intent: Intent?) {
        intent?.let {
            if (it.getBooleanExtra("start", false)) {
                mView.setStartContext()
                startContext = true
            }
            if (it.getBooleanExtra("rsvp", false)) {
                mView.setRSVPContext()
                startContext = false
            }
        }
    }
}