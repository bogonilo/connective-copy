package com.lorenzo.connectivecp.ideas.creation.target_chapter.list

import android.view.View
import kotlinx.android.synthetic.main.subchapter_element.view.*

class SubChapterViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    var subChapters = ArrayList<String>()

    fun bindView(entry: String, adapter: SubChapterAdapter) {
        itemView.checkBox_subChapter.setText(entry)
    }
}