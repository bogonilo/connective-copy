package com.lorenzo.connectivecp.ideas.creation

import android.app.Activity
import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.lorenzo.connectivecp.model.User
import android.content.Intent
import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.ideas.creation.category.CategoryAdapter
import com.lorenzo.connectivecp.ideas.creation.date_time.DateTimeActivity
import com.lorenzo.connectivecp.model.Joiner
import com.lorenzo.connectivecp.utils.Utils
import com.bumptech.glide.Glide
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CreateIdeasActivity : AppCompatActivity(), CreateIdeasContract.View {

    private lateinit var title: EditText
    private lateinit var categoryrecyclerView: RecyclerView
    private lateinit var description: EditText
    private lateinit var startDate: TextView
    private lateinit var endDate: TextView
    private lateinit var rsvpDate: TextView
    private lateinit var location: EditText
    private lateinit var picture: ImageView
    private lateinit var picCard: CardView
    private lateinit var post: Button
    private lateinit var progressBar: ProgressBar
    private lateinit var switchRSVP: Switch
    private lateinit var switchPartecipants: Switch
    private lateinit var switchTarget: Switch
    private lateinit var officeTargetGroup: RadioGroup
    private lateinit var officeTargAms: RadioButton
    private lateinit var officeTargGro: RadioButton
    private lateinit var officeTargCor: RadioButton
    private lateinit var officeTargMal: RadioButton
    private lateinit var officesRadio: Array<RadioButton>
    private lateinit var rangePartecipants: CrystalRangeSeekbar
    private lateinit var addTarget: ImageButton
    private lateinit var minP: TextView
    private lateinit var maxP: TextView
    private var df = SimpleDateFormat("EEE, MMM d, yyyy")
    private var tf = SimpleDateFormat("HH:mm")
    private lateinit var presenter: CreateIdeasPresenter
    private lateinit var viewAdapterCategories: RecyclerView.Adapter<*>
    private var imageSet = false
    private var imageUri = Uri.parse("")
    private var imagePath = ""
    private var currDate = Calendar.getInstance()
    private lateinit var initialPicParams: FrameLayout.LayoutParams
    private lateinit var confirmDialogView: View
    private lateinit var dialog: AlertDialog
    private lateinit var createBtnDialog: Button
    private lateinit var cancelBtnConfirmDialog: Button
    private lateinit var dialogBuilder: AlertDialog.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_ideas)
        title = findViewById(R.id.title_createAct)
        categoryrecyclerView = findViewById(R.id.recyclerViewCategory_createIdea)
        description = findViewById(R.id.description_createAct)
        startDate = findViewById(R.id.date_createAct)
        endDate = findViewById(R.id.endDate_createAct)
        rsvpDate = findViewById(R.id.expDate_createAct)
        location = findViewById(R.id.location_createAct)
        post = findViewById(R.id.post_createAct)
        picture = findViewById(R.id.picImgView_create_ideas)
        picCard = findViewById(R.id.picture_create_ideas)
        progressBar = findViewById(R.id.progressBar_createActivity)
        switchRSVP = findViewById(R.id.exp_switch_createAct)
        switchPartecipants = findViewById(R.id.part_switch_createAct)
        switchTarget = findViewById(R.id.target_switch_createAct)
        officeTargetGroup = findViewById(R.id.officeTarget_group)
        officeTargAms = findViewById(R.id.officeTarget_amsterdam)
        officeTargGro = findViewById(R.id.officeTarget_gronigen)
        officeTargCor = findViewById(R.id.officeTarget_coruna)
        officeTargMal = findViewById(R.id.officeTarget_malaga)
        officesRadio = arrayOf(officeTargAms, officeTargGro, officeTargCor, officeTargMal)
        rangePartecipants = findViewById(R.id.rangeSeekbar_createAct)
        addTarget = findViewById(R.id.addTarget_createAct)
        minP = findViewById(R.id.textMinP_createAct)
        maxP = findViewById(R.id.textMaxP_createAct)
        presenter = CreateIdeasPresenter(this)
        presenter.user = intent.getSerializableExtra("user") as User
        presenter.joiners.add(Joiner(presenter.user.uid))
        presenter.targetOffice.add(presenter.user.office)
        presenter.checkOfficeTarget()
        presenter.loadCategories()
        val layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        viewAdapterCategories = CategoryAdapter(presenter.categories, this)
        categoryrecyclerView.layoutManager = layoutManager
        categoryrecyclerView.adapter = viewAdapterCategories
        startDate.text = "Start "+ df.format(currDate.time) + " at " +tf.format(currDate.time)
        initialPicParams = picture.layoutParams as FrameLayout.LayoutParams
        startDate.setOnClickListener {startDateTimeActForStartDate()}
        endDate.setOnClickListener { startDateTimeActForStartDate() }
        rsvpDate.setOnClickListener{startDateTimeActForRSVP()}
        picCard.setOnClickListener{createDialogForPic()}
        post.setOnClickListener{presenter.createClicked(title.text.toString(), description.text.toString(),
            getSelectedCategories(), location.text.toString())}
        switchTarget.setOnCheckedChangeListener { buttonView, isChecked -> presenter.switchTarget(isChecked) }
        switchPartecipants.setOnCheckedChangeListener{ buttonView, isChecked -> presenter.switchPartecipants(isChecked) }
        switchRSVP.setOnCheckedChangeListener{ buttonView, isChecked -> presenter.switchRSVP(isChecked) }
        rangePartecipants.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            minP.text = minValue.toString()
            maxP.text = maxValue.toString()
        }
        title.addTextChangedListener(object: TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {presenter.checkMandatoryFields()}
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        })
        description.addTextChangedListener(object: TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {presenter.checkMandatoryFields()}
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        })
        location.addTextChangedListener(object: TextWatcher{
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {presenter.checkMandatoryFields()}
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        })
        confirmDialogView = View.inflate(this, R.layout.confirm_event_creation_dialog, null)
        dialogBuilder = AlertDialog.Builder(this).setView(confirmDialogView)
        createBtnDialog = confirmDialogView.findViewById(R.id.button_create_confirmDialog)
        cancelBtnConfirmDialog = confirmDialogView.findViewById(R.id.button_cancel_confirmDialog)
        createBtnDialog.setOnClickListener {
            presenter.postIdeaClicked(title.text.toString(), description.text.toString(),
                getSelectedCategories(), location.text.toString(), minP.text.toString().toInt(),
                maxP.text.toString().toInt(), imageUri)
        }
        cancelBtnConfirmDialog.setOnClickListener {
            closeConfirmDialog()
        }
    }

    override fun closeConfirmDialog(){
        dialog.cancel()
        enableCreateButtonDialog()
        enableCreateButton()
    }

    fun createDialogForPic(){
        val listItems = presenter.getPicListItems(resources.getStringArray(R.array.pic_options).toCollection(ArrayList<String>()))
        val builder = AlertDialog.Builder(this, R.style.full_width_dialog)
        builder.setTitle("Choose an option:")
        builder.setItems(listItems) { dialog, which ->
            presenter.picOptClicked(which)
        }
        val dialog = builder.create()
        dialog.show()
        val window = dialog.window
        window?.let {
            it.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            it.setBackgroundDrawableResource(R.drawable.shape_location_dialog)
            val wlp = it.attributes
            wlp.gravity = Gravity.BOTTOM
            wlp.dimAmount = 0.3f
            it.attributes = wlp
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.activityResult(requestCode, resultCode, data, this)
    }

    override fun setUri(uri: Uri){
        imageUri = uri
        picture.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT)
        Glide.with(picCard.context).load(uri).centerCrop().into(picture)
        imagePath = Utils.getPath(this, imageUri)
    }

    override fun setImgUri(uri: Uri){
        imageUri = uri
    }

    override fun setImageSet(bol: Boolean){
        imageSet = bol
    }

    override fun getImageSet(): Boolean{
        return imageSet
    }

    override fun setImageView(){
        picture.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT)
        Glide.with(picCard.context).load(imageUri).centerCrop().into(picture)
    }

    override fun hideProgBar() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun showProgBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun finishActivity() {
        Toast.makeText(applicationContext, "Idea posted!", Toast.LENGTH_LONG).show()
        val intent = Intent()
        intent.putExtra("user", presenter.user)
        setResult(Activity.RESULT_OK, intent)
        progressBar.visibility = View.INVISIBLE
        finish()
    }

    override fun requiredFieldsEmpty() {
        Toast.makeText(this, getString(R.string.missing_fields_event_creation), Toast.LENGTH_LONG).show()
    }

    override fun clickAddPic() {
        picCard.performClick()
    }

    override fun galleryPic(){
        Utils.pickFromGallery(this)
    }

    override fun cameraPic(): File? {
        return Utils.pickFromCameraEventImg(this)
    }

    override fun setImageFromCamera(file: File) {
        imageUri = Uri.fromFile(file)
        imagePath = file.absolutePath
    }

    override fun removePic() {
        picture.layoutParams = initialPicParams
        picture.setImageResource(R.drawable.ic_add_black_24dp)
        imageUri = Uri.parse("")
        imagePath = ""
        imageSet = false
    }

    override fun startDateTimeActForStartDate() {
        val intent = Intent(this, DateTimeActivity::class.java)
        intent.putExtra("start", true)
        startActivityForResult(intent, Utils.SET_START_DATE_CODE)
    }

    override fun startDateTimeActForRSVP() {
        val intent = Intent(this, DateTimeActivity::class.java)
        intent.putExtra("rsvp", true)
        startActivityForResult(intent, Utils.SET_RSVP_CODE)
    }

    override fun hidePartecipantsField() {
        rangePartecipants.visibility = View.GONE
        minP.visibility = View.GONE
        maxP.visibility = View.GONE
    }

    override fun showPartecipantsField() {
        rangePartecipants.visibility = View.VISIBLE
        minP.visibility = View.VISIBLE
        maxP.visibility = View.VISIBLE
    }

    override fun hideRSVPField() {
        rsvpDate.visibility = View.GONE
    }

    override fun showRSVPField() {
        rsvpDate.visibility = View.VISIBLE
    }

    override fun hideTargetField() {
        officeTargetGroup.visibility = View.GONE
    }

    override fun showTargetField() {
        officeTargetGroup.visibility = View.VISIBLE
    }

    override fun setEndDate(date: Date) {
        endDate.visibility = View.VISIBLE
        endDate.text = "End "+ df.format(date) + " at " +tf.format(date)
    }

    override fun setStartDate(date: Date) {
        startDate.text = "Start "+ df.format(date) + " at " +tf.format(date)
    }

    override fun setRSVPDate(date: Date) {
        rsvpDate.text = df.format(date) + " at " +tf.format(date)
    }

    override fun hideEndDate() {
        endDate.visibility = View.GONE
    }

    override fun notifyCategories() {
        viewAdapterCategories.notifyDataSetChanged()
    }

    override fun activateCreateBtn() {
        post.setBackgroundResource(R.drawable.shape_signup)
    }

    override fun deactCreateBtn() {
        post.setBackgroundResource(R.drawable.shape_signup_unselected)
    }

    override fun getDescription(): String {
        return description.text.toString()
    }

    override fun getLocation(): String {
        return location.text.toString()
    }

    override fun getEventTitle():String{
        return title.text.toString()
    }

    override fun getSelectedCategories(): ArrayList<String> {
        return (viewAdapterCategories as CategoryAdapter).retrieveCategoriesSelected()
    }

    override fun disableCreateButtonDialog() {
        createBtnDialog.isClickable = false
    }

    override fun enableCreateButtonDialog() {
        createBtnDialog.isClickable = true
    }

    override fun getStringResValue(id: Int): String {
        return getString(id)
    }

    override fun checkOfficeRadio(n: Int) {
        officesRadio[n].isChecked = true
    }

    fun checkMandatoryFields(){
        presenter.checkMandatoryFields()
    }

    fun onOfficeRadioButtonClicked(view: View){
        if(view is RadioButton){
            presenter.officeRadioButtonClicked(view.id, view.isChecked)
        }
    }

    override fun showConfirmDialog() {
        dialog = dialogBuilder.create()
        with(dialog){
            show()
            window?.setBackgroundDrawable(android.graphics.drawable.ColorDrawable(android.graphics.Color.TRANSPARENT))
            setOnCancelListener { (confirmDialogView.parent as ViewGroup).removeView(confirmDialogView)
            enableCreateButton()
            }
        }
    }

    override fun disableCreateButton() {
        post.isClickable = false
    }

    override fun enableCreateButton() {
        post.isClickable = true
    }
}