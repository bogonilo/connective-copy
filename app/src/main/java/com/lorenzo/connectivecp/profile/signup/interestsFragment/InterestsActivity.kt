package com.lorenzo.connectivecp.profile.signup.interestsFragment

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.signup.interestsFragment.interestsList.InterestsAdapter
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent

class InterestsActivity : AppCompatActivity(), InterestsContract.View {

    private lateinit var presenter: InterestsPresenter
    private lateinit var progBar: ProgressBar
    private lateinit var done: Button
    private lateinit var recyclerViewInterests: RecyclerView
    private lateinit var viewAdapterInterests: RecyclerView.Adapter<*>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.interests_page_edit)
        progBar = findViewById(R.id.progressBar_interests2)
        presenter = InterestsPresenter(this)
        recyclerViewInterests = findViewById(R.id.recyclerView_interests_page2)
        done = findViewById(R.id.done_interestsFrag)
        val layoutManager = FlexboxLayoutManager(this)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.CENTER
        layoutManager.alignItems = AlignItems.STRETCH
        viewAdapterInterests = InterestsAdapter(presenter.interests, presenter.selectedInterests, this)
        presenter.getInterestsFromIntent(intent)
        recyclerViewInterests.layoutManager = layoutManager
        recyclerViewInterests.adapter = viewAdapterInterests
    }

    override fun onStart() {
        super.onStart()
        hideProgBar()
        done.setOnClickListener {presenter.doneFromActivity()}
    }

    override fun hideProgBar() {
        progBar.visibility = View.INVISIBLE
    }

    override fun showProgBar() {
        progBar.visibility = View.VISIBLE
    }

    override fun notEnough() {
        Toast.makeText(this, R.string.toast_interests_number, Toast.LENGTH_LONG).show()
    }

    override fun notifyAdapter() {
        viewAdapterInterests.notifyDataSetChanged()
    }

    fun interestsSelected(interests: ArrayList<String>){
        presenter.interests.addAll(interests)
        presenter.checkNOfInterests()
    }

    override fun activateDoneButton(){
        done.setBackgroundResource(R.drawable.shape_signup)
    }

    override fun deactDoneButton(){
        done.setBackgroundResource(R.drawable.shape_signup_unselected)
    }

    override fun startMainActivity() {
        //
    }

    override fun passSelectedToAdapter() {
//        (viewAdapterInterests as InterestsAdapter).addInterestsSelected(presenter.selectedInterests)
//        viewAdapterInterests.notifyDataSetChanged()
    }

    override fun finishActWithResult() {
        var intent = Intent()
        intent.putExtra("interests", presenter.selectedInterests)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
