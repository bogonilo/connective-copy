package com.lorenzo.connectivecp.profile.signup.step_two

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import com.lorenzo.connectivecp.utils.Utils
import java.lang.IndexOutOfBoundsException
import java.util.regex.Pattern

class Fragment2Presenter(view: Fragment2Contract.View) :
    Fragment2Contract.Presenter {

    private var mView = view
    private var imageSet = false

    override fun nextBtnClicked(email: String, name: String, pathImg: String) {
        if (!validateName(name))
            mView.nameFieldEmpty()
        else {
            val user: ArrayList<String> = ArrayList()
            user.add(email)
            user.add(name)
            user.add(pathImg)
            val bundle = Bundle()
            bundle.putStringArrayList("user", user)
            mView.setArgumentsBundle(bundle)
            mView.nextPage()
        }
    }

    override fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, context: Context) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Utils.GALLERY_REQUEST_CODE -> {
                    data?.let{
                        it.data?.let {
//                            mView.setUri(it)
                            mView.startCrop(it)
                        }
                    }
                }
                Utils.CAMERA_REQUEST_CODE -> {
                    mView.setImageView()
                }
                Utils.CROP_IMAGE ->{
                    data?.let{
                        mView.setUriPathEverything(it.getParcelableExtra("croppedImgUri") as Uri,
                            it.getStringExtra("croppedImgAbsPath"))
                        mView.setRadiusOnCard()
                    }
                    imageSet = true
                }
            }
        } else{
            mView.setImgUri(Uri.parse("android.resource://com.accenture.standapp/drawable/profile_photo"))
            mView.unsetRadiusOnCard()
        }
    }

    override fun requestPermissionResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mView.clickAddPic()
        }
    }

    override fun picOptClicked(opt: Int) {
        when (opt) {
            0 -> mView.cameraPic()?.let { mView.setImageFromCamera(it) }
            1 -> mView.galleryPic()
            2 -> {
                mView.removePic()
                imageSet = false
            }
        }
    }

    override fun textChanged(s: CharSequence?) {
        s?.let {
            if(validateName(it.toString())){
                mView.switchOnNextButton()
            }
            else
                mView.switchOffNextButton()
        }
    }

    override fun getPicListItems(array: ArrayList<String>): Array<String> {
        if(imageSet){
            array.add("Delete")
            return array.toArray(arrayOfNulls(array.size))
        }
        else
            return array.toArray(arrayOfNulls(array.size))
    }

    private fun validateName(name: String): Boolean{
        val NAME_REGEX = "^[a-zA-Z\\s]+"
        val pattern = Pattern.compile(NAME_REGEX)
        val matcher = pattern.matcher(name)
        try {
            return (matcher.matches() && (name.split(" ")[1].isNotEmpty()))
        }
        catch (e: IndexOutOfBoundsException){
            return false
        }
    }
}