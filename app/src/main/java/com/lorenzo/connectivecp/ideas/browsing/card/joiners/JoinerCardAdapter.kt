package com.lorenzo.connectivecp.ideas.browsing.card.joiners

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.model.Joiner

class JoinerCardAdapter(private val joiners: ArrayList<Joiner>) : RecyclerView.Adapter<JoinerCardViewHolder>() {

    private var firestoreManager = FirestoreManager()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): JoinerCardViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.joiner_card, p0, false)
        return JoinerCardViewHolder(v)
    }

    override fun getItemCount(): Int {
        if(joiners.size > 3)
            return 4
        else
            return joiners.size
    }

    override fun onBindViewHolder(p0: JoinerCardViewHolder, p1: Int) {
        firestoreManager.getProfilePicture(joiners[p1].uid, object : FireCallback.ProfilePic {
            override fun onCallbackProPicUrl(value: String) {
                p0.setImg(value)
            }
        })
    }
}