package com.lorenzo.connectivecp.profile.signup.interestsFragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.*
import com.lorenzo.connectivecp.R
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.main.MainActivity
import com.lorenzo.connectivecp.profile.signup.SignUpFragmentsActivity
import com.lorenzo.connectivecp.profile.signup.interestsFragment.interestsList.InterestsAdapter
import com.google.android.flexbox.*

class InterestsFragment: Fragment(), InterestsContract.View {

    private lateinit var presenter: InterestsPresenter
    private lateinit var progBar: ProgressBar
    private lateinit var done: Button
    private lateinit var recyclerViewInterests: RecyclerView
    private lateinit var viewAdapterInterests: RecyclerView.Adapter<*>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.interests_page2, container, false)
        progBar = view.findViewById(R.id.progressBar_interests2)
        presenter = InterestsPresenter(this)
        recyclerViewInterests = view.findViewById(R.id.recyclerView_interests_page2)
        done = view.findViewById(R.id.done_interestsFrag)
        val layoutManager = FlexboxLayoutManager(context)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.CENTER
        layoutManager.alignItems = AlignItems.STRETCH
        viewAdapterInterests = InterestsAdapter(presenter.interests, presenter.selectedInterests, this)
        recyclerViewInterests.layoutManager = layoutManager
        recyclerViewInterests.adapter = viewAdapterInterests
        presenter.loadInterests()
        return view
    }

    override fun onStart() {
        super.onStart()
        (activity as SignUpFragmentsActivity).setTitleText(4)
        hideProgBar()
        done.setOnClickListener { presenter.doneClicked(arguments!!) }
    }

    override fun hideProgBar() {
        progBar.visibility = View.INVISIBLE
    }

    override fun showProgBar() {
        progBar.visibility = View.VISIBLE
    }

    override fun notEnough() {
        Toast.makeText(context, R.string.toast_interests_number, Toast.LENGTH_LONG).show()
    }

    override fun notifyAdapter() {
        viewAdapterInterests.notifyDataSetChanged()
    }

    fun interestsSelected(interests: ArrayList<String>){
        presenter.interests.addAll(interests)
        presenter.checkNOfInterests()
    }

    override fun activateDoneButton(){
        done.setBackgroundResource(R.drawable.shape_signup)
    }

    override fun deactDoneButton(){
        done.setBackgroundResource(R.drawable.shape_signup_unselected)
    }

    override fun startMainActivity() {
        hideProgBar()
        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra("user", presenter.user)
        startActivity(intent)
        activity!!.finishAffinity()
    }

    override fun passSelectedToAdapter() {
        //TODO
    }

    override fun finishActWithResult() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}