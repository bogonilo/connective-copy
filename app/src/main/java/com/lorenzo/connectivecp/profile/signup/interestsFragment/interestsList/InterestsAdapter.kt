package com.lorenzo.connectivecp.profile.signup.interestsFragment.interestsList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.signup.interestsFragment.InterestsActivity
import com.lorenzo.connectivecp.profile.signup.interestsFragment.InterestsFragment

class InterestsAdapter (private var interests: ArrayList<String>, var interestsSelected: ArrayList<String>): RecyclerView.Adapter<InterestsViewHolder>() {

    private var act = InterestsActivity()
    private var frag = InterestsFragment()
    var isFrag = false

    constructor(interests: ArrayList<String>, interestsSelected: ArrayList<String>, act: InterestsActivity)
            : this(interests, interestsSelected) {
        this.act = act
        isFrag = false
    }

    constructor(interests: ArrayList<String>, interestsSelected: ArrayList<String>, frag: InterestsFragment)
            : this(interests, interestsSelected) {
        this.frag = frag
        isFrag = true
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): InterestsViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.category_element, p0, false)
        return InterestsViewHolder(v)
    }

    override fun getItemCount(): Int {
        return interests.size
    }

    override fun onBindViewHolder(p0: InterestsViewHolder, p1: Int) {
        p0.bindView(interests[p1], this)
    }

    fun giveSelectedToFrag(){
        frag.interestsSelected(interestsSelected)
    }

    fun giveSelectedToAct(){
        act.interestsSelected(interestsSelected)
    }

    fun addInterestsSelected(interests: ArrayList<String>){
        interestsSelected.clear()
        interestsSelected.addAll(interests)
    }
}