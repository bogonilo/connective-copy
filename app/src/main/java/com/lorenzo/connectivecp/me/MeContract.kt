package com.lorenzo.connectivecp.me

import android.content.Context
import android.os.Bundle

interface MeContract {

    interface View{
        fun loadInfo()
        fun displayUpcoming()
        fun displayPast()
        fun passDataToFragment()
        fun startViewProfileActivity()
        fun startLandingActivity()
        fun failedLogOut()
        fun hideEmptyUpcoming()
        fun hideEmptyPast()
        fun emptyAdaptersInFragments()
        fun signOut()
        fun showEmptyUpcoming()
        fun showEmptyPast()
        fun showLogOutDialog()
    }

    interface Presenter{
        fun getBundleData(bundle: Bundle)
        fun getCalendarListOfEvents()
        fun tabSelected(position: Int?)
        fun viewProfileClicked()
        fun signOutUser(context: Context?)
        fun settingsClicked(index: Int)
        fun getUserData()
    }
}