package com.lorenzo.connectivecp.profile.signup.skillsFragment

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.edit.SkillsAdapter
import com.lorenzo.connectivecp.profile.signup.skillsFragment.suggestion.SuggestionDialogAdapter
import com.lorenzo.connectivecp.view.TextInputField
import com.google.android.flexbox.AlignItems
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.google.android.flexbox.JustifyContent

class SkillsActivity : AppCompatActivity(), SkillsContract.View {

    private lateinit var addText: TextInputField
    private lateinit var nextBtn: Button
    private lateinit var presenter: SkillsPresenter
    private lateinit var recyclerViewSuggestionSkills: RecyclerView
    private lateinit var viewAdapterSuggestionSkills: RecyclerView.Adapter<*>
    private lateinit var recyclerViewSkills: RecyclerView
    private lateinit var viewAdapterSkills: RecyclerView.Adapter<*>
    private lateinit var dialogView: View
    private lateinit var dialogText: EditText
    private lateinit var cancelDialog: TextView
    private lateinit var dialog: AlertDialog
    private lateinit var dialogBuilder: AlertDialog.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.skills_page_edit)
        presenter = SkillsPresenter(this)
        addText = findViewById(R.id.addText_skills2)
        nextBtn = findViewById(R.id.nextBtn_skills2)
        dialogView = View.inflate(this, R.layout.skills_search_dialog_signup, null)
        dialogText = dialogView.findViewById(R.id.searchText_skills_signUp)
        recyclerViewSuggestionSkills = dialogView.findViewById(R.id.recyclerview_dialog_skills)
        recyclerViewSkills = findViewById(R.id.recyclerView_skills_page2)
        cancelDialog = dialogView.findViewById(R.id.cancel_dialog_skills)
        recyclerViewSuggestionSkills.layoutManager = LinearLayoutManager(this)
        viewAdapterSuggestionSkills =
            SuggestionDialogAdapter(
                presenter.suggestedSkills,
                this
            )
        recyclerViewSuggestionSkills.adapter = viewAdapterSuggestionSkills
        val layoutManager = FlexboxLayoutManager(this)
        layoutManager.flexDirection = FlexDirection.ROW
        layoutManager.justifyContent = JustifyContent.FLEX_START
        layoutManager.alignItems = AlignItems.STRETCH
        recyclerViewSkills.layoutManager = layoutManager
        viewAdapterSkills = SkillsAdapter(presenter.skills, true)
        recyclerViewSkills.adapter = viewAdapterSkills
        dialogBuilder = AlertDialog.Builder(this, R.style.full_width_dialog).setView(dialogView)
        presenter.receiveIntentWithSkills(intent)
        presenter.loadSkillsList()
        nextBtn.setText("Done")
        nextBtn.setOnClickListener { presenter.doneBtnClicked() }
        dialogText.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                return presenter.dialogTextEnter(actionId, v?.text.toString())
            }
        })
        dialogText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) { presenter.dialogTextChange(s, count) }
        })
        cancelDialog.setOnClickListener { cancelDialog()}
        addText.setOnClickListener {
            dialog = dialogBuilder.create()
            dialog.setOnKeyListener(object: DialogInterface.OnKeyListener{
                override fun onKey(dialog: DialogInterface?, keyCode: Int, event: KeyEvent?): Boolean { return presenter.dialogBackPressed(keyCode) }
            })
            dialog.show()
            val window = dialog.getWindow()
            window?.let{
                it.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)
                val wlp = it.getAttributes()
                wlp.gravity = Gravity.TOP
                wlp.dimAmount = 0.3f
                it.setAttributes(wlp)
            }
            dialogText.requestFocus()
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        }
        viewAdapterSkills.registerAdapterDataObserver(object: RecyclerView.AdapterDataObserver(){
            override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                presenter.checkSkillsNumber()
                super.onItemRangeChanged(positionStart, itemCount)
            }
        })
    }

    override fun cancelDialog() {
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        dialogText.setText("")
        presenter.suggestedSkills.clear()
        viewAdapterSuggestionSkills.notifyDataSetChanged()
        dialog.cancel()
        (dialogView.parent as ViewGroup).removeView(dialogView)
    }

    override fun notifyAdapterSuggestions() {
        viewAdapterSuggestionSkills.notifyDataSetChanged()
    }

    override fun notifyAdapterSkills() {
        viewAdapterSkills.notifyDataSetChanged()
    }

    override fun clearText() {
        addText.setText("")
    }

    override fun activateNextBtn() {
        nextBtn.setBackgroundResource(R.drawable.shape_signup)
    }

    override fun deactivateNextBtn() {
        nextBtn.setBackgroundResource(R.drawable.shape_signup_unselected)
    }

    override fun shakeText() {
        addText.shakeView(this)
    }

    override fun receiveData(skill: String){
        presenter.addNewSkill(skill.toLowerCase())
    }

    override fun addSkillsToBundle(skills: ArrayList<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun startInterestFrag() {
        // TODO ritornare la lista di skills aggiornata all'activity EditProfile
    }

    override fun finishActivityAndPassResult() {
        val intent = Intent()
        intent.putExtra("skills", presenter.skills)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
