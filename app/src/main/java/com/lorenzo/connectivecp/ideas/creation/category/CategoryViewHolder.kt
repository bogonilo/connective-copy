package com.lorenzo.connectivecp.ideas.creation.category

import android.view.View
import com.lorenzo.connectivecp.R
import kotlinx.android.synthetic.main.category_element.view.*

class CategoryViewHolder (itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    fun bindView(entry: String, adapter: CategoryAdapter, position: Int) {
        var isSelected = adapter.categories.contains(entry)
        if(isSelected)
            itemView.category_element_layout.setBackgroundResource(R.drawable.activity_category_selected)
        else
            itemView.category_element_layout.setBackgroundResource(R.drawable.activity_category)
        itemView.text_category_element.setText(entry)
//        if (selectedPosition == position) selected else !selected
        itemView.category_element_layout.setOnClickListener{
            if(!isSelected){
                itemView.category_element_layout.setBackgroundResource(R.drawable.activity_category_selected)
                adapter.categories.add(entry)
                isSelected = true
                adapter.sendFieldsCheckToActivity()
//                selectedPosition = position
//                adapter.notifyItemChanged(position)
//                selected = true
            }
            else{
                itemView.category_element_layout.setBackgroundResource(R.drawable.activity_category)
                adapter.categories.remove(entry)
                isSelected = false
                adapter.sendFieldsCheckToActivity()
//                selectedPosition = position
//                adapter.notifyItemChanged(position)
//                selected = false
            }
//            adapter.notifyDataSetChanged()
        }
    }
}