package com.lorenzo.connectivecp.profile.view

import android.content.Context
import android.content.Intent

interface ViewProfileContract {

    interface View{
        fun loadInfo()
        fun editProfileBtnInvisible()
        fun startLandingActivity()
        fun failedLogOut()
        fun notifyAdapter()
    }

    interface Presenter{
        fun getUserFromFirestore(uid: String)
        fun activtiyResult(requestCode: Int, resultCode: Int, data: Intent?)
        fun getIntentData(intent: Intent)
        fun signOutUser(context: Context)
        fun getUserData()
    }

}