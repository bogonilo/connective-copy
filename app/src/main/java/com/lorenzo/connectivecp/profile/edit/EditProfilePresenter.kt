package com.lorenzo.connectivecp.profile.edit

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.text.InputType
import android.widget.EditText
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.firebase.StorageCallback
import com.lorenzo.connectivecp.firebase.StorageManager
import com.lorenzo.connectivecp.utils.Utils
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.io.File
import java.lang.IndexOutOfBoundsException
import java.util.regex.Pattern

class EditProfilePresenter(view: EditProfileContract.View) :
    EditProfileContract.Presenter {

    private var mView = view
    var user = User("", "", "")
    private var storage = FirebaseStorage.getInstance()
    private var firestoreManager = FirestoreManager()
    private var storageManager = StorageManager()
    private var imageUrl = ""
    var roles = emptyArray<String>()
    var mainChapters = emptyArray<String>()
    var parentChapters = emptyArray<String>()
    lateinit var subChapters: Array<Array<String>?>
    private var imageChange = false

    override fun setImgUrl() {
        imageUrl = user.profileImgUrl
    }

    override fun getImageReference(): StorageReference {
        return storage.getReferenceFromUrl(user.profileImgUrl)
    }

    override fun updateUserInfo(name: String, position: String, office: String, chapter: String, imagePath: String) {
        mView.showProgBar()
        if(validateName(name)){
            if(imageChange){
                if (File(imagePath).exists()) {
                    storageManager.addImgToFirebase(user.uid, Uri.fromFile(File(imagePath)).toString(), object: StorageCallback{
                        override fun onCallbackImgUrl(value: String) {
                            user = User(user.uid, user.email, name, "", position, office, value,
                                chapter, "", user.skills, user.interests, user.joinedIdeas)
                            firestoreManager.updateUserInfo(user)
                            mView.finishActivityWithResult()
                        }
                        override fun onUploadError() {
                            user = User(user.uid, user.email, name, "", position, office,
                                "https://firebasestorage.googleapis.com/v0/b/standapp-bcbb2.appspot.com/o/profile_images%2Faccenture_logo.png?alt=media&token=b581c20a-4602-4e23-a742-1236c2e3c0d7",
                                chapter, "", user.skills, user.interests, user.joinedIdeas)
                            firestoreManager.updateUserInfo(user)
                            mView.finishActivityWithResult()
                        }
                    })
                }
                else{
                    user = User(user.uid, user.email, name, "", position, office,
                        "https://firebasestorage.googleapis.com/v0/b/standapp-bcbb2.appspot.com/o/profile_images%2Faccenture_logo.png?alt=media&token=b581c20a-4602-4e23-a742-1236c2e3c0d7",
                        chapter, "", user.skills, user.interests, user.joinedIdeas)
                    firestoreManager.updateUserInfo(user)
                    mView.finishActivityWithResult()
                }
            }
            else{
                user = User(user.uid, user.email, name, "", position, office, imageUrl, chapter, "",
                    user.skills, user.interests, user.joinedIdeas)
                firestoreManager.updateUserInfo(user)
                mView.finishActivityWithResult()
            }
        }
        else{
            mView.invalidName()
            mView.hideProgBar()
        }
    }

    override fun deleteUserClicked(): Dialog {
        val builder = AlertDialog.Builder(mView.getContext())
            .setTitle("Delete user")
            .setMessage("Are you sure you want to completely delete your profile?")
            .setPositiveButton(android.R.string.yes, DialogInterface.OnClickListener { dialog, which ->
                delCurrentUser()
            })
            .setNegativeButton(android.R.string.no, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
        return builder.create()
    }

    override fun delCurrentUser() {
        val user_del = FirebaseAuth.getInstance().currentUser
        val builder = AlertDialog.Builder(mView.getContext())
        var password = ""
        builder.setTitle("Enter password")
        val input = EditText(mView.getContext())
        input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        builder.setView(input)
        builder.setPositiveButton("OK") { dialog, which ->
            password = input.text.toString()
            reauthUserAndDelete(user_del!!, password)
        }
        builder.setNegativeButton("Cancel") { dialog, which ->
            dialog.cancel()
        }
        mView.showDialog(builder.create())
    }

    override fun reauthUserAndDelete(user: FirebaseUser, password: String) {
        val credential = EmailAuthProvider
            .getCredential(this.user.email, password)
        user.reauthenticate(credential)
            .addOnCompleteListener {
                storageManager.deletePicFromDatabase(this.user.profileImgUrl)
                user.delete()
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            firestoreManager.deleteUserFromFirestore(this.user)
                            firestoreManager.deleteActivityListCreatedByUser(this.user.uid)
                            mView.restartFromSplash()
                        }
                    }
            }
            .addOnFailureListener {
                mView.showErrorDelUser(it)
            }
    }

    override fun activityResult(requestCode: Int, resultCode: Int, data: Intent?, context: Context) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Utils.GALLERY_REQUEST_CODE -> {
                    data?.let {
//                        mView.setUri(it.data)
                        mView.startCrop(it.data!!)
                    }
                }
                Utils.CAMERA_REQUEST_CODE -> {
                    mView.setImageView()
                }
                Utils.CROP_IMAGE ->{
                    data?.let{
//                        val file = Utils.saveTempBitmap(it.getParcelableExtra("croppedImg"), context)
                        mView.setUriPathEverything(it.getParcelableExtra("croppedImgUri") as Uri, it.getStringExtra("croppedImgAbsPath"))
                    }
                    imageChange = true
                }
                Utils.ADD_SKILL ->{
                    user.skills.clear()
                    user.skills.addAll(data!!.getStringArrayListExtra("skills"))
                    mView.notifyAdapterSkills()
                }
                Utils.ADD_INTEREST ->{
                    user.interests.clear()
                    user.interests.addAll(data!!.getStringArrayListExtra("interests"))
                    mView.notifyAdapterInterests()
                }
            }
        } else{
            mView.reSetOriginalPic()
//            if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
//                mView.setImgUri(Uri.parse("android.resource://com.accenture.standapp/drawable/accenture_logo"))
        }
    }

    override fun dialogClicked(tag: String, value: String, index: Int) {
        when (tag) {
            "location" -> {
                mView.setLocationText(value)
            }
            "role" -> {
                mView.setRoleText(value)
            }
            "chapter" -> {
                chapterSelection(value)
            }
            "subChapters" -> {
                mView.setChapterText(value)
            }
            "profilePicture"-> {
               picOptClicked(index)
            }
        }
    }

    override fun picOptClicked(opt: Int) {
        when (opt) {
            0 -> mView.cameraPic()?.let { mView.setImageFromCamera(it) }
            1 -> mView.galleryPic()
            2 -> {
                mView.removePic()
                imageChange = true
            }
        }
    }

    fun chapterSelection(value: String) {
        if (parentChapters.contains(value))
            mView.createDialog(subChapters[parentChapters.indexOf(value)]!!, value, "subChapters")
        else
            mView.setChapterText(value)
    }

    override fun loadRolesArray() {
        firestoreManager.loadCareersLevel(object : FireCallback.CareersLevel {
            override fun onCallbackCareersLevelList(list: Array<String>) {
                roles = list
            }
        })
    }

    override fun loadChapters() {
        firestoreManager.loadMainChapters(object : FireCallback.MainChapters {
            override fun onCallbackMainChaptersList(listAll: Array<String>, listParents: Array<String>) {
                mainChapters = listAll
                parentChapters = listParents
                subChapters = arrayOfNulls(parentChapters.size)
                parentChapters.forEachIndexed { index, strings ->
                    loadSubChapters(index)
                }
            }
        })
    }

    fun loadSubChapters(index: Int) {
        firestoreManager.loadSubChapters(parentChapters[index], object : FireCallback.SubChapters {
            override fun onCallbackSubChaptersList(list: Array<String>) {
                subChapters[index] = list
            }
        })
    }

    override fun getPicListItems(array: ArrayList<String>): Array<String> {
//        if (imageChange) {
            array.add("Delete")
            return array.toArray(arrayOfNulls(array.size))
//        } else
//            return array.toArray(arrayOfNulls(array.size))
    }

    override fun removePicFromDB() {
        if(user.profileImgUrl != "https://firebasestorage.googleapis.com/v0/b/standapp-bcbb2.appspot.com/o/profile_images%2Faccenture_logo.png?alt=media&token=b581c20a-4602-4e23-a742-1236c2e3c0d7")
            storageManager.deletePicFromDatabase(user.profileImgUrl)
    }

    private fun validateName(name: String): Boolean{
        val NAME_REGEX = "^[a-zA-Z\\s]+"
        val pattern = Pattern.compile(NAME_REGEX)
        val matcher = pattern.matcher(name)
        try {
            return (matcher.matches() && (name.split(" ")[1].isNotEmpty()))
        }
        catch (e: IndexOutOfBoundsException){
            return false
        }
    }
}