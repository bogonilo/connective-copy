package com.lorenzo.connectivecp.main

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.ideas.browsing.IdeasFragment
import com.lorenzo.connectivecp.me.MeFragment
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.skills.SkillsSearchFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity(), MainContract.View {

    private lateinit var navigationView: BottomNavigationView
    private var presenter = MainPresenter(this)
    private lateinit var active: Fragment
    private var ideasFragment = IdeasFragment()
    private var skillsFragment = SkillsSearchFragment()
    //    private var budgetFragment = BudgetFragment()
    private var profileFragment = MeFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_new)
        navigationView = findViewById(R.id.navigationView_main_new)
        presenter.user = intent.getSerializableExtra("user") as User
        val bundle = Bundle()
        bundle.putSerializable("user", presenter.user)
        ideasFragment.arguments = bundle
        supportFragmentManager.beginTransaction().add(R.id.container_main_new, profileFragment, "profile")
            .hide(profileFragment).commit()
//        supportFragmentManager.beginTransaction().add(R.id.container_main_new, budgetFragment, "budget").hide(budgetFragment).commit()
        supportFragmentManager.beginTransaction().add(R.id.container_main_new, skillsFragment, "skills")
            .hide(skillsFragment).commit()
        supportFragmentManager.beginTransaction().add(R.id.container_main_new, ideasFragment, "events").commit()
        navigationView.setOnNavigationItemSelectedListener { presenter.navigationItemSelected(it.itemId) }
        active = ideasFragment
    }

    override fun onBackPressed() {
        if (active == skillsFragment && skillsFragment.searching)
            skillsFragment.emptyFragment()
        else
            super.onBackPressed()
    }

    override fun showBudgetFrag() {
//        supportFragmentManager.beginTransaction().hide(active).show(budgetFragment).commit();
//        active = budgetFragment
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        if(requestCode == Utils.PROFILE_VIEW_CODE){
            if(resultCode == Activity.RESULT_OK){
                ideasFragment.updateUserInPresenter()
                profileFragment.updateUser()
            }
//        }
    }

    override fun showEventFrag() {
        supportFragmentManager.beginTransaction().hide(active).show(ideasFragment).commit();
        active = ideasFragment
    }

    override fun showProfileFrag() {
        supportFragmentManager.beginTransaction().hide(active).show(profileFragment).commit()
        active = profileFragment
    }

    override fun showSkillsFrag() {
        supportFragmentManager.beginTransaction().hide(active).show(skillsFragment).commit();
        active = skillsFragment
    }

    fun updateCalendarInMeFrag() {
        profileFragment.updateCalendar()
    }
}