package com.lorenzo.connectivecp.profile.signup

import android.content.Intent

interface SignUpFragmentsContract {

    interface View{
        fun startWithSecondFrag()
        fun startFromSkills()
    }

    interface Presenter{
        fun getEmailUser(): String
        fun checkReceivingIntent(intent: Intent)
    }
}