package com.lorenzo.connectivecp.profile.signup.skillsFragment

import android.content.Intent
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager

class SkillsPresenter(view: SkillsContract.View) :
    SkillsContract.Presenter {

    private var mView = view
    var skills = ArrayList<String>()
    var allSkillsList = ArrayList<String>()
    var suggestedSkills = ArrayList<String>()
    private var firestoreManager = FirestoreManager()

    override fun checkSkillsNumber() {
        if (skills.size > 2)
            mView.activateNextBtn()
        else
            mView.deactivateNextBtn()
    }

    override fun nextBtnClicked() {
        if (skills.size > 2) {
            mView.addSkillsToBundle(skills)
            mView.startInterestFrag()
        } else {
            mView.shakeText()
            mView.deactivateNextBtn()
        }
    }

    override fun loadSkillsList() {
        firestoreManager.getSkillsList(object : FireCallback.SkillsList {
            override fun onCallbackSkillsList(skillsList: ArrayList<String>) {
                allSkillsList.addAll(skillsList)
            }
        })
    }

    override fun dialogTextChange(s: CharSequence?, count: Int) {
        suggestedSkills.clear()
        s?.let {
            if (count > 1) {
                filterSuggestion(it)
            }
            if (count == 1) {
                suggestedSkills.clear()
            }
            mView.notifyAdapterSuggestions()
        }
    }

    fun filterSuggestion(s: CharSequence) {
        for (item in allSkillsList)
            if (item.contains(s))
                if (!suggestedSkills.contains(item))
                    suggestedSkills.add(item)
    }

    override fun dialogBackPressed(keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            mView.cancelDialog()
        }
        return true
    }

    override fun dialogTextEnter(actionId: Int, text: String): Boolean {
        if (text.isNotEmpty() && actionId == EditorInfo.IME_ACTION_DONE) {
            mView.receiveData(text)
            return true
        } else
            return false
    }

    override fun receiveIntentWithSkills(intent: Intent) {
        skills.addAll(intent.getStringArrayListExtra("skills"))
        mView.notifyAdapterSkills()
        if(skills.size > 2){
            mView.activateNextBtn()
        }
    }

    override fun doneBtnClicked() {
        if(skills.size > 2)
            mView.finishActivityAndPassResult()
        else {
            mView.shakeText()
            mView.deactivateNextBtn()
        }
    }

    override fun addNewSkill(skill: String) {
        if(!skills.contains(skill) && !skill.matches(Regex("[0-9]+"))){
            skills.add(skill)
            mView.notifyAdapterSkills()
            mView.cancelDialog()
        }
        else
            mView.cancelDialog()
        checkSkillsNumber()
    }
}