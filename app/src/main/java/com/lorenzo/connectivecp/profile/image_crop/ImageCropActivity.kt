package com.lorenzo.connectivecp.profile.image_crop

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.fragment.app.Fragment
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.utils.Utils
import com.adamstyrc.cookiecutter.CookieCutterImageView
import com.adamstyrc.cookiecutter.ImageUtils


class ImageCropActivity : AppCompatActivity() {

    private lateinit var cutterImageView: CookieCutterImageView
    private lateinit var cancelButton: Button
    private lateinit var doneButton: Button
    private lateinit var imgBitmap: Bitmap

    companion object{
        private lateinit var imgUri: Uri
        fun startActivity(activity: Activity, img: Uri)  {
            imgUri = img
            val intent = Intent(activity, ImageCropActivity::class.java)
            activity.startActivityForResult(intent, Utils.CROP_IMAGE)
        }
        fun startActivity(fragment: Fragment, img: Uri)  {
            imgUri = img
            val intent = Intent(fragment.context, ImageCropActivity::class.java)
            fragment.startActivityForResult(intent, Utils.CROP_IMAGE)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_crop)
        cutterImageView = findViewById(R.id.cookie_cutter_image_crop)
        cancelButton = findViewById(R.id.cancel_crop_image)
        doneButton = findViewById(R.id.done_crop_image)
        val screenSize = ImageUtils.getScreenSize(this)
        val scaledBitmap = ImageUtils.decodeUriToScaledBitmap(this, imgUri, screenSize.x, screenSize.y)
        cutterImageView.setImageBitmap(scaledBitmap)
//        cutterImageView.setImageURI(imgUri)
        doneButton.setOnClickListener { returnBitmapToActivity() }
        cancelButton.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish() }
//        Glide.with(this).load(imgUri).fitCenter().into(cutterImageView)
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
        super.onBackPressed()
    }

    fun returnBitmapToActivity(){
        imgBitmap = cutterImageView.croppedBitmap
        val file = Utils.saveTempBitmap(imgBitmap, this)
        val intent = Intent()
        intent.putExtra("croppedImgUri", Uri.fromFile(file))
        intent.putExtra("croppedImgAbsPath", file?.absolutePath)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
