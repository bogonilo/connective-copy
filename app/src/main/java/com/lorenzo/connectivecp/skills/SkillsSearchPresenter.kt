package com.lorenzo.connectivecp.skills

import android.content.Context
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.utils.SharedPreferenceManager
import kotlin.collections.ArrayList

class SkillsSearchPresenter(view: SkillsSearchContract.View, context: Context?) :
    SkillsSearchContract.Presenter {

    var skills = ArrayList<String>()
    var suggestedSkills = ArrayList<String>()
    var results = ArrayList<User>()
    private var firestoreManager = FirestoreManager()
    private var mView = view
    private var preferenceManager = SharedPreferenceManager(context)
    private var currUID = preferenceManager.getIDFromPref()

    override fun loadSkills() {
        firestoreManager.getSkillsList(object : FireCallback.SkillsList {
            override fun onCallbackSkillsList(skillsList: ArrayList<String>) {
                skills = skillsList
            }
        })
    }

    override fun dialogBackPressed(keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            mView.cancelDialog()
        }
        return true
    }

    override fun dialogTextEnter(actionId: Int, text: String): Boolean {
        if (text.isNotEmpty() && actionId == EditorInfo.IME_ACTION_DONE) {
            mView.onSuggestionSelected(text)
            return true
        } else
            return false
    }

    override fun dialogTextChange(s: CharSequence?, count: Int) {
        suggestedSkills.clear()
        s?.let {
            if (count > 1) {
                filterSuggestion(it)
            }
            if (count == 1) {
                suggestedSkills.clear()
            }
            mView.notifyAdapterSuggestions()
        }
    }

    fun filterSuggestion(s: CharSequence) {
        for (item in skills)
            if (item.contains(s))
                if (!suggestedSkills.contains(item))
                    suggestedSkills.add(item)
    }

    override fun textSelected(skill: String) {
        firestoreManager.getUserListBySkill(skill, object : FireCallback.UserListBySkill {
            override fun onCallbackUserList(usersUid: ArrayList<User>) {
                    mView.hideNoExperts()
                    mView.hideProgressBar()
                    for (element in usersUid) {
                        if(element.uid != currUID)
                            results.add(element)
                        mView.notifyNewDataResults()
                    }
            }
            override fun onCallbackNoResults() {
                mView.hideProgressBar()
                mView.showNoExperts()
            }
        })
    }

    override fun backPressedInDialog(keyCode: Int): Boolean {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            mView.cancelDialog()
            return true
        }
        else
            return false
    }
}