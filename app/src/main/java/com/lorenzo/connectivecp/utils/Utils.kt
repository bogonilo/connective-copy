package com.lorenzo.connectivecp.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


object Utils {

    const val PROFILE_EDIT_CODE = 111
    const val PROFILE_VIEW_CODE = 112
    const val CREATE_ACTIVITY_CODE = 113
    const val GALLERY_REQUEST_CODE = 123
    const val CAMERA_REQUEST_CODE = 124
    const val SET_START_DATE_CODE = 125
    const val SET_RSVP_CODE = 126
    const val VIEW_EVENT_INFO = 127
    const val ADD_SKILL = 128
    const val ADD_INTEREST = 129
    const val CROP_IMAGE = 130

    fun getPath(context: Context?, uri: Uri): String {
        var result: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)

        val cursor = context?.contentResolver?.query(uri, proj, null, null, null)
        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    val columnIndex = cursor.getColumnIndexOrThrow(proj[0])
                    result = cursor.getString(columnIndex)
                }
            }
        } catch (e: Exception) {
            cursor?.close()
        } finally {
            cursor?.close()
        }
        if (result == null) {
            result = "Not found"
        }
        return result
    }

    fun pickFromGallery(activity: Activity) {
        if (isStoragePermissionGranted(activity, activity.applicationContext)) {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            val mimeTypes = arrayOf("image/jpeg", "image/png")
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            activity.startActivityForResult(intent, GALLERY_REQUEST_CODE)
        }
    }

    fun pickFromGallery(fragment: Fragment, context: Context) {
        if (isStoragePermissionGranted(fragment, context)) {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            val mimeTypes = arrayOf("image/jpeg", "image/png")
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            fragment.startActivityForResult(intent, GALLERY_REQUEST_CODE)
        } else
            Log.e("Utils", "Permission problem")
    }

    fun pickFromCameraForProPic(fragment: Fragment, context: Context): File? {
        if (isStoragePermissionGranted(fragment, context)) {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(fragment.activity?.packageManager!!)?.also {
                    val photoFile: File? = try {
                        createImageFile(context)
                    } catch (ex: IOException) {
                        null
                    }
                    return photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            context,
                            "com.accenture.standapp.fileprovider",
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        fragment.startActivityForResult(
                            takePictureIntent,
                            CAMERA_REQUEST_CODE
                        )
                    }
                }
            }
        }
        return null
    }

    fun pickFromCameraForProPic(activity: Activity, context: Context): File? {
        if (isStoragePermissionGranted(activity, context)) {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(activity.packageManager)?.also {
                    val photoFile: File? = try {
                        createImageFile(context)
                    } catch (ex: IOException) {
                        null
                    }
                    return photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            context,
                            "com.accenture.standapp.fileprovider",
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        activity.startActivityForResult(
                            takePictureIntent,
                            CAMERA_REQUEST_CODE
                        )
                    }
                }
            }
        }
        return null
    }

    fun pickFromCameraEventImg(activity: Activity): File? {
        if (isStoragePermissionGranted(activity, activity.applicationContext)) {
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                takePictureIntent.resolveActivity(activity.packageManager)?.also {
                    val photoFile: File? = try {
                        createImageFileForIdea(activity.applicationContext)
                    } catch (ex: IOException) {
                        null
                    }
                    return photoFile?.also {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            activity.applicationContext,
                            "com.accenture.standapp.fileprovider",
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        activity.startActivityForResult(
                            takePictureIntent,
                            CAMERA_REQUEST_CODE
                        )
                    }
                }
            }
        }
        return null
    }

    private fun isStoragePermissionGranted(activity: Activity, context: Context): Boolean {
        val permissions = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return if (context.let {
                    ActivityCompat.checkSelfPermission(
                        it,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) + ActivityCompat.checkSelfPermission(
                        it,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) + ActivityCompat.checkSelfPermission(it, Manifest.permission.CAMERA)
                } == PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                requestPermissions(activity, permissions, 1)
                false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true
        }
    }

    fun isStoragePermissionGranted(fragment: Fragment, context: Context): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.let {
                    ActivityCompat.checkSelfPermission(
                        it,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) + ActivityCompat.checkSelfPermission(
                        it,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) + ActivityCompat.checkSelfPermission(it, Manifest.permission.CAMERA)
                } == PackageManager.PERMISSION_GRANTED) {
                return true
            } else {
                fragment.requestPermissions(
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA
                    ), 2
                )
                return false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true
        }
    }

    fun dpToPx(context: Context?, dp: Float): Int? =
        context?.resources?.displayMetrics?.density?.let { dp.times(it) }?.let { Math.round(it) }

    @Throws(IOException::class)
    private fun createImageFile(context: Context): File {
        val storageDir: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val file = File(storageDir, "profile_image.jpg")
        if (file.exists()) file.delete()
        return file
    }

    @Throws(IOException::class)
    private fun createImageFileForIdea(context: Context): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val storageDir: File? = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val file = File(storageDir, "idea_image$timeStamp.jpg")
        if (file.exists()) file.delete()
        return file
    }

    fun getContactBitmapFromURI(context: Context, uri: Uri): Bitmap? {
        try {
            val input = context.contentResolver.openInputStream(uri) ?: return null
            return BitmapFactory.decodeStream(input)
        } catch (e: FileNotFoundException) {

        }
        return null
    }

    fun saveTempBitmap(bitmap: Bitmap, context: Context): File? {
        return if (isExternalStorageWritable()) {
            saveImage(bitmap, context)
        } else
            null
    }

    private fun saveImage(finalBitmap: Bitmap, context: Context): File {
        val root = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString()
        val myDir = File(root)
        myDir.mkdirs()
        val fname = "profile_image.jpg"

        val file = File(myDir, fname)
        if (file.exists()) file.delete()
        try {
            val out = FileOutputStream(file)
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return file
    }

    /* Checks if external storage is available for read and write */
    private fun isExternalStorageWritable(): Boolean {
        val state = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state
    }
}