package com.lorenzo.connectivecp.firebase

import android.net.Uri
import android.util.Log
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import java.io.File

class StorageManager {

    private var storage = FirebaseStorage.getInstance()
    private var storageRef = storage.reference
    private var firestoreManager = FirestoreManager()

    fun addImgToFirebase(uid: String, uri: String, callback: StorageCallback) {
        var file = Uri.parse(uri)
        val riversRef = storageRef.child("profile_images/" + uid + file.lastPathSegment)
        var uploadTask = riversRef.putFile(file)

        val urlTask = uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation riversRef.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                firestoreManager.updatePicUrl(uid, downloadUri)
                callback.onCallbackImgUrl(downloadUri.toString())
            } else {
                callback.onUploadError()
            }
        }
        uploadTask  //TODO : fix the failed upload
            .addOnFailureListener {
                // Handle unsuccessful uploads
                Log.d("StoreManager", "Error uploading of pic")
            }

            .addOnSuccessListener {
                Log.d("StoreManager", "upload complete")
            }
    }

    fun addIdeaImgToFirebase(uid: String, uri: Uri, callback: StorageCallback) {
        val riversRef = storageRef.child("idea_images/" + uid + uri.lastPathSegment)
        var uploadTask = riversRef.putFile(uri)
        val urlTask = uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation riversRef.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                callback.onCallbackImgUrl(downloadUri.toString())
            } else {
                // Handle failures
                // TODO
            }
        }
        uploadTask  //TODO : fix the failed upload
            .addOnFailureListener {
                // Handle unsuccessful uploads
                Log.d("StoreManager", "Error uploading of pic")
            }
            .addOnSuccessListener {
                Log.d("StoreManager", "upload complete")
            }
    }

    fun addImgToFirebase(uid: String, uri: String) {
        var file = Uri.parse(uri)
        val riversRef = storageRef.child("profile_images/" + uid + file.lastPathSegment)
        var uploadTask = riversRef.putFile(file)

        val urlTask = uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation riversRef.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                firestoreManager.updatePicUrl(uid, downloadUri)
            } else {
                // Handle failures
                // TODO
            }
        }
        uploadTask  //TODO : fix the failed upload
            .addOnFailureListener {
                // Handle unsuccessful uploads
                Log.d("StoreManager", "Error uploading of pic")
            }

            .addOnSuccessListener {
                Log.d("StoreManager", "upload complete")
            }
    }

    fun updateImgToFirebase(uid: String, file: File, httpsReference: StorageReference, callback: StorageCallback) {
        var file = Uri.fromFile(file)
        val riversRef = storage.reference.child("profile_images/" + uid + file.lastPathSegment)
        var uploadTask = riversRef.putFile(file)

        val urlTask = uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation riversRef.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result
                httpsReference.delete()
                    .addOnCompleteListener {
                        Log.d("StorageManager", "old pic deleted")
                    }
                    .addOnFailureListener {
                        Log.d("StorageManager", "old pic not deleted")
                    }
                callback.onCallbackImgUrl(downloadUri.toString())
            } else {
                // TODO
            }
        }
            .addOnFailureListener {
                //TODO
            }

    }

    fun deletePicFromDatabase(url: String){
        var storage = FirebaseStorage.getInstance()
        var httpsReference = storage.getReferenceFromUrl(url)
        httpsReference.delete().addOnSuccessListener {
            Log.d("StorageManager", "Picture deleted from Firebase")
        }
            .addOnFailureListener {
                Log.e("StorageManager", "Picture not deleted from Firebase")
            }
    }

}