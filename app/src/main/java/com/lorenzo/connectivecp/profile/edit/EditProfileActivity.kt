package com.lorenzo.connectivecp.profile.edit

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.constraintlayout.widget.ConstraintLayout
import android.widget.*
import com.bumptech.glide.Glide
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.init.splash.SplashActivity
import com.lorenzo.connectivecp.utils.Utils
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.profile.image_crop.ImageCropActivity
import com.lorenzo.connectivecp.profile.signup.interestsFragment.InterestsActivity
import com.lorenzo.connectivecp.profile.signup.skillsFragment.SkillsActivity
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import java.io.File
import java.lang.Exception

class EditProfileActivity : AppCompatActivity(), EditProfileContract.View {

    private lateinit var name: TextView
    private lateinit var office: TextView
    private lateinit var skillRecyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var interestRecyclerView: androidx.recyclerview.widget.RecyclerView
    private lateinit var interestsTitle: TextView
    private lateinit var position: EditText
    private lateinit var chapter: EditText
    private lateinit var proPic: ImageView
    private lateinit var saveEditsBtn: TextView
    private lateinit var changePic: ImageButton
    private lateinit var addSkill: ImageButton
    private lateinit var addInterest: ImageButton
    private lateinit var progressBar: ProgressBar
    private lateinit var layout: ConstraintLayout
    private var skills_AL = ArrayList<String>()
    private var interests_AL = ArrayList<String>()
    private lateinit var viewAdapterSkills: androidx.recyclerview.widget.RecyclerView.Adapter<*>
    private lateinit var viewAdapterInterests: androidx.recyclerview.widget.RecyclerView.Adapter<*>
    private lateinit var viewManagerSkills: FlexboxLayoutManager
    private lateinit var viewManagerInterests: FlexboxLayoutManager
    private lateinit var presenter: EditProfilePresenter
    private var imageUri: Uri = Uri.parse("android.resource://com.accenture.standapp/drawable/profile_photo")
    private var imagePath = "android.resource://com.accenture.standapp/drawable/accenture_logo"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        presenter = EditProfilePresenter(this)
        layout = findViewById(R.id.layout_editProfile)
        name = findViewById(R.id.name_editProfile)
        office = findViewById(R.id.officeLocation_editProfile)
        position = findViewById(R.id.position_editProfile)
        proPic = findViewById(R.id.proPic_editProfile)
        changePic = findViewById(R.id.changePic_editProfile)
        saveEditsBtn = findViewById(R.id.save_editProfile)
        chapter = findViewById(R.id.chapter_editProfile)
        progressBar = findViewById(R.id.progressBar_editProfile)
        skillRecyclerView = findViewById(R.id.recyclerView_skills_page2)
        interestsTitle = findViewById(R.id.interestsTitle_editProfile)
        interestRecyclerView = findViewById(R.id.recyclerView_interests)
        addSkill = findViewById(R.id.addSkill_editProfile)
        addInterest = findViewById(R.id.addInterest_editProfile)
        presenter.user = intent.getSerializableExtra("user") as User
        presenter.setImgUrl()
        skills_AL = presenter.user.skills
        interests_AL = presenter.user.interests
        viewManagerSkills = FlexboxLayoutManager(this)
        viewManagerSkills.flexDirection = FlexDirection.ROW
        viewManagerInterests = FlexboxLayoutManager(this)
        viewManagerInterests.flexDirection = FlexDirection.ROW
        viewAdapterSkills = SkillsAdapter(presenter.user.skills, false)
        viewAdapterInterests = SkillsAdapter(presenter.user.interests, false)
        skillRecyclerView.layoutManager = viewManagerSkills
        interestRecyclerView.layoutManager = viewManagerInterests
        skillRecyclerView.adapter = viewAdapterSkills
        interestRecyclerView.adapter = viewAdapterInterests
        name.text = presenter.user.name
        office.text = presenter.user.office
        position.setText(presenter.user.position)
        Glide.with(this).load(presenter.user.profileImgUrl).diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true).centerCrop().into(proPic)
        chapter.setText(presenter.user.chapter)
        progressBar.visibility = View.INVISIBLE
        presenter.loadRolesArray()
        presenter.loadChapters()
        office.setOnClickListener{ createDialog(resources.getStringArray(R.array.offices),
            getString(R.string.dialog_location_title),"location")}
        position.setOnClickListener { createDialog(presenter.roles, getString(R.string.dialog_role_title), "role")}
        chapter.setOnClickListener {
            createDialog(presenter.mainChapters, getString(R.string.dialog_chapter_title), "chapter")}
        addSkill.setOnClickListener { startSkillsActivity()}
        addInterest.setOnClickListener{ startInterestsActivity() }
        saveEditsBtn.setOnClickListener{ presenter.updateUserInfo(name.text.toString(), position.text.toString(),
            office.text.toString(), chapter.text.toString(), imagePath)}
        changePic.setOnClickListener{
            createDialog(presenter.getPicListItems(resources.getStringArray(R.array.pic_options).toCollection(java.util.ArrayList())),
                "", "profilePicture")}
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.activityResult(requestCode, resultCode, data, this)
    }

    override fun showDialog(dialog: Dialog) {
        dialog.show()
    }

    override fun notifyAdapterSkills() {
        viewAdapterSkills.notifyDataSetChanged()
    }

    override fun notifyAdapterInterests() {
        viewAdapterInterests.notifyDataSetChanged()
    }

    override fun showProgBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgBar() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun finishActivityWithResult() {
        val resultIntent = Intent()
        resultIntent.putExtra("user", presenter.user)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    override fun restartFromSplash() {
        val intent = Intent(this, SplashActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    override fun showErrorDelUser(exception: Exception) {
        Toast.makeText(this, R.string.error_delete_user, Toast.LENGTH_LONG).show()
    }

    override fun setImage(uri: Uri) {
        proPic.setImageURI(uri)
    }

    override fun getContext(): Context {
        return this
    }

    override fun setOfficeText(office: String) {
        this.office.text = office
    }

    override fun createDialog(listItems: Array<String>, title: String, tag: String) {
        val items = listItems
        val builder = AlertDialog.Builder(this, R.style.full_width_dialog)
        builder.setTitle(title)
        builder.setItems(items) { dialog, which ->
            presenter.dialogClicked(tag, items[which], which)
        }
        val dialog = builder.create()
        with(dialog) {
            listView.divider = ColorDrawable(Color.parseColor("#E8E8E8"))
            listView.dividerHeight = 2
            show()
        }
        val window = dialog.window
        window?.let {
            with(it) {
                setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
                setBackgroundDrawableResource(R.drawable.shape_location_dialog)
                val wlp = attributes
                wlp.gravity = Gravity.BOTTOM
                wlp.dimAmount = 0.3f
                attributes = wlp
            }
        }
    }

    override fun setLocationText(value: String) {
        office.text = value
    }

    override fun setRoleText(value: String) {
        position.setText(value)
    }

    override fun setChapterText(value: String) {
        chapter.setText(value)
    }

//    override fun setUri(uri: Uri) {
//        imageUri = uri
//        proPic.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//            ViewGroup.LayoutParams.MATCH_PARENT)
//        Glide.with(proPic.context).load(uri).centerCrop().into(proPic)
//        imagePath = Utils.getPath(this, imageUri)
//    }
//
//    override fun setImgUri(uri: Uri) {
//        imageUri = uri
//        imagePath = "android.resource://com.accenture.standapp/drawable/accenture_logo"
//        proPic.setImageURI(Uri.parse("android.resource://com.accenture.standapp/drawable/profile_photo"))
//        Glide.with(proPic.context).load(uri).centerCrop().into(proPic)
//    }

    override fun setUriPathEverything(uri: Uri, path: String) {
        imageUri = uri
        imagePath = path
        proPic.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT)
        Glide.with(proPic.context).load(uri.toString()).diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true).centerCrop().into(proPic)
//        proPic.setImageURI(uri)
    }

    override fun setImageFromCamera(file: File) {
        imageUri = Uri.fromFile(file)
        imagePath = file.absolutePath
    }

    override fun setImageView() {
        startCrop(imageUri)
//        proPic.layoutParams = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
//            ViewGroup.LayoutParams.MATCH_PARENT)
//        Glide.with(proPic.context).load(imageUri).centerCrop().into(proPic)
    }

    override fun clickAddPic() {
        proPic.performClick()
    }

    override fun galleryPic() {
        Utils.pickFromGallery(this)
    }

    override fun cameraPic(): File? {
        return Utils.pickFromCameraForProPic(this, this.baseContext)
    }

    override fun startCrop(uri: Uri) {
//        val intent = CropImage.activity(uri)
//            .setCropShape(CropImageView.CropShape.OVAL)
//            .setFixAspectRatio(true)
//            .setActivityTitle("")
//            .setAllowRotation(false)
//            .setAllowFlipping(false)
//            .getIntent(this)
//        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        ImageCropActivity.startActivity(this, uri)
    }

    override fun removePic() {
        imageUri = Uri.parse("android.resource://com.accenture.standapp/drawable/accenture_logo")
        imagePath = "android.resource://com.accenture.standapp/drawable/accenture_logo"
        proPic.setImageURI(imageUri)
    }

    override fun startSkillsActivity() {
        val intent = Intent(this, SkillsActivity::class.java)
        intent.putExtra("skills", presenter.user.skills)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivityForResult(intent, Utils.ADD_SKILL)
    }

    override fun startInterestsActivity() {
        val intent = Intent(this, InterestsActivity::class.java)
        intent.putExtra("interests", presenter.user.interests)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivityForResult(intent, Utils.ADD_INTEREST)
    }

    override fun invalidName() {
        Toast.makeText(this, "Insert full name", Toast.LENGTH_LONG).show()
    }

    override fun reSetOriginalPic(){
        Glide.with(this).load(presenter.user.profileImgUrl).centerCrop().into(proPic)
    }
}