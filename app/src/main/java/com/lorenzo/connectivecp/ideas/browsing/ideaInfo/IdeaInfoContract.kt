package com.lorenzo.connectivecp.ideas.browsing.ideaInfo

import com.lorenzo.connectivecp.model.Joiner

interface IdeaInfoContract {

    interface View{
        fun setOrgImg(url: String)
        fun setIdeamImg(url: String)
        fun hideIdeaImageView()
        fun startViewProfileActivity(uid: String)
        fun setUnjoinButton()
        fun setJoinButton()
        fun setJoinersNumberAndNotifyAdapter()
        fun showInsertComm()
        fun setCreator(value: Boolean)
        fun notifyNewComment()
        fun showUpdatesLayout()
        fun hideUpdatesLayout()
        fun moveInterestBtn()
        fun hideJoinBtn()
        fun loadInfo(joinersTextRes: Int)
        fun showEditBtn()
        fun hideEditBtn()
        fun createPopUpMenu()
        fun closeActivity()
        fun showJoiners(joiners: ArrayList<Joiner>)
    }

    interface Presenter{
        fun loadOrganizerInfo()
        fun setupImage()
        fun viewProfileClicked()
        fun checkForInsertUpdate()
        fun checkForUpdates()
        fun setBtn()
        fun joinBtnClicked()
        fun addComment(comment: String)
        fun loadEventInfo()
        fun loadUser()
        fun menuItemClicked(id: Int):Boolean
    }
}