package com.lorenzo.connectivecp.model

import java.io.Serializable

data class Update(val date: String, val text: String): Serializable