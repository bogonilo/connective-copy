package com.lorenzo.connectivecp.model

import java.io.Serializable

data class User(val uid: String, val email: String, val name: String, val phoneN: String = "", val position: String = "",
                val office : String = "", val profileImgUrl: String = "", val chapter: String = "", val bio: String = "",
                val skills: ArrayList<String> = ArrayList(), val interests: ArrayList<String> = ArrayList(),
                val joinedIdeas: ArrayList<String> = ArrayList()):Serializable