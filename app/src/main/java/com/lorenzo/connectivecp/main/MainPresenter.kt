package com.lorenzo.connectivecp.main

import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.recommendation.APIClient
import com.lorenzo.connectivecp.recommendation.RecommendationAPI
import android.util.Log
import com.lorenzo.connectivecp.model.Recommendation
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainPresenter(view: MainContract.View) :
    MainContract.Presenter {

    private var mView = view
    var user = User("", "", "")

    override fun navigationItemSelected(id: Int): Boolean {
        when (id) {
            R.id.navigation_events -> {
                mView.showEventFrag()
                return true
            }
            R.id.navigation_skills -> {
                mView.showSkillsFrag()
                return true
            }
//            R.id.navigation_proposals -> {
//                mView.showBudgetFrag()
//                return true
//            }
            R.id.navigation_profile -> {
                mView.showProfileFrag()
                return true
            }
        }
        return false
    }

}