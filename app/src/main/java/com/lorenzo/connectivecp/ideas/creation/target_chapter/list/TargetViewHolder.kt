package com.lorenzo.connectivecp.ideas.creation.target_chapter.list

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.chapter_element.view.*

class TargetViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    var subChapters = ArrayList<String>()

    fun bindView(entry: String, adapter: TargetAdapter, parent: Boolean) {
        itemView.checkBox_chapter.setText(entry)
        if(parent){
            itemView.subchapter_recyclerView.adapter =
                SubChapterAdapter(subChapters)
            itemView.subchapter_recyclerView.layoutManager = LinearLayoutManager(itemView.context)
        }
    }
}