package com.lorenzo.connectivecp.model

import android.os.Parcelable
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Idea(var title: String, var description: String, var categories: ArrayList<String> = ArrayList(),
                var startDate: Timestamp, var isEndDateSet: Boolean = false,
                var endDate: Timestamp = Timestamp(0,0), var isRSVPDateSet: Boolean = false,
                var RSVPDate: Timestamp = Timestamp(0,0), var location: String,
                var creatorName: String, var creatorUid: String, var minPartecipants: Int, var maxPartecipants: Int,
                var target: ArrayList<String>, var updates: ArrayList<Update> = ArrayList(),
                var joiners: ArrayList<Joiner> = ArrayList(), var pictureUrl: String = "", var id: String = "") :Parcelable