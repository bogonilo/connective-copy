package com.lorenzo.connectivecp.profile.login.restore

interface RestorePasswordContract {

    interface View{
        fun onPasswordResetSuccessful()
        fun onPasswordResetError()
    }

    interface Presenter{
        fun restoreBtnClick(email: String)
    }

}