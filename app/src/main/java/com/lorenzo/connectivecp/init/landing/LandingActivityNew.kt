package com.lorenzo.connectivecp.init.landing

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.login.LoginActivity
import com.lorenzo.connectivecp.profile.signup.SignUpFragmentsActivity


class LandingActivityNew : AppCompatActivity() {

    private lateinit var inflater: LayoutInflater
    private lateinit var viewPager: ViewPager
    private lateinit var dot1: ImageView
    private lateinit var dot2: ImageView
    private lateinit var dot3: ImageView
    private lateinit var dot: Array<ImageView>
    private lateinit var signUp: Button
    private lateinit var logIn: TextView
    private lateinit var slideToContinue: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing_new)
        dot1 = findViewById(R.id.dot1_landing)
        dot2 = findViewById(R.id.dot2_landing)
        dot3 = findViewById(R.id.dot3_landing)
        signUp = findViewById(R.id.signUp_btn_landing)
        logIn = findViewById(R.id.login_member_landing)
        dot = arrayOf(dot1, dot2, dot3)
        slideToContinue = findViewById(R.id.slide_to_cont_landing)
        inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater;
        viewPager = findViewById(R.id.viewPager_landing)
        viewPager.adapter = LandingPagerAdapter(inflater)
    }

    override fun onStart() {
        super.onStart()
        val pageChangeListener = object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                dot1.setImageResource(R.drawable.page_dot_notselected)
                dot2.setImageResource(R.drawable.page_dot_notselected)
                dot3.setImageResource(R.drawable.page_dot_notselected)
                dot[position].setImageResource(R.drawable.page_dot_selected)
                if(position == 2){
                    slideToContinue.visibility = View.INVISIBLE
                    signUp.visibility = View.VISIBLE
                    logIn.visibility = View.VISIBLE
                }
                else{
                    slideToContinue.visibility = View.VISIBLE
                    signUp.visibility = View.INVISIBLE
                    logIn.visibility = View.INVISIBLE
                }
            }
        }
        viewPager.addOnPageChangeListener(pageChangeListener)
        signUp.setOnClickListener{ startActivity(Intent(this, SignUpFragmentsActivity::class.java)) }
        logIn.setOnClickListener{startActivity(Intent(this, LoginActivity::class.java)) }

    }
}
