package com.lorenzo.connectivecp.profile.signup.step_three

import android.net.Uri
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.firebase.StorageCallback
import com.lorenzo.connectivecp.firebase.StorageManager
import com.google.firebase.auth.FirebaseAuth
import java.io.File

class Fragment3Presenter(view: Fragment3Contract.View) :
    Fragment3Contract.Presenter {

    private var mView = view
    private var auth = FirebaseAuth.getInstance()
    private var firestoreManager = FirestoreManager()
    var roles = emptyArray<String>()
    var mainChapters = emptyArray<String>()
    var parentChapters = emptyArray<String>()
    lateinit var subChapters: Array<Array<String>?>
    private var storageManager = StorageManager()
    private var user = User("", "", "")

    override fun nextClicked(email: String, name: String, imagePath: String, position: String,
        location: String, chapter: String) {
        mView.showProgBar()
        if (location.isEmpty()) {
            mView.shakeLocation()
            mView.hideProgBar()
        } else if (chapter.isEmpty()) {
            mView.shakeChapter()
            mView.hideProgBar()
        } else if (position.isEmpty()) {
            mView.shakeRole()
            mView.hideProgBar()
        } else {
            if (File(imagePath).exists()) {
                user = User(auth.currentUser!!.uid, email, name, "", position, location, imagePath, chapter)
                storageManager.addImgToFirebase(
                    user.uid,
                    Uri.fromFile(File(imagePath)).toString(),
                    object : StorageCallback {
                        override fun onCallbackImgUrl(value: String) {
                            var user2 = User(user.uid, user.email, user.name, user.phoneN, user.position, user.office,
                                value, user.chapter)
                            user = user2
                            firestoreManager.addUserToCollection(user, object : FireCallback.UserAdded {
                                override fun onCallbackUser(value: User) {
                                    mView.skillsFrag(user)
                                }
                            })
                        }

                        override fun onUploadError() {
                            mView.skillsFrag(user)
                        }
                    })
            } else {
                val user = User(auth.currentUser!!.uid, email, name, "", position, location,
                    "https://firebasestorage.googleapis.com/v0/b/standapp-bcbb2.appspot.com/o/profile_images%2Faccenture_logo.png?alt=media&token=b581c20a-4602-4e23-a742-1236c2e3c0d7",
                    chapter)
                firestoreManager.addUserToCollection(user)
                mView.skillsFrag(user)
            }
        }
    }

    override fun dialogClicked(tag: String, value: String, index: Int) {
        when (tag) {
            "location" -> {
                mView.setLocationText(value)
            }
            "role" -> {
                mView.setRoleText(value)
            }
            "chapter" -> {
                chapterSelection(value)
            }
            "subChapters" -> {
                mView.setChapterText(value)
            }
        }
    }

    override fun loadRolesArray() {
        firestoreManager.loadCareersLevel(object : FireCallback.CareersLevel {
            override fun onCallbackCareersLevelList(list: Array<String>) {
                roles = list
            }
        })
    }

    override fun loadChapters() {
        firestoreManager.loadMainChapters(object : FireCallback.MainChapters {
            override fun onCallbackMainChaptersList(listAll: Array<String>, listParents: Array<String>) {
                mainChapters = listAll
                parentChapters = listParents
                subChapters = arrayOfNulls(parentChapters.size)
                parentChapters.forEachIndexed { index, strings ->
                    loadSubChapters(index)
                }
            }
        })
    }

    fun loadSubChapters(index: Int) {
        firestoreManager.loadSubChapters(parentChapters[index], object : FireCallback.SubChapters {
            override fun onCallbackSubChaptersList(list: Array<String>) {
                subChapters[index] = list
            }
        })
    }

    fun chapterSelection(value: String) {
        if (parentChapters.contains(value))
            mView.createDialog(subChapters[parentChapters.indexOf(value)]!!, value, "subChapters")
        else
            mView.setChapterText(value)
    }

    override fun checkAllFields() {
        if(mView.getChapter().isNotEmpty() && mView.getLocation().isNotEmpty() && mView.getPosition().isNotEmpty())
            mView.activateButton()
        else
            mView.deactButton()
    }
}