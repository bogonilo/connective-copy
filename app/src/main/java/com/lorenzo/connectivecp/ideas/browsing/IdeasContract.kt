package com.lorenzo.connectivecp.ideas.browsing

import androidx.fragment.app.Fragment
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.utils.EventsFilterEnum

interface IdeasContract {
    interface View {
        fun getFragment(): Fragment
        fun setAdapter(ideas: ArrayList<Idea>)
        fun stopRefreshing()
        fun updateEvents()
        fun loadEvents()
        fun switchOnAllEvents()
        fun switchOnRecommended()
    }

    interface Presenter {
        fun initialSet()
        fun getIdeasList()
        fun getEventsOrdering()
        fun setEventsFilter(type: EventsFilterEnum)
        fun getUserData()
        fun getRecommendations()
        fun buildRecommendedList(): ArrayList<Idea>
    }
}