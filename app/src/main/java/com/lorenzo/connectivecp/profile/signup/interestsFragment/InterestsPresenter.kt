package com.lorenzo.connectivecp.profile.signup.interestsFragment

import android.content.Intent
import android.os.Bundle
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.model.User


class InterestsPresenter(view: InterestsContract.View) :
    InterestsContract.Presenter {

    private var mView = view
    var interests = ArrayList<String>()
    var selectedInterests = ArrayList<String>()
    private var firestoreManager = FirestoreManager()
    var user = User("", "", "")

    override fun doneClicked(arguments: Bundle) {
        if(selectedInterests.size > 2){
            var skills = arguments.getStringArrayList("skills")
            mView.showProgBar()
            var userFromArguments = arguments.getSerializable("user") as User
            firestoreManager.updateSkillsInterests(userFromArguments.uid, skills!!, selectedInterests)
            user = User(
                userFromArguments.uid,
                userFromArguments.email,
                userFromArguments.name,
                userFromArguments.phoneN,
                userFromArguments.position,
                userFromArguments.office,
                userFromArguments.profileImgUrl,
                userFromArguments.chapter,
                "",
                skills,
                selectedInterests
            )
            firestoreManager.updateSkillsCollection(skills)
            mView.startMainActivity()
        }
        else
            mView.notEnough()
    }

    override fun loadInterests() {
        firestoreManager.getInterestsList(object: FireCallback.InterestsList{
            override fun onCallbackInterestsList(interestsList: ArrayList<String>) {
                interests.addAll(interestsList)
                mView.notifyAdapter()
            }
        })
    }

    override fun getInterestsFromIntent(intent: Intent) {
        firestoreManager.getInterestsList(object: FireCallback.InterestsList{
            override fun onCallbackInterestsList(interestsList: ArrayList<String>) {
                interests.addAll(interestsList)
                selectedInterests.addAll(intent.getStringArrayListExtra("interests"))
                mView.notifyAdapter()
                checkNOfInterests()
            }
        })
    }

    override fun checkNOfInterests() {
        if(selectedInterests.size > 2)
            mView.activateDoneButton()
        else
            mView.deactDoneButton()
    }

    override fun doneFromActivity() {
        if(selectedInterests.size > 2)
            mView.finishActWithResult()
        else
            mView.notEnough()
    }
}