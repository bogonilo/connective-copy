package com.lorenzo.connectivecp.me.past

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.main.MainActivity
import com.lorenzo.connectivecp.me.calendar.CalendarItemAdapter
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.utils.SharedPreferenceManager

class PastFragment : Fragment() {

    private lateinit var calendarRecyclerView: RecyclerView
    private lateinit var calendarAdapter: RecyclerView.Adapter<*>
    private lateinit var emptyListtext: TextView
    private lateinit var refreshLayout: SwipeRefreshLayout
    private var calendarItems = ArrayList<Idea>()
    var user = User("","","")
    private lateinit var preferenceManager: SharedPreferenceManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferenceManager = SharedPreferenceManager(context)
        calendarAdapter = CalendarItemAdapter(calendarItems, preferenceManager.getIDFromPref(), context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_past, container, false)
        calendarRecyclerView = v.findViewById(R.id.recyclerView_past)
        emptyListtext = v.findViewById(R.id.emptyList_past)
        refreshLayout = v.findViewById(R.id.refreshLayout_past)
        calendarRecyclerView.layoutManager = LinearLayoutManager(context)
        calendarRecyclerView.adapter = calendarAdapter
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refreshLayout.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                (activity as MainActivity).updateCalendarInMeFrag()
                refreshLayout.isRefreshing = false
            }
        })
    }

    fun populateList(list: ArrayList<Idea>){
        calendarItems.clear()
        calendarItems.addAll(list)
        calendarAdapter.notifyDataSetChanged()
    }

    fun hideEmptyListTxt(){
        emptyListtext.visibility = View.GONE
    }

    fun showEmptyListTxt(){
        emptyListtext.visibility = View.VISIBLE
    }

    fun emptyAdapter() {
        calendarAdapter.notifyDataSetChanged()
    }
}
