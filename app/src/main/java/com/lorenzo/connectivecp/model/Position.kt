package com.lorenzo.connectivecp.model

data class Position(val name: String, val number: Int)