package com.lorenzo.connectivecp.profile.edit

import android.view.View
import kotlinx.android.synthetic.main.skill_element_viewprofile.view.*

class SkillsViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView)  {
    fun bindView(entry: String, adapter: SkillsAdapter, editable: Boolean, position: Int){
        itemView.skill_txt_viewProfile.setText(entry)
        if(editable){
            itemView.skills_element_layout.setOnClickListener {
                adapter.remove(position)
            }
        }
    }
}