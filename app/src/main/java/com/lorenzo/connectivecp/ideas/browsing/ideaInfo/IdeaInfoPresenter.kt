package com.lorenzo.connectivecp.ideas.browsing.ideaInfo

import android.content.Context
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
import com.lorenzo.connectivecp.model.Idea
import com.lorenzo.connectivecp.model.Joiner
import com.lorenzo.connectivecp.model.Update
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.recommendation.APIManager
import com.lorenzo.connectivecp.utils.SharedPreferenceManager
import java.text.SimpleDateFormat
import java.util.*

class IdeaInfoPresenter(view: IdeaInfoContract.View, context: Context):
    IdeaInfoContract.Presenter {

    private var mView = view
    lateinit var user: User
    lateinit var idea: Idea
    var activityID = ""
    private var firestoreManager = FirestoreManager()
    private var apiManager = APIManager()
    var joined = false
    private var df = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH)
    private var preferenceManager = SharedPreferenceManager(context)
    private var currUID = preferenceManager.getIDFromPref()

    override fun loadUser() {
        firestoreManager.getUserFromFirestore(currUID, object : FireCallback.User{
            override fun onCallbackUser(value: User) {
                user = value
                if(activityID in user.joinedIdeas)
                    joined = true
                setBtn()
            }
            override fun onCallbackUserAuthButNoCompleteSignUp() {}
        })
    }

    override fun loadEventInfo() {
        firestoreManager.getIdea(activityID, object : FireCallback.SingleActivity{
            override fun onCallbackActivity(value: Idea) {
                idea = value
                if(value.joiners.size > 1)
                    mView.loadInfo(R.string.people_joined)
                else
                    mView.loadInfo(R.string.person_joined)
                checkForInsertUpdate()
                checkForUpdates()
            }
        })
    }

    override fun loadOrganizerInfo() {
        firestoreManager.getPicPositionCard(idea.creatorUid, object : FireCallback.CardInfo {
            override fun onCallbackProPicPosition(proPic: String, position: String) {
                mView.setOrgImg(proPic)
            }
        })
    }

    override fun setupImage() {
        if(idea.pictureUrl.isNotEmpty())
            mView.setIdeamImg(idea.pictureUrl)
        else{
            mView.hideIdeaImageView()
            mView.moveInterestBtn()
        }
    }

    override fun viewProfileClicked() {
        mView.startViewProfileActivity(idea.creatorUid)
    }

    override fun setBtn() {
        if(joined)
            mView.setUnjoinButton()
        else
            mView.setJoinButton()
    }

    override fun joinBtnClicked(){
        val uid = currUID
        val joiner = Joiner(uid)
        if(joined){
            mView.setJoinButton()
            joined = false
            idea.joiners.remove(joiner)
            mView.setJoinersNumberAndNotifyAdapter()
            firestoreManager.removeJoinerFromIdea(activityID, joiner)
            firestoreManager.removeJoinedIdeaToUser(activityID, uid)
            apiManager.setParticipation(uid, activityID, 0)
        }
        else{
            mView.setUnjoinButton()
            joined = true
            idea.joiners.add(joiner)
            mView.setJoinersNumberAndNotifyAdapter()
            firestoreManager.addJoinerToIdea(activityID, joiner)
            firestoreManager.addJoinedIdeaToUser(activityID, uid)
            apiManager.setParticipation(uid, activityID, 1)
        }
    }

    override fun checkForInsertUpdate(){
        if(currUID == idea.creatorUid){
            mView.setCreator(true)
            mView.showInsertComm()
            mView.hideJoinBtn()
            mView.showEditBtn()
        }
        else{
            mView.setCreator(false)
            mView.hideEditBtn()
        }
    }

    override fun addComment(comment: String) {
        if(comment.isNotEmpty()){
            val update = Update(df.format(Date()), comment)
            idea.updates.add(update)
            firestoreManager.addUpdateToActivity(activityID, update)
            mView.notifyNewComment()
        }
    }

    override fun checkForUpdates() {
        if(idea.updates.size > 0){
            mView.showUpdatesLayout()
        }
    }

    override fun menuItemClicked(id: Int):Boolean {
        when(id){
            R.id.delete_idea_info_menu->{
                firestoreManager.deleteActivityFromFirestore(idea.id)
                mView.closeActivity()
                return true
            }
            else->{
                return false
            }
        }
    }
}