package com.lorenzo.connectivecp.main

interface MainContract {

    interface View{
        fun showEventFrag()
        fun showSkillsFrag()
        fun showBudgetFrag()
        fun showProfileFrag()
    }

    interface Presenter{
        fun navigationItemSelected(id: Int):Boolean
    }
}