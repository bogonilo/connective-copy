package com.lorenzo.connectivecp.profile.signup.step_three

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.Toast
import com.lorenzo.connectivecp.model.User
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.profile.signup.SignUpFragmentsActivity
import android.view.WindowManager
import android.view.Gravity
import androidx.fragment.app.FragmentManager
import com.lorenzo.connectivecp.main.MainActivity
import com.lorenzo.connectivecp.profile.signup.skillsFragment.SkillsFragment
import com.lorenzo.connectivecp.view.TextInputField

class Fragment3SignUp : Fragment(), Fragment3Contract.View {

    private lateinit var location: TextInputField
    private lateinit var position: TextInputField
    private lateinit var chapter: TextInputField
    private lateinit var next: Button
    private lateinit var progBar: ProgressBar
    private lateinit var presenter: Fragment3Presenter
    private lateinit var fm: FragmentManager
    private var skillsFrag = SkillsFragment()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.signup_frag3_new, container, false)
        location = view.findViewById(R.id.location_signUp2_new)
        position = view.findViewById(R.id.position_signUp2_new)
        chapter = view.findViewById(R.id.chapter_signUp2_new)
        next = view.findViewById(R.id.joinFragBtn3_new)
        progBar = view.findViewById(R.id.progressBar_signUp2_new)
        presenter = Fragment3Presenter(this)
        return view
    }

    override fun onStart() {
        super.onStart()
        fm = fragmentManager!!
        progBar.visibility = View.INVISIBLE
        (activity as SignUpFragmentsActivity).setTitleText(2)
        presenter.loadRolesArray()
        presenter.loadChapters()
        location.setOnClickListener{
            createDialog(resources.getStringArray(R.array.offices), getString(R.string.dialog_location_title), "location") }
        position.setOnClickListener { createDialog(presenter.roles,getString(R.string.dialog_role_title), "role") }
        chapter.setOnClickListener { createDialog(presenter.mainChapters, getString(R.string.dialog_chapter_title), "chapter") }
        next.setOnClickListener {
            arguments?.let {
                presenter.nextClicked(it.getStringArrayList("user")!![0], it.getStringArrayList("user")!![1],
                    it.getStringArrayList("user")!![2], position.text.toString(), location.text.toString(),
                    chapter.text.toString())
            }
        }
    }

    override fun nonValidfields() {
        Toast.makeText(context, "Fill in all the fields", Toast.LENGTH_LONG).show()
    }

    override fun startMainActivity(user: User) {
        val intent = Intent(context, MainActivity::class.java)
        intent.putExtra("user", user)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent)
        hideProgBar()
        activity!!.finishAffinity()
    }

    override fun hideProgBar() {
        progBar.visibility = View.INVISIBLE
    }

    override fun showProgBar() {
        progBar.visibility = View.VISIBLE
    }

    override fun setLocationText(value: String) {
        location.setText(value)
        presenter.checkAllFields()
    }

    override fun setRoleText(value: String) {
        position.setText(value)
        presenter.checkAllFields()
    }

    override fun setChapterText(value: String) {
        chapter.setText(value)
        presenter.checkAllFields()
    }

    override fun shakeChapter() {
        chapter.shakeView(context)
        nonValidfields()
    }

    override fun shakeLocation() {
        location.shakeView(context)
        nonValidfields()
    }

    override fun shakeRole() {
        position.shakeView(context)
        nonValidfields()
    }

    override fun createDialog(listItems: Array<String>, title: String, tag: String){
        val items = listItems
        val builder = AlertDialog.Builder(context, R.style.full_width_dialog)
        builder.setTitle(title)
        builder.setItems(items) { dialog, which ->
            presenter.dialogClicked(tag, items[which], which)
        }
        val dialog = builder.create()
        with(dialog){
            listView.divider = ColorDrawable(Color.parseColor("#E8E8E8"))
            listView.dividerHeight = 2
            show()
        }
        val window = dialog.getWindow()
        window?.let{
            it.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT)
            it.setBackgroundDrawableResource(R.drawable.shape_location_dialog)
            val wlp = it.getAttributes()
            wlp.gravity = Gravity.BOTTOM
            wlp.dimAmount = 0.3f
            it.setAttributes(wlp)
        }
    }

    override fun activateButton() {
        next.setBackgroundResource(R.drawable.shape_signup)
    }

    override fun deactButton() {
        next.setBackgroundResource(R.drawable.shape_signup_unselected)
    }

    override fun skillsFrag(user: User) {
        val bundle = Bundle()
        bundle.putSerializable("user", user)
        skillsFrag.arguments = bundle
        SignUpFragmentsActivity.nextFragment(fm, skillsFrag)
    }

    override fun getChapter(): String {
        return chapter.text.toString()
    }

    override fun getLocation(): String {
        return location.text.toString()
    }

    override fun getPosition(): String {
        return position.text.toString()
    }
}