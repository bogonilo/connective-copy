package com.lorenzo.connectivecp.utils

import com.lorenzo.connectivecp.model.User

interface UpdateUserCallback {
    fun updateUserObject(updatedUser: User)
}