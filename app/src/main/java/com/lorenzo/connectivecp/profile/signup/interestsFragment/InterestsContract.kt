package com.lorenzo.connectivecp.profile.signup.interestsFragment

import android.content.Intent
import android.os.Bundle

interface InterestsContract {

    interface View{
        fun notifyAdapter()
        fun showProgBar()
        fun hideProgBar()
        fun notEnough()
        fun startMainActivity()
        fun activateDoneButton()
        fun deactDoneButton()
        fun passSelectedToAdapter()
        fun finishActWithResult()
    }

    interface Presenter{
        fun doneClicked(arguments: Bundle)
        fun doneFromActivity()
        fun loadInterests()
        fun getInterestsFromIntent(intent: Intent)
        fun checkNOfInterests()
    }
}