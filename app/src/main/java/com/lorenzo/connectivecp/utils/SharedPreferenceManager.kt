package com.lorenzo.connectivecp.utils

import android.content.Context

const val PREFERENCE_FILE_KEY = "ConnectivePrefKey"
const val USER_ID_KEY = "UserIdKey"
const val USER_EVENTS_ORDERING = "UserEventsOrdering"

class SharedPreferenceManager(context: Context?) {
    private var sharedPref = context?.getSharedPreferences(PREFERENCE_FILE_KEY, Context.MODE_PRIVATE)

    fun updateIDinPreferences(id: String){
        val editor = sharedPref?.edit()
        editor?.let{
            it.putString(USER_ID_KEY, id)
            it.apply()
        }
    }

    fun getIDFromPref():String{
        return sharedPref?.getString(USER_ID_KEY, "")!!
    }

    fun updateEventsFilterPreference(filter: EventsFilterEnum){
        val editor = sharedPref?.edit()
        editor?.let{
            it.putString(USER_EVENTS_ORDERING, filter.name)
            it.apply()
        }
    }

    fun getEventsFilterFromPref():EventsFilterEnum{
        return EventsFilterEnum.valueOf(sharedPref?.getString(USER_EVENTS_ORDERING, EventsFilterEnum.TIME.name)!!)
    }
}