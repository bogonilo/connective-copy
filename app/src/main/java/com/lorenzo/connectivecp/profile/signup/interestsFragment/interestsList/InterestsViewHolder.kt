package com.lorenzo.connectivecp.profile.signup.interestsFragment.interestsList

import android.view.View
import com.lorenzo.connectivecp.R
import kotlinx.android.synthetic.main.category_element.view.*

class InterestsViewHolder (itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

    fun bindView(entry: String, adapter: InterestsAdapter) {
        var selected = entry in adapter.interestsSelected
        if(selected)
            itemView.category_element_layout.setBackgroundResource(R.drawable.activity_category_selected)
        else
            itemView.category_element_layout.setBackgroundResource(R.drawable.activity_category)
        itemView.text_category_element.setText(entry)
        itemView.category_element_layout.setOnClickListener(View.OnClickListener {
            if(!selected){
                selected = true
                itemView.category_element_layout.setBackgroundResource(R.drawable.activity_category_selected)
                adapter.interestsSelected.add(entry)
                if(adapter.isFrag)
                    adapter.giveSelectedToFrag()
                else
                    adapter.giveSelectedToAct()
            }
            else{
                selected = false
                itemView.category_element_layout.setBackgroundResource(R.drawable.activity_category)
                adapter.interestsSelected.remove(entry)
                if(adapter.isFrag)
                    adapter.giveSelectedToFrag()
                else
                    adapter.giveSelectedToAct()
            }
        })
    }
}