package com.lorenzo.connectivecp.ideas.browsing.joiner

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.lorenzo.connectivecp.R
import com.lorenzo.connectivecp.model.Joiner
import com.lorenzo.connectivecp.firebase.FireCallback
import com.lorenzo.connectivecp.firebase.FirestoreManager
class JoinersAdapter(private val joiners: ArrayList<Joiner>) : RecyclerView.Adapter<JoinersViewHolder>() {

    private var firestoreManager = FirestoreManager()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): JoinersViewHolder {
        val v = LayoutInflater.from(p0.getContext())
            .inflate(R.layout.joiner, p0, false)
        return JoinersViewHolder(v)
    }

    override fun getItemCount(): Int {
        return joiners.size
    }

    override fun onBindViewHolder(p0: JoinersViewHolder, p1: Int) {
        p0.bindView(joiners[p1])
        firestoreManager.getNameProPic(joiners[p1].uid, object: FireCallback.NameProfilePic{
            override fun onCallbackNameProPicUrl(name: String, url: String) {
                p0.setNameAndImg(name, url)
            }
        })
    }
}