package com.lorenzo.connectivecp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Joiner(val uid: String): Parcelable